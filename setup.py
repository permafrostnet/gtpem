import setuptools
import codecs
import os.path

with open("README.md", "r") as fh:
    long_description = fh.read()


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith('__version__'):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


setuptools.setup(
    name="gtpem",
    version=get_version("gtpem/_version.py"),
    author="Nick Brown",
    author_email="nick.brown@carleton.ca",
    description="A package for running permafrost models on HPC resources",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/permafrostnet/gtpem",
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'gtpem = gtpem.gtpem_cli:main',
            'ssu = gtpem.stepwise_spinup_cli:main'
        ]
    },
    scripts=['scripts/gtpem_run', 'scripts/gtpem_build', 'scripts/gtpem_aggregate', 'scripts/gtpem_status'],
    classifiers=[
        "Programming Language :: Python :: 3"
    ],
    python_requires='>=3.6',
    install_requires=["pandas", "netCDF4", "tomlkit", "f90nml", "globsim"]
)
