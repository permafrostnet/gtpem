#!/usr/bin/env python3

import os
import argparse

from gtpem.backend import lookup_backend
from gtpem.core import PemConfig
from gtpem.controller import simulations_from_directories, simulation_from_abstraction, JobList
from gtpem._version import __version__ as version
from gtpem.log_config import configure_logging
from gtpem.decorators import deprecated

import logging
logger = logging.getLogger()


@deprecated("Use 'gtpem run' instead.")
def main(args):
    #logging.basicConfig(level=log_levels[args.verbosity], force=True)
    configure_logging(logger, args)

    cfg = PemConfig(args.config)

    if args.job_dir is not None:
        cfg._config['jobdata']['title'] = os.path.basename(args.job_dir)
        cfg._config['jobdata']['workingdirectory'] = os.path.dirname(args.job_dir)

    wd = os.path.join(cfg.get_working_directory(), cfg.get_job_name())
    
    if args.build_dirs:
        # Make simulations automatically from control file
        jobs = JobList.from_naive_combination(cfg)
        simulations = [simulation_from_abstraction(cfg, a) for a in jobs.abstractions]
    else:
        # Make Simulations from existing directories
        simulations = simulations_from_directories(cfg, wd)
    
    simulations = [s for s in simulations if s] # Remove failed ones
    # run
    Backend = lookup_backend(cfg.get('jobdata', 'backend', datatype='String'))
    backend = Backend(cfg, args.dry_run)
    print(backend.start(simulations))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Launch gtpem jobs")

    parser.add_argument("config", help="path to configuration toml file. ")
    parser.add_argument("-b", "--build-directories", action='store_true',
                        dest='build_dirs',
                        help="Whether or not to build directories automatically from a control file")
    parser.add_argument("-d", "--dry-run", action='store_true', default=False,
                        dest='dry_run',
                        help="If selected, will not submit job, and will only create directories and submission scripts")
    parser.add_argument("-J", '--job-dir', type=str, dest='job_dir', default=None,
                        help="Overrides the working directory in the configuration file and runs the job on the provided directory instead. Does not modify the configuration file")
    parser.add_argument("-v", "--verbose", dest="verbosity", action="count", default=0,
                        help="Verbosity (between 1-4 occurrences with more leading to more "
                        "verbose logging). ERROR=1, WARN=2, INFO=3, "
                        "DEBUG=4")
    parser.add_argument('-L', "--log", type=str, dest="logfile", default=None)
    parser.add_argument('--version', action='version', version=f"gtpem version {version}")
    
    args = parser.parse_args()

    main(args)
