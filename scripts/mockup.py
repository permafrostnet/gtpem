#!/usr/bin/env python3
#
# This "mockup" code is Ryan's demonstration of using the Backend
# class to run permafrostnet models. See the included README.txt for
# an example of how to use this command.  It assumes you are running
# on the ComputeCanada Niagara cluster.
#
# Since I don't know how the models need to be created, I setup a
# silly example uses hard-coded settings and file paths instead of
# reading them in from a config file.  I also use a fake model.

import os           # Instead of GeoTOP, I wrote a fake model
import sys

from gtpem.backend.backend import OneJobBackend     # Backend that runs everything as a single job
from gtpem.core import PemConfig
from gtpem.models.library import library
from gtpem.controller import simulations_from_directories

gtpem_folder = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
config_file = os.path.join(gtpem_folder, "controlfile.toml")

cfg = PemConfig(config_file)

wd = os.path.join(cfg.get_working_directory(), cfg.get_job_name())
'''
# Create work directory
if os.path.exists(wd):
    print(f"Path '{wd}' already exists!")
    sys.exit(1)
os.mkdir(wd)

fake_config = {
    'work_dir': os.path.join(os.environ["SCRATCH"], 'gtpem-work-test'),
    'path_to_cmd': os.path.join(gtpem_folder, 'bin', 'ryancmd'),
    'template_dir': os.path.join(gtpem_folder, 'templates', 'ryancmd'),
    'pause_times': [1, 1, 5, 3, 2, 2, 1],
}


def create_sims(fake_config):
    """Make a list of fake models as an example workload"""
    simulations = []
    for pause in fake_config['pause_times']:
        model = RyanCmd(
            fake_config['path_to_cmd'],
            len(simulations)+1,
            fake_config['work_dir'],
            fake_config['template_dir'],
            pause)
        simulations.append(model)
    return simulations
'''


# Make Simulations from existing directories
simulations = simulations_from_directories(cfg, wd)

# Here is the new interface 
backend = OneJobBackend(cfg)
backend.write_glost(simulations)
backend.write_slurm()
