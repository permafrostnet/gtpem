import unittest

from os import path

from rootpath import rootpath
from gtpem.core import PemConfig


class TestConfigFile(unittest.TestCase):
     
    def setUp(self):
        # toml = pkg_resources.resource_filename("gtpem.builder", '../controlfile.toml.dev')
        toml = path.join(rootpath, 'controlfile.dev.toml')
        self.cfg = PemConfig(toml)

    def test_creation(self):
        self.assertTrue(isinstance(self.cfg, PemConfig))
    
         