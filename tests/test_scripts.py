import subprocess
import unittest
from pathlib import Path
from os import path
from glob import glob
import shutil

from rootpath import rootpath


class TestRun(unittest.TestCase):
    def setUp(self):
        self.bin = Path(rootpath, "scripts", "gtpem_run")
        
    def test_runs(self):
        self.assertEqual(subprocess.run(["python", self.bin, "--version"]).returncode, 0)


class TestBuild(unittest.TestCase):
    def setUp(self):
        self.bin = Path(rootpath, "scripts", "gtpem_build")
        self.build_dir = Path(rootpath, "tests", "testfiles", "gtpem_build")
        self.ctrlfile = Path(rootpath, "tests", "testfiles", "test_build_config.toml")

    def test_runs(self):
        self.assertEqual(subprocess.run(["python", self.bin, "--version"]).returncode, 0)

    def test_builds(self):
        self.assertEqual(subprocess.run(["python", self.bin, "-J", self.build_dir, self.ctrlfile]).returncode, 0)
        self.assertEqual(len(list(Path(self.build_dir).glob("*"))), 2 * 3 * 6 + 1)  # manifest file

    def tearDown(self):
        if Path(self.build_dir).is_dir():
            shutil.rmtree(self.build_dir)


class TestAggregate(unittest.TestCase):
    def setUp(self):
        self.bin = path.join(rootpath, "scripts", "gtpem_aggregate")
        
    def test_runs(self):
        self.assertEqual(subprocess.run(["python", self.bin, "--version"]).returncode, 0)


if __name__ == '__main__':
    unittest.main()
