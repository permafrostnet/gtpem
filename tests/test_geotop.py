import unittest

from os import path

from rootpath import rootpath
from gtpem.models.geotop.geotop import GeotopDirectory
from gtpem.models.template import ModelDirectory


class TestGeotopDirectory(unittest.TestCase):
    
    def setUp(self):
        dirpath = path.join(rootpath, "tests", "testfiles", "GT_DEMO")
        self.GD = GeotopDirectory(dirpath)
    
    def test_creation(self):
        self.assertTrue(isinstance(self.GD, GeotopDirectory))
        self.assertTrue(isinstance(self.GD, ModelDirectory))
    
    def test_valid(self):
        self.assertTrue(self.GD.validate())

    def tearDown(self):
        pass
        

if __name__ == '__main__':
    unittest.main()
