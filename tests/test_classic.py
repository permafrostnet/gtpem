import unittest

from os import path

from rootpath import rootpath
from gtpem.models.classic.classic import ClassicDirectory
from gtpem.models.template import ModelDirectory


class TestClassicDirectory(unittest.TestCase):
    
    def setUp(self):
        dirpath = path.join(rootpath, "tests", "testfiles", "classic_demo")
        self.GD = ClassicDirectory(dirpath)
    
    def test_creation(self):
        self.assertTrue(isinstance(self.GD, ClassicDirectory))
        self.assertTrue(isinstance(self.GD, ModelDirectory))

    def tearDown(self):
        pass
        

if __name__ == '__main__':
    unittest.main()
