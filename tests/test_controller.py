import unittest
from gtpem.core import PemConfig
from gtpem.controller import SimulationAbstraction, build_abstraction_list, JobList
from rootpath import rootpath
from os import path, unlink
import tomlkit
import tempfile


class TestConfig(unittest.TestCase):
    
    def setUp(self):
        # toml = pkg_resources.resource_filename("gtpem.builder", '../controlfile.toml.dev')
        toml = path.join(rootpath, 'controlfile.dev.toml')
        self.cfg = PemConfig(toml)
    
    def test_creation(self):
        self.assertTrue(isinstance(self.cfg, PemConfig))

    
class TestSimulationDescription(unittest.TestCase):
    
    def setUp(self):
        self.sim = SimulationAbstraction(model='geotop', site='site1', forcing='wind.txt')
    
    def test_creation(self):
        self.assertTrue(isinstance(self.sim, SimulationAbstraction))
        self.assertRaises(TypeError, SimulationAbstraction(model='x', site='y', forcing='z', parameters='1'))
        self.assertRaises(TypeError, SimulationAbstraction(model='x', site='y', forcing='z', parameters=[[1, 2, 3]]))

    def test_getters(self):
        self.assertEqual(self.sim.get_model(), 'geotop')
        self.assertEqual(self.sim.get_forcing(), 'wind.txt')
        self.assertEqual(self.sim.get_site_name(), 'site1')

    def test_csv_creation(self):
        self.assertEqual(len(self.sim.to_lines()), 2)

    
class TestGeotopFactory(unittest.TestCase):
    
    def setUp(self):
        self.cfg = PemConfig.empty()
        
        # make temp files
        self.P1 = tempfile.NamedTemporaryFile(prefix='gtpem', delete=False)
        self.P2 = tempfile.NamedTemporaryFile(prefix='gtpem', delete=False)
        self.P3 = tempfile.NamedTemporaryFile(prefix='gtpem', delete=False)

        with tempfile.NamedTemporaryFile('w', delete=False) as f:
            f.write(f"site,{self.P1.name},{self.P2.name},{self.P3.name}\n")
            f.write("S1,yes,yes,yes\n")
            f.write("S2,,yes,yes\n")
            f.write("S3,,,\n")
            csv = f.name

        # write params
        params = tomlkit.table()
        params.add('soil', [['basicgeotop', 'file1'], ['basicgeotop', 'file2'], ['basicgeotop', 'file3']])
        params.add('snow', [['basicgeotop', 'file4'], ['basicgeotop', 'file5']])
        params.add('vegetation', [['selectivegeotop', csv]])

        self.cfg._config["parameters"]["geotop"] = params

        self.L = build_abstraction_list(self.cfg, ["geotop"], {'met1': ["S1", "S2", "S3"], 'met2': ["S1", "S2", "S3"]})
        self.J = JobList(self.cfg, self.L)
        self.df = self.J.table()

    def tearDown(self) -> None:
        for file in [self.P1, self.P2, self.P3]:
            file.close()
            unlink(file.name)
            
        return super().tearDown()

    def test_abstractions_exist(self):
        self.assertLess(0, len(self.L))

    def test_one_from_each(self):
        self.assertTrue(all(self.df.loc[:, ['file4', 'file5']].sum(axis=1) == 1))
        self.assertTrue(all(self.df.loc[:, ['file1', 'file2', 'file3']].sum(axis=1) == 1))

    def test_correct_number_of_sims(self):
        self.assertEqual(2 * 2 * 3 * 1, self.df[self.df.site == "S3"].shape[0])
        self.assertEqual(2 * 2 * 3 * 2, self.df[self.df.site == "S2"].shape[0])
        self.assertEqual(2 * 2 * 3 * 3, self.df[self.df.site == "S1"].shape[0])


if __name__ == '__main__':
    unittest.main()
