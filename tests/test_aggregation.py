import netCDF4 as nc
import unittest

from rootpath import rootpath
from os import path, remove

from gtpem.models.template import ModelAggregator
from gtpem.models.geotop.geotop import GeotopSoilTemperatureAggregator
from gtpem.models.geotop.geotop import GeotopDirectory
from gtpem.models import lookup_aggregator


class TestGeotopSoilTemperatureAggregator(unittest.TestCase):
    
    def setUp(self):
        geotop_dirpath = path.join(rootpath, 'tests', 'testfiles', 'GT_DEMO')
        geotop_dirpath2 = path.join(rootpath, 'tests', 'testfiles', 'GT_DEMO_2')
        directory = GeotopDirectory(geotop_dirpath)
        directory2 = GeotopDirectory(geotop_dirpath2)
        self.aggregator = GeotopSoilTemperatureAggregator(directory)
        self.aggregator2 = GeotopSoilTemperatureAggregator(directory2)
        self.target = path.join(rootpath, 'tests', 'testfiles', 'temp_test_aggregation.nc')

    def test_creation(self):
        self.assertTrue(isinstance(self.aggregator, ModelAggregator))
        self.assertTrue(isinstance(self.aggregator, GeotopSoilTemperatureAggregator))

    def test_new_aggregation(self):
        self.aggregator.aggregate(self.target)
        self.assertTrue(path.isfile(self.target))
        
        with nc.Dataset(self.target) as dataset:
            self.assertTrue('geotop' in dataset.groups)
            self.assertEqual(len(dataset['geotop']['simulation'][:]), 1)
        
        self.aggregator2.aggregate(self.target)

        with nc.Dataset(self.target) as dataset:
            self.assertTrue('geotop' in dataset.groups)
            self.assertEqual(len(dataset['geotop']['simulation'][:]), 2)

    def test_lookup(self):
        self.assertEqual(lookup_aggregator('soil_temperature', model='geotop'),
                         GeotopSoilTemperatureAggregator)

    def tearDown(self):
        if path.isfile(self.target):
            remove(self.target)
        
    
if __name__ == '__main__':
    unittest.main()
