# Grid Toolkit for Permafrost Ensemble Modelling

Documentation for GTPEM is found at https://permafrostnet.gitlab.io/gtpem/index.html

# Installation
See the documentation at https://permafrostnet.gitlab.io/gtpem/Installation.html

# Using GTPEM

## When you already have directories created
If you already have directories created, you can use `gtpem_run` to send your jobs to the scheduler.
`
gtpem_run <config.toml>
`
Usually your configuration file needs to be set up so that the `title` parameter corresponds to the name of the directory in which the simulation directories are located, and that directory is within the `workingdirectory`. If you want to override this, and point to the directory at runtime, use the `-J` flag:
`
gtpem_run -J <jobs/directory> <config.toml>
`

## For creating directories
If you want to create directories automatically based on the information provided in the control file, use the `-b` flag to build directories.
`
gtpem_run -b <controlfile>
`

## Development and contributing
* Development meeting notes in the [wiki](https://gitlab.com/permafrostnet/gtpem/-/wikis/meeting-minutes)
* Please see [contributing guidelines](https://gitlab.com/permafrostnet/gtpem/-/blob/master/CONTRIBUTING.md)

## documentation notes:
* simulation folders are created in workingdirectory/jobname. If they already exist, the must be in the working directory
* 


## Description of GTPEM classes
`![Semantic description of image](/images/path/to/folder/image.png "Image Title")`

* `PemConfig`: created from a *.TOML control file. Contains information required for the job, and environment configuration necessary to run the various models. 
* `ModelDirectory`: Object representing the collection of files used for a simulation
* `Simulation`: Composition of a `ModelDirectory` along with methods necessary to run the model from the command line
* `SimulationAbstraction`: Abstracted description of a simulation, contains information on which model to use, which forcing data to use, which abstracted parameter set to apply. Contains no model-specific information and can be interpreted by `ModelDirectory` objects to create model-specific directories.
* `JobList`: A collection of `SimulationAbstraction` objects


### current general workflow
1. User calls GTPEM script
    - provides: path to control file, ...

2.  Generate model directories (if necessary)
    - Get a list of sites from Globsim file(s) 
    - Get a list of how many forcing (globsim) reanalyses there are?
    - Get a list of model parameter abstractions to apply (**"parameter abstractions" will be hard to apply**)
    - Generate `SimulationAbstraction` objects 
    - scheduler task maybe looks like `run.sh {pickled simulation description} {configuration file}`

3.  Submit job to cluster
    - list of `Simulation` objects is passed to the backend
    - generate submission scripts
    - submit to SLURM
    
4. Run models 


5. Aggregate results on the node
    - A  continually adds files to an output binary file
    - aggregation depends on control file (options include: everything? temperature output only? input parameters?)
    - aggregate individual simulation results (+/- input parameters) on the node to reduce number of files (1 simulation = 1 HDF5 output)
    - keep aggregating results on the node until... out of space? @ryantaylor-cu

5. Aggregate results 
    - Model-specific `DirectoryAggregator` objects 


### Outputs

### Other notes
The total number of simulations is controlled by the product of:
- number of reanalyses 
- number of sites 
- number of models to run
- number of parameter variations**

