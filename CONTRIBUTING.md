
### Recommenations for contributing to gtpem:

* Use numpy-style docstrings so that documentation can be generated automatically using sphinx 
    - [numpy docstrings](https://numpydoc.readthedocs.io/en/latest/format.html)


* Write unit tests where feasible


* Use the [gitlab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) branching strategy 
    - create a feature branch to add a new feature: feature branches should be short-lived.
    - merge back into `master`.
    - working versions are pushed to `production`.

## Adding content
Most classes should derive from one of the abstract base classes in `gtpem.models.template`

### Adding a new model
1. Create a new folder in `gtpem.models` with an `__init__.py`. The name of your model 
2. Implement `ModelDirectory` and `Simulation` subclasses for your model, including the necessary interfaces and `MODEL` class attribute which should be consistent with the folder name
3. Add your model to the library so it can be found (`gtpem.models.library.py`)

### Adding a new aggregator
1. Create a generic aggregator class in `gtpem.models.generic`, give it a unique `NAME` class attribute
2. For each model you want to support the aggregator, create a subclass, ensuring that the model name is contained within the `SUPPORTS` attribute list
3. Register the aggregator in the library (`gtpem.models.library.py`)

### Adding a new parameter set
1. For the model you want to Implement a subclass of the `ParameterSet` object
2. Register the parameter set in the library (`gtpem.models.library.py`)
