=============
Installation
=============


Installing on Niagara
=====================

The following commands are recommended installation steps for the Niagara server.

.. code-block:: bash

    mkdir gtpem-dev 
    cd gtpem-dev
    git clone https://github.com/geocryology/globsim.git
    git clone https://gitlab.com/permafrostnet/gtpem.git

    module load CCEnv nixpkgs gcc openmpi
    module load glost
    module load python hdf5 netcdf scipy-stack

    virtualenv --no-download env-gtpem
    source env-gtpem/bin/activate

    pip install --no-deps f90nml tomlkit pandas
    pip install --no-index netCDF4

    cd globsim 
    python setup.py develop
    cd ..

    cd gtpem 
    python setup.py develop --no-deps
    cd ..
   

The '--no-index' tells pip to install from a local Compute Canada repository, which contains packages built to be compatible and optimized for the cluster.  The '--no-deps' installs packages from the main pip repo, without installing/upgrading dependencies (which might be better left installed --no-index).


Installing with Anaconda
========================

If you are not installing GTPEM on HPC system, you may prefer to use anaconda as your package manager. The following commands are recommended installation steps for the Talik server.

.. code-block:: bash

    ENVNAME=env-gtpem
    
    mkdir gtpem-dev
    cd gtpem-dev

    git clone https://github.com/geocryology/globsim.git
    git clone https://gitlab.com/permafrostnet/gtpem.git

    conda create -n $ENVNAME python=3.7
    conda activate $ENVNAME

    cd globsim
    python setup.py develop --user
    cd ..

    cd gtpem
    python setup.py develop 
    cd ..

    conda activate $ENVNAME



