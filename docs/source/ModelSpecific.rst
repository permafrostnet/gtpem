==========================
Model-specific information
==========================

FreeThaw 1D
-----------
FreeThaw1D uses the OMS framework. To keep a consistent way of running the model, GTPEM makes some assumptions about the simulation directory that are more restrictive than running the simulation 

0. Each FreeThaw1d simulation directory must have a ./simulation subdirectory
1. Each FreeThaw1d simulation directory must contain exactly one \*.sim file 
2. File paths in the \*.sim file are given relative to the project directory (called $home: e.g. $home/data/)
3. The model Netcdf grid is already built
4. FreeThaw1d \*.jar files are expected to be in the ``lib`` subdirectory. The directory need not actually contain these jar files, because they are mounted at run-time using Singularity from a single location.