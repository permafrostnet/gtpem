===========
Using GTPEM
===========

Once you have GTPEM `installed <Installation>`_, you can use one of its three primary command line utilities:

.. code-block:: bash

    gtpem_build
    gtpem_run
    gtpem_aggregate

These each require a configuration file. To get more information on one of the command-line utilities, there is a help function available (i.e. ``gtpem-run --help``)

For all GTPEM runs
==================

Create a configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
GTPEM uses a `configuration file <Configuration>`_ to describe the ensemble simulations and to configure the required paths to your working directory, model executable and other required directories. 


If you don't already have model directories built
=================================================
GTPEM can be used to generate ensemble simulation directories from the configuration file. This can be useful if you are intending to run many simulations and don't want to create the directories yourself, or use a script to do so. However, its also possible to use GTPEM to submit existing simulation folders to the computing resource. 


Create template directories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
GTPEM uses template directories to simplify the directory creation process. Template directories are copied and subsequently modified by changing parameters or forcing data. Template directories must be valid model directories that can be run and are set up to work with your model. The path to a template directory must be supplied in the configuration file under the `appropriate subheading for each model <Configuration.rst#models>`__. 

.. image:: images/gtpem-build.svg
   :width: 700
   :alt: schematic of GTPEM simulation directory creation


If you already have model directories built
===========================================
If you do not want to auto-generate model directories, it is also possible to use GTPEM to submit existing simulation directories. For this use-case, after generating your model directories, use the ``gtpem_run`` to submit the simulations.

Running Simulations
===================

.. image:: images/gtpem-run.svg
   :width: 250
   :alt: schematic of GTPEM job submission


The script ``gtpem_run`` will run a collection of simulations on a Backend. For example, depending on the name of your configuration file:

.. code-block:: bash
                
                gtpem_run -b controlfile.example.toml

There are currently two main types of backends.  The LocalBackend runs all simulations, one at a time.  The ``gtpem_run`` script finishes after all simulations have completed.

The other type of backend runs simulations on an HPC cluster.  You cannot directly run computational programs on a cluster.  Instead, you must submit a job, which tells the cluster scheduler software what to run.  The job runs your programs when enough resources become available.  For cluster backends, ``gtpem_run`` finishes after submitting your job(s).  Note that the jobs might not have started.

The existing cluster backends are SlurmBackend and NiagaraBackend. They support the SLURM cluster scheduler software.  You can submit your jobs using ``gtpem_run``

Monitoring GTPEM Jobs
======================

On any backend with a slurm scheduler, you can monitor the job status using this command:

.. code-block:: bash
                
                squeue --me

This provides a high-level view of how your job is progressing. GTPEM also provides a script that provides more detail on the status of individual simulations. To check on the status of the simulations in your jobs, use this command:

.. code-block:: bash
                
                gtpem_status

This will give you a summary of which jobs have succeeded and which have not. Jobs that haven't succeeded may be queued, failed, or in progress.

Cancelling GTPEM Jobs
======================

The jobs should usually complete on their own.  If there is a problem and you need to stop all your SLURM jobs, you can run:

.. code-block:: bash
                
                scancel --user=$(id -un)

GTPEM on NiagaraBackend
=======================
There are some special considerations when running GTPEM on the Niagara backend, 

**Location of your simulations** : Niagara uses three separate storage areas: ``$SCRATCH``, ``$PROJECT`` and ``$HOME``.  The ``$HOME`` partition is not writeable from the compute nodes, so if you try to run simulations from there it will not work. It is considered best practice to submit jobs from ``$SCRATCH``, but keep in mind that files in this area may be periodically purged, so back up anything important to ``$PROJECT``.  This workflow also helps in preventing the buildup of endless simulation directories that take up shared storage in the ``$PROJECT`` area.

**Workflow for GlobSim**  : Downloaded Globsim data is kept on the ``Talik`` virtual machine. GTPEM uses scaled globsim files to create model directories. The easiest workflow for this is to: 

1. Attach the ``$PROJECT`` or ``$SCRATCH`` filesystems to ``Talik`` using the `sshfs <https://wiki.archlinux.org/title/SSHFS>`_ command (see the `Compute Canada Wiki <https://docs.computecanada.ca/wiki/Mounting_/project_space_on_a_VM_in_the_cloud>`_ for information on security considerations when doing this.
2. Run the GlobSim interpolation and scaling functions on Talik, using the attached filesystem as the `output directory <https://globsim.readthedocs.io/en/latest/Operation.html#interpolating>`_ 


**Demos** : There are a few demonstrator directories on Niagara to get you started. The scripts below will copy the demonstrator files and run the GTPEM toolkit

1. Build, run and aggregate a set of geotop simulations

.. code-block:: bash

   WKDIR=$SCRATCH/gtdemo20210802
   mkdir $WKDIR
   cd $WKDIR
   cp "/project/s/stgruber/nbr512/GTPEM-demos/AugustDemo/AugustDemo.toml" .
   sed "s_workingdirectory = \"/project/s/stgruber/nbr512/GTPEM-demos/AugustDemo\"_workingdirectory = \"$WKDIR\"_" AugustDemo.toml -i

   gtpem_build AugustDemo.toml
   gtpem_run AugustDemo.toml
   gtpem_aggregate AugustDemo.toml

2. Build, run and aggregate a set of CLASSIC simulations

.. code-block:: bash
   
   WKDIR=$SCRATCH/cldemo20210802 