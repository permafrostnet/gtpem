
=============
Configuration 
=============

At the heart of any GTPEM job is a configuration file. This provides the information necessary to GTPEM to find and run model executables. The configuration file follows the `TOML <https://toml.io/en/>`_ standard.

jobdata
=======
This section provides information on the job as a whole.

.. _title:

**title** `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : The name of your job. This is used as the folder name within the :ref:`working directory<workingdirectory>` in which your simulation directories are created.

.. _date:

**date** `(local date-time) <https://toml.io/en/v1.0.0-rc.3#local-date-time>`__ : The date associated with the job.

.. _jobdata-models:

**models** `(array) <https://toml.io/en/v1.0.0-rc.3#array>`__ : An array of models for which simulation directories will be built.  Model names must match their entry in the models section, and should be all lowercase.

.. _forcing:

**forcing** `(array) <https://toml.io/en/v1.0.0-rc.3#array>`__ : An array of file paths corresponding to scaled `GlobSim <https://github.com/geocryology/globsim>`__ output files which will be used to build job directories. 

.. _workingdirectory:

**workingdirectory** `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : The full path to your working directory. This directory is where your job will be created with the title specified in the :ref:`title<title>`.

.. _jobdata-backend:

**backend** `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__  The name of the backend to use. Must match the class definition and the relevant sections of the backend configuration.

.. _jobdata-skipchecks:

**skipchecks** `(boolean) <https://toml.io/en/v1.0.0-rc.3#boolean>`__ : (optional). If true, skip some directory validation checks (that give warning messages) that slow down the build process.  Use at your own risk, and consider setting it to false until your workflow is running smoothly.


models
======
This section provides model-specific configuration information

geotop
------

**binary** `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : The path to the binary model file used to run the geotop simulation.

**template**  `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : Full path to a working geotop simulation directory. This will be used to auto-generate simulation directories if ``gtpem_build`` is run.

classic
-------
**binary** `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : Full path to the compiled binary file used to run the classic simulation.

**container** `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : Full path to Singularity container user to run the classic simulation.

**template**  `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : Full path to a working classic simulation directory. This will be used to auto-generate simulation directories if ``gtpem_build`` is run.

**metloader**  `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : The path to the ASCIIMetLoader tool. This is used to convert GlobSim outputs into classic inputs. It is usually found in the Classic code repository.

freethaw
--------
**lib**  : `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : Full path to FreeThaw1D \*.jar files .
**template**  `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : Full path to a working FreeThaw1D simulation directory. This will be used to auto-generate simulation directories if ``gtpem_build`` is run.
**container** `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : Full path to Singularity-compatible container user to run the simulation.


backend
=======

The backend sections provide details on what a particular backend should do.  You must have a backend section that matches your chosen backend.  For example, if ``backend = 'NiagaraBackend'`` then the corresponding section will be called ``[backend.NiagaraBackend]``.

The configuration variables vary depending on the particular backend. The following subsections describe the available backends:

NiagaraBackend
--------------

The Niagara backend is the same as SlurmBackend, except has restrictions specific to the Compute Canada Niagara cluster, where jobs are requested for entire nodes instead of for cores (see nodecount below). This backend is specified by the configuration line ``backend = NiagaraBackend` in the `[jobdata]`` section.

**nodecount** `(integer) <https://toml.io/en/v1.0.0-rc.3#integer>`__ : How many nodes to request per job. On Niagara, each node has 40 cores. You should make sure there are enough simulations to keep all cores busy. Always request fewer *cores* than your number of simulations.

**jobcount** `(integer) <https://toml.io/en/v1.0.0-rc.3#integer>`__ : How many jobs to split the work into. 

**duration**  `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : The maximum duration (walltime) of a job, formatted as HH:MM:SS or MM:SS. This must be chosen carefully; when the time limit expires, all running simulations are terminated and any half-finished simulations are lost. Choosing a duration that is too short will cause jobs to be cut off. Choosing a duration that is too long may result in a longer wait in queue for resources. 

**account**  `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : Which partition to use for job submission.  Use 'debug' for testing that you have your job configured properly, and use 'compute' for running large simulations.  `See the Niagara help  for more information <https://docs.scinet.utoronto.ca/index.php/Niagara_Quickstart#Limits>`__


SlurmBackend
------------

This backend runs simulations on a generic HPC cluster that uses SLURM to schedule jobs. It requests jobs for a specified number of cores. This backend is specified by the configuration line ``backend = SlurmBackend`` in the ``[jobdata]`` section.

**corecount** `(integer) <https://toml.io/en/v1.0.0-rc.3#integer>`__ : How many cores to use in each job. You can fewer request fewer *cores* then simulations in a job

**jobcount** `(integer) <https://toml.io/en/v1.0.0-rc.3#integer>`__ : How many jobs to split the work into. 

**duration**  `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : The maximum duration (walltime) of a job, formatted as HH:MM:SS or MM:SS. This must be chosen carefully; when the time limit expires, all running simulations are terminated and any half-finished simulations are lost. Choosing a duration that is too short will cause jobs to be cut off. Choosing a duration that is too long may result in a longer wait in queue for resources. 

**account**  `(string) <https://toml.io/en/v1.0.0-rc.3#string>`__ : Which partition to use for job submission.  Use 'debug' for testing that you have your job configured properly, and use 'compute' for running large simulations.  `See the Niagara help  for more information <https://docs.scinet.utoronto.ca/index.php/Niagara_Quickstart#Limits>`__


LocalBackend
------------

The LocalBackend runs directly on the current Linux computer. This backend is specified by the configuration line ``backend = LocalBackend`` in the ``[jobdata]`` section.

**corecount**  `(integer) <https://toml.io/en/v1.0.0-rc.3#integer>`__ : Currently, corecount is ignored; LocalBackend only supports 'sequential' execution on a local server.


parameters
==========

**key**  `(array) <https://toml.io/en/v1.0.0-rc.3#array>`__: A nested array of `(strings) <https://toml.io/en/v1.0.0-rc.3#string>`__. Each string array consists of the name of a GTPEM ``ParameterSet`` and an appropriate target for that ``ParameterSet`` object, which may be a path to a file, or a single value.

The parameters section of the configuration allows you to create simulations with different combinations of inputs. The *key* name provided on each line is not used, and may be named however you like.

When creating directories, GTPEM will apply one parameter set from each key. A simulation will only take one parameter from each row. For example, the configuration below would result in the creation of 6 separate geotop simulations for each combination of soil and vegetation.

.. code-block:: toml

  soil = [['basicgeotop', 'path\to\soil_1.inpts'],
          ['basicgeotop', 'path\to\soil_2.inpts'],
          ['basicgeotop', 'path\to\soil_3.inpts']]
  vegetation = [['basicgeotop', 'path\to\vegetation_1.inpts'],
              ['basicgeotop', 'path\to\vegetation_2.inpts']]

If you choose to set up your own simulation directories, the``parameters`` configuration information is not used.

global
------
When a parameter set is under the parameters.global section, it will create simulation directories for each model specified in the `jobdata <Configuration.rst#jobdata>`__ section. This requries that the ``ParameterSet`` object is able to appropriately apply the parameters for each model.

model-specific
--------------
When a parameter subsection is given the same name as a model (such as parameters.geotop), it will only be applied when creating directories for that model. Most ``ParameterSet`` objects only work with a single model.

aggregation
===========
The aggregation section of the control file consists strictly of key - value pairs. Values can be either true or false depending on whether or not you want to run the named aggregator to collect model data.

**tar** `(boolean) <https://toml.io/en/v1.0.0-rc.3#boolean>`__ : If true, collects all simulation data in a tarfile. This has the tendency to create massive files, and is usually used for testing rather than for large ensemble simulations.

**soil_temperature**  `(boolean) <https://toml.io/en/v1.0.0-rc.3#boolean>`__ : If true, collects soil temperature data from the simulations and aggregates it into a single netCDF file.
