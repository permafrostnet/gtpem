============
Introduction
============

GTPEM is the Grid Toolkit for Permafrost Ensemble Modelling. It is designed to run permafrost ensemble simulations on high performance computing resources.

GTPEM does three things
========================
The core functionality of GTPEM can be broken down into:

1. Building model directories for ensemble simulations.
2. Managing the submission of simulations to computing resources (backends).
3. Aggregating simulation results to homogenize results from different models.

Depending on your use-case, you may only be interested in one of these functionalities. For instance, if you would rather create your own simulation directories by hand, you might use GTPEM only to submit and retrieve your jobs. Alternately, if you don't want any aggregation done on the resulting job directories, you might turn off all aggregators and deal with the model directories yourself.


GTPEM is extendable
===================

GTPEM uses python classes with common interfaces so that new models, backends and aggregation methods can be added without changes to the core code. If you want to add support for a new model, its as easy as writing a few new classes with the appropriate methods to run the model.

Models
======
GTPEM currently supports the `GEOtop <https://github.com/geotopmodel/geotop/tree/se27xx>`_ and `CLASSIC <https://cccma.gitlab.io/classic_pages/model/>`_ models. 

Backends
========
GTPEM currently has backend support for the `Compute Canada Niagara Grid <https://docs.computecanada.ca/wiki/Niagara>`_, or on a local machine. If you are interested in contributing to add support for another :ref:`contributing<page-contributing>`.

GTPEM Glossary
==============

GTPEM uses the following terminology:

General
-------

**model**: piece of software that simulates the behaviour of the real world

**simulation**: Single instance of a model execution intended to yield output

**run**: Collection of simulations that you want to execute at the same time

**job**: Collection of simulations that you submit to a cluster as an atomic item of work.

**forcing**: Time-varying input to a model; typically meteorological data.

**parameter**: Time-invariant input to a model or other property of that model. GTPEM does not make the distinction between measurable inputs (e.g. initial conditions, soil profile) and model parameters (e.g. physical constants, tuning parameters).
