Stepwise Spinup
===============

GTPEM includes a module for stepwise model spin-up (inspired by GEOtop). In this module, successive subsets of the soil column are spun up over a number of simulation periods. In simulation periods where the column depth is greater than the previous one, initial values are extrapolated from the previous simulation's run-averaged values and/or final model state. 

the Spinner
-----------
Each model capable of the stepwise spin-up must have an implementation of the ``Spinner`` abstract class.
This provides a common interface that is used to perform the spinup. It defines the following methods:

- **Set up**: back up any files, create any temporary files that are needed
- **Set simulation** times: Edit model configuration to only run between certain dates
- **Set model column depth**: Edit model configuration to 
- **Extrapolate**: extrapolate initial conditions from the spun-up depth to an arbitrary, greater depth
- **Run model**: run model with curent configuration
- **Save model results**: make a backup of the simulation results from previous run
- **Save model state**: make a backup only of the end-of-simulation model state
- **Tear down**: restore the model directory to its initial state, removing/renaming any temporary files 


Routine
-------
The full stepwise spinup routine consists of 6 steps:

1. Set up
2. Set model column depth and start/end times
3. Run model
4. (optional) save model state and/or results
5. (If more simulation periods remain), adjust column depth and time bounds, re-initialize initial conditions, extrapolating below previous spin-up depth if necessary and return to step 2. (If no more simulation periods remain) Move on to step 6
6. Tear down 


Command-line
------------
The stepwise spinup module can be accessed from the command line using the ``ssu`` command-line utility that is installed with GTPEM. The entire spinup routine can be run from the command line.

.. code-block:: bash
    ssu spinup -m CLASSIC --file myspinup.toml -K binary=/path/to/CLASSICbinary -K container=/path/to/container.simg /path/to/directory


This requires the existence of a TOML file providing the parameters for the simulation periods. It generally uses GEOtop-like key values:

.. code-block:: toml 
    InitInNewPeriods = 1  # Integer, Either 1 or 2
    NumSimulationTimes = [ 5, 20 ]  # TOML array of integers. Number of times to run model in each simulation period
    SpinupLayerBottom = [ 10, 15 ]  # TOML array of integers. Lowest layer to spin up in each simulation period 
    InitDate = [ 1980-01-01T00:00, 1980-01-01T00:00 ]  # TOML array of dates. Start date for each simulation period
    EndDate = [ 1985-05-27T07:32, 1995-12-31T00:00 ]  # TOML array of dates. End date for each simulation period
    SaveModelState = [ true, true ]  # TOML array of booleans. Whether or not to save model state at end of each run
    SaveModelResults = [false, true] # TOML array of booleans. Whether or not to back-up model results at end of each run


Individual parts of the stepwise spin-up can also be accessed

.. codeblock:: bash
    ssu run -m CLASSIC -K binary=/path/to/binary -K /path/to/directory
    ssu init -i 1 -d 20 -m CLASSIC .
    ssu save --state --results


