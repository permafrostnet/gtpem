Troubleshooting
---------------


Q: My jobs get cancelled by the scheduler

A: Have you chosen an appropriate wall time in the config file? (Backend.x.duration). 

Q: My jobs get killed (Singal 11)

A: Are the directories writeable?  On Niagara, home directories are not writable by compute nodes.
