.. _page-contributing:

=====================
Contributing to GTPEM
=====================
This information can also be found on the `GitLab Repository <https://gitlab.com/permafrostnet/gtpem/-/blob/master/CONTRIBUTING.md>`_.

Recommenations for Contributing
===============================

- Use `numpy-style <https://numpydoc.readthedocs.io/en/latest/format.html>`__ docstrings so that documentation can be generated automatically using sphinx.

- Update documentation as necessary in the ``/docs/source`` folder. Documentation is written in RestructuredText and compiled with Sphinx.

- Write unit tests where you can.

- Use the `gitlab flow <https://docs.gitlab.com/ee/topics/gitlab_flow.html>`_ branching strategy on GitLab: 

   1. Create a feature branch to add a new feature: feature branches should be short-lived.
   2. Create a pull request to merge back into ``master``. Invite others to review it.
   3. working versions are pushed to ``production``.

- When adding a new model, backend, aggregator, or parameter set, please aim to keep the configuration key names as similar as possible to existing ones.


Adding content
==============

Most contributions will involve creating new classes that inherit from one of the abstract base classes in ``gtpem.models.template``. This allows you to represent new models, new ways of creating ensemble simulations from parameter combinations, and new ways of aggregating results for models.

Adding a new model
------------------
1. Create a new folder in ``gtpem.models`` with an ``__init__.py``. The name of your model 
2. Implement ``ModelDirectory`` and ``Simulation`` subclasses for your model, including the necessary interfaces and ``MODEL`` class attribute which should be consistent with the folder name
3. Add your model to the library so it can be found (``gtpem.models.library.py``)

Adding a new aggregator
-----------------------
1. Create a generic aggregator class in ``gtpem.models.generic``, give it a unique ``NAME`` class attribute
2. For each model you want to support the aggregator, create a subclass, ensuring that the model name is contained within the ``SUPPORTS`` attribute list
3. Register the aggregator in the library (``gtpem.models.library.py``)

Adding a new parameter set
--------------------------
1. For the model you want to Implement a subclass of the ``ParameterSet`` object
2. Register the parameter set in the library (``gtpem.models.library.py``)


When adding a new model, backend, aggregator, or parameter set, please aim to keep the configuration key names as similar as possible to existing ones.


Description of GTPEM abstract classes
-------------------------------------

``PemConfig``: A representation of the configuration necessary for a job. Created from a \\*.TOML control file. Contains information required for the job, and environment configuration necessary to run the various models.

``ModelDirectory``: Represents the collection of files used for a simulation.

``Simulation``: Representation of a simulation, comprising a ModelDirectory along with methods necessary to run the model from the command line.

``SimulationAbstraction``:  A model-independent description of a simulation, contains information on which model to use, which forcing data to use, and which abstracted parameter set to apply. Contains no model-specific information and can be interpreted by ModelDirectory objects to create model-specific directories.

``ParameterSet``: ParameterSet objects are used to generate simulation directories by modifiying model parameters. A ``ParameterSet`` is initialized with a single argument representing the parameters of interest. This could be a constant, a JSON array, or a filepath, depending on the level of complexity that is needed. A ``ParameterSet`` that is initialized with a single value might be used to set a single parameter; On the other hand, initializing by pointing to a file allows for more complex configurations or parameter sets that represent well-described terrain types or vegetation classes.