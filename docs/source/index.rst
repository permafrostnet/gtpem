.. gtpem documentation master file, created by
   sphinx-quickstart on Tue Aug 25 11:01:22 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gtpem's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Introduction
   Installation
   Usage
   Configuration
   Spinup
   Contributing
   ModelSpecific
   FAQ
   Autodoc


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
