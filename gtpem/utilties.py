from collections.abc import Iterable
import six


def iterable(arg):
    return (
        isinstance(arg, Iterable)
        and not isinstance(arg, six.string_types)
    )
    
    
def listify(listlike):
    new_list = []
    for e in listlike:
        if iterable(e):
            new_list.append(listify(e))
        else:
            new_list.append(e)
            
    return new_list
