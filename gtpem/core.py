
import itertools
import netCDF4 as nc
import tomlkit
import logging
import sys
import traceback
import tempfile

from gtpem.utilties import listify


logger = logging.getLogger(__name__)


class PemConfig:
    """
    Class to handle the control file.
    used as a wrapper in case the underlying structure changes

    Parameters
    ----------
    tomlfile : path
        path to .toml file with configuration information
    """

    def __init__(self, tomlfile):
        logger.debug(f"Creating PemConfig object from {tomlfile}")
        self._file = tomlfile
        self._config = self.__read_config(tomlfile)
        
    def __str__(self):
        return "Configuration file"

    def __read_config(self, tomlfile):
        with open(tomlfile, encoding='utf8') as stream:
            config = tomlkit.parse(stream.read())

        return config
    
    @classmethod
    def empty(cls):
        doc = tomlkit.document()
        jobdata = tomlkit.table()
        models = tomlkit.table()
        backend = tomlkit.table()
        parameters = tomlkit.table()
        aggregation = tomlkit.table()
        doc["jobdata"] = jobdata
        doc["models"] = models
        doc["backend"] = backend
        doc["parameters"] = parameters
        doc["aggregation"] = aggregation

        with tempfile.NamedTemporaryFile('w', delete=False) as f:
            f.write(tomlkit.dumps(doc))
            f.seek(0)
        self = cls(f.name)
        return self  # a = PemConfig.empty(); a._config

    @property
    def config(self):
        return self._config

    def get_reanalyses(self) -> "list":
        """
        returns a list of reanalysis files
        """
        return self.get('jobdata', 'forcing')

    def get_sites(self) -> "dict[str, str]":
        """
        An dictionary of sites, keys are the reanalyses
        """
        forcing_files = self.get_reanalyses()
        forcings = [nc.Dataset(f) for f in forcing_files]
        sites = {f.filepath():
                 nc.chartostring(f['station_name'][:]) for f in forcings}

        # colnames = [path.basename(f) for f in forcing_files]
        return sites

    def get_models(self) -> "list":
        models_to_run = self.get('jobdata', 'models')
        return models_to_run

    def get_working_directory(self) -> "str":
        v = self.get('jobdata', 'workingdirectory')
        return v

    def get_job_name(self) -> "str":
        v = self.get('jobdata', 'title')
        return v

    def get_aggregators(self) -> "list":
        v = [key for key in self.get('aggregation')
             if self.get('aggregation', key)]
        return v
    
    def get_template_directory(self, model) -> "str":
        v = self.get('models', model, 'template')
        return v
    
    def get_model_parameters(self, model: str) -> list:
        if "global" in self.get("parameters").keys():
            generic = list(self.get('parameters', 'global').values())
        else:
            generic = []
        model_specific = list(self.get('parameters', model).values())
        all_parameters = generic + model_specific
        return listify(all_parameters)

    def naive_combination(self, model: str) -> list:
        all_parameters = self.get_model_parameters(model)
        v = [p for p in itertools.product(*all_parameters)]

        return v

    def get(self, *args, datatype=None, fallback=None):
        """Method to access the TOML config stores in self._config

        By using this method instead of cirectly accessing _config, we
        are able to do some error handling in a central place. In
        particular, a nicer message is printed out if a key is missing
        in a file.  Also, we can optionally check for TOML type (by
        passing in a TOML Item class as datatype), allowing for some
        basic config file typechecking as well.

        """
        fullkey = '.'.join(args)
        val = self._config
        try:
            # Loop through nested TOML keys
            for key in args:
                val = val[key]
        except tomlkit.exceptions.NonExistentKey as e:
            if fallback is not None:
                # On error, print stack trace, full nested key, and TOML filename
                traceback.print_exc()
                print(f"\nERROR: Config item '{fullkey}' not found in '{self._file}'")
                sys.exit(1)
            else:
                return fallback
        if datatype:
            # Optionally handle TOML config item typechecking
            #
            # First, get passed in datatype (can be a string, like
            # 'String', or a full class like tomlkit.items.String)
            if type(datatype) == str:
                typeclass = getattr(tomlkit.items, datatype, None)
                typestr = datatype
            else:
                typeclass = datatype
                typestr = datatype.__name__
            # assert should be true unless an incorrect class is
            # passed in.  For example 'string' exists as a Python builtin,
            # but is never the type of a TOML config item.  Instead,
            # 'String', as in tomlkit.items.String, could be used.
            assert(typeclass in tomlkit.items.Item.__subclasses__())
            if type(val) != typeclass:
                print(f"CONFIG ERROR: '{fullkey}' in '{self._file}' should be a {typestr}")
                sys.exit(1)
        return val
