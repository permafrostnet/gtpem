import re
import shutil


from abc import ABC, abstractmethod
from os import path
from pathlib import Path
from datetime import datetime
from pandas import read_csv
from typing import Optional

"""
The abstract classes ModelWrapper and ModelDirectory are used as templates
to create objects to interact with models
"""


class Simulation(ABC):
    """
    A composition of a ModelDirectory with model information .
    
    Must be implemented for each model

    Parameters
    ----------
    cfg : PemConfig
        GTPEM configuration object
    
    dirpath : str
        Path to model directory
    """

    MODEL = None

    def __init__(self, cfg, dirpath):
        self.cfg = cfg
        self.directory : ModelDirectory
        
    def run(self, *args, **kwargs):
        """
        Launch the simulation.

        Returns
        -------
        bool
            Whether the simulation completed successfully

        """
        pass

    def get_directory(self) -> "ModelDirectory":
        return self.directory

    @abstractmethod
    def command(self, directory) -> str:
        """
        return the shell command necessary to run the model
        on the specified directory

        Returns
        -------
        str
            The shell command to run the simulation
        """
        pass

    @abstractmethod
    def successful_run(self) -> bool:
        """
        Returns
        -------
        bool
            Whether the simulation completed successfully
        """
        return False

    @abstractmethod
    def failed_run(self) -> bool:
        """
        Returns
        -------
        bool
            Whether the simulation failed to complete successfully
        """
        return False

    def reset_status(self):
        """Reset all FAILURE or SUCCESS flags
        """
        success = Path(self.directory.dirpath, "_SUCCESSFUL_RUN")
        
        if success.is_file():
            success.unlink()
        
        failed = Path(self.directory.dirpath, "_FAILED_RUN")
        
        if failed.is_file():
            failed.unlink()


    def __str__(self):
        return f"{type(self).__name__} /{path.basename(self.directory.dirpath)}"
        

class ModelDirectory(ABC):
    """
    An abstraction of all files necessary to run a simulation.

    A class to encapsulate the model directory, and point to any other necessary
    files. Can be built from a SimulationAbstraction.

    Must be implemented for each model

    Parameters
    ----------
    dirpath : str
        Path to model directory or primary configuration file.
    """
    MODEL = None

    def __init__(self, dirpath: str):
        self.dirpath = dirpath

    def validate(self) -> bool:
        return self.is_valid_directory(self.dirpath)
    
    @staticmethod
    @abstractmethod
    def is_valid_directory(path) -> bool:
        """
        Check whether the directory is valid (e.g. has all necessary inputs)
        This method is also used to identify directories

        Returns
        -------
        bool
            whether or not the directory is acceptable
        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def from_simulation_abstraction(self, cfg, sim):
        """
        Build a directory using information in cfg and simulation abstraction
        objects
        
        Parameters
        ----------
        cfg : PemConfig
            GTPEM configuration object.
        sim : SimulationAbstraction
            GTPEM SimulationAbstraction object
        """

        raise NotImplementedError

    @staticmethod
    def copy_template_directory(cfg, sim) -> str:
        """
        Parameters
        ----------
        cfg : PemConfig
            GTPEM configuration object
        sim : SimulationAbstraction
            GTPEM SimulationAbstraction object

        Returns
        -------
        str
            Path to new directory
        """
        td = cfg.get_template_directory(sim.param['model'])
        jd = cfg.get_job_name()
        wd = cfg.get_working_directory()
        dst = path.join(wd, jd, sim.directory_name)
        if Path(dst).is_dir():
            dst = re.sub(".{6}$", datetime.now().isoformat()[-6:], dst)
        model_dirpath = shutil.copytree(td, dst)

        return model_dirpath

    def apply_parameters(self, sim, cfg):
        for parameter_set in sim.get_parameters():
            parameter_set.apply(self, cfg)


class ModelAggregator(ABC):
    """
    A class that aggregates simulation results.

    Must be implemented for each unique model and each aggregation type. Aggregation
    types include: 'soil_temperature', 'parameters',

    Parameters
    ----------
    modeldirectory : ModelDirectory
        GTPEM ModelDirectory object
    """
    NAME = None
    SUPPORTS = list()

    def __init__(self, modeldirectory: ModelDirectory):
        self.directory = modeldirectory
        self.info = self.read_manifest()
        self.validate()

    def validate(self):
        """ Tests whether the ModelAggregator is compatible with the directory """
        if not (self.directory.MODEL in self.SUPPORTS):
            raise TypeError(f"{self.NAME} aggregator does not support {self.directory.MODEL}")

    @abstractmethod
    def aggregate(self, target: str):
        """
        Append model directory to aggregated file

        Parameters
        ----------
        target : str
            Path to existing aggregation file created by the aggregator object.
        """
        pass

    @staticmethod
    @abstractmethod
    def new(target: str):
        """ Create new aggregation file.
        
        Parameters
        ----------
        target : str
            path to new aggregation file to be created.
        """
        pass

    def read_manifest(self):
        """ Get directory manifest information from parent directory """
        manifest_csv = Path(Path(self.directory.dirpath).parent, "folder_manifest.csv")

        if manifest_csv.is_file():
            manifest = read_csv(manifest_csv)
            info = manifest[manifest["directory"] == self.directory.dirpath.name]
            
            if info.shape[0] == 0:
                info = None

        else:
            info = None
        
        return info


class ParameterFactory(ABC):
    NAME = None
    SUPPORTS = list()

    def __init__(self, target):
        self._target = target

    @abstractmethod
    def generate(self, site: "Optional[str]" = None) -> "list[ParameterSet]":
        pass

    def generate_text(self, site: str) -> "list[tuple[str, str]]":
        parameters = self.generate(site)
        return [(p.NAME, p.filepath) for p in parameters]



class ParameterSet(ABC):
    """
    A class that aggregates simulation results.

    Must be implemented for each unique model and each aggregation type. Aggregation
    types include: 'soil_temperature', 'parameters',

    Parameters
    ----------
    modeldirectory : ModelDirectory
        GTPEM ModelDirectory object
    """

    SUPPORTS = list()

    def __init__(self, filepath):
        self._filepath = filepath
    
    @abstractmethod
    def apply(self, model_directory: ModelDirectory, cfg=None):
        """
        Write parameter information to a model directory

        Parameters
        ----------
        model_directory : ModelDirectory
            a GTPEM ModelDirectory object of the same model type
        """
        pass

    @classmethod
    @property
    @abstractmethod
    def NAME(cls) -> str:
        raise NotImplementedError

    @property
    @abstractmethod
    def filepath(self) -> str:
        return self._filepath
