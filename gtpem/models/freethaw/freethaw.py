import logging
import netCDF4 as nc
import numpy as np

from pathlib import Path

from gtpem.models.generic import GroupedNetCDFAggregator
from gtpem.models.template import ModelAggregator
from gtpem.models.template import ModelDirectory, Simulation, ParameterSet
from gtpem.core import PemConfig
from gtpem.models.freethaw.simfile import Simfile

logger = logging.getLogger(__name__)


class FreeThawSimulation(Simulation):
    """ Simulation is a composition of a directory and model information """
    MODEL = "freethaw"

    def __init__(self, cfg, dirpath):
        self.cfg = cfg
        self.dirpath = dirpath
        self.directory = FreeThawDirectory(dirpath)

    @classmethod
    def from_directory(cls):
        pass
     
    def command(self):
        simfile_name = self.directory.get_simulation_file().name
        library = self.cfg.get("models", "freethaw", "lib")
        library_target = self.directory.resource_path()
        
        if Path(self.cfg.get("models", "freethaw", "container")).is_file():
            target = self.cfg.get("models", "freethaw", "container")     
        else:
            target = "docker://geocryology/oms:0.3_py3-hpc"

        # -c contain (don't make changes to user home directory when linking to /opt/.oms)
        # --pwd "/"  Start the container in "/" so that entrypoint.sh is accessible
        # -B $({self.dirpath}):/work mount the simulation directory to "/work" (expected by OMS)
        # -B {library}:{library_target} mount the freethaw java libraries
        cmd = rf'singularity run --pwd "/" -c -B {self.dirpath}:/work -B {library}:{library_target} {target} simulation/{simfile_name}'

        return cmd

    def clear_directory(self):
        # Delete any existing output files 
        pass

    def successful_run(self) -> bool:
        return self.directory.output_exists()

    def failed_run(self) -> bool:
        logger.warning("Check for failed FreeThaw run has not been implemented. Defaulting to False.")
        return False


class FreeThawSimulation2(FreeThawSimulation):
    MODEL = "freethaw2"

    def __init__(self, cfg, dirpath):
        self.cfg = cfg
        self.dirpath = dirpath
        self.directory = FreeThawDirectory2(dirpath)

    @classmethod
    def from_directory(cls):
        pass
     
    def command(self):
        simfile = self.directory.get_simulation_file()
        library = self.cfg.get("models", "freethaw", "lib")
        library_target = self.directory.resource_path()
        
        if Path(self.cfg.get("models", "freethaw", "container", fallback="")).is_file():
            target = self.cfg.get("models", "freethaw", "container")     
        else:
            target = "docker://geocryology/oms:0.3_py3-hpc"

        projpath = self.cfg.get("models", "freethaw", "projpath")
        # -c contain (don't make changes to user home directory when linking to /opt/.oms)
        # --pwd "/"  Start the container in "/" so that entrypoint.sh is accessible
        # -B $({self.dirpath}):/work mount the simulation directory to "/work" (expected by OMS)
        # -B {library}:{library_target} mount the freethaw java libraries
        cmd = ";".join([
            f'rm -f {Path(self.dirpath, "_SUCCESSFUL_SIMULATION")}',
            f'rm -f {Path(self.dirpath, "_FAILED_SIMULATION")}',
            f'echo `date` > {Path(self.dirpath, "_INCOMPLETE_SIMULATION")}',
            f'cd {projpath}',
            (
                rf'singularity run --pwd "/" -c -B {projpath}:/work -B {library}:{library_target} {target} {Path(simfile).relative_to(projpath)}'
                f'&& echo `date` > {Path(self.dirpath, "_SUCCESSFUL_SIMULATION")}'
                f'|| echo `date` > {Path(self.dirpath, "_FAILED_SIMULATION")}'
            ),
            f"rm -f {Path(self.dirpath,'output','_Sim_complete_all_variables_0000.nc')}",
            f'rm -f {Path(self.dirpath, "_INCOMPLETE_SIMULATION")}',
        ])

        return cmd

    def clear_directory(self):
        # Delete any existing output files 
        pass

    def successful_run(self) -> bool:
        return self.directory.output_exists()

    def failed_run(self) -> bool:
        logger.warning("Check for failed FreeThaw run has not been implemented. Defaulting to False.")
        return False


class FreeThawDirectory(ModelDirectory):
    """
    
    Parameters
    ----------
    dirpath : str
        Path to model directory or primary configuration file.

    Conditions / Assumptions
    0. Directory must have a ./simulation subdirectory
    1. Directory must have a single *.sim file in the simulation/ subdirectory
    2. file paths in the *.sim file are given relative to the project directory (called $home: e.g. $home/data/)
    3. The model netcdf grid is already built

    
    """
    MODEL = "freethaw"

    def __init__(self, dirpath: str):
        super().__init__(dirpath)
        self.validate()
        simfile_path = self.get_simulation_file()
        self.sim = Simfile(simfile_path, self.dirpath)
    
    @staticmethod
    def is_valid_directory(dirpath) -> bool:
        # must have simulation directory
        # 
        """
        Check whether the directory is valid (e.g. has all necessary inputs)
        This method is also used to identify directories

        Returns
        -------
        bool
            whether or not the directory is acceptable
        """
        if not Path(dirpath, "simulation").is_dir():
            logger.debug("Missing simulation directory")
            return False

        if not FreeThawDirectory.single_simulation(dirpath):
            logger.error("Multiple *.sim files found in directory")
            return False
        
        return True
        
    def get_simulation_file(self):
        simfile = list(Path(self.dirpath, "simulation").glob("*sim"))[0]
        return simfile

    @staticmethod
    def single_simulation(dirpath):
        """ check that there is a single simulation in the directory"""
        n_sim_files = list(Path(dirpath, "simulation").glob("*sim"))
        
        return len(n_sim_files) == 1

    def output_file(self) -> str:
        output_file = self.sim.get('pathOutput')
        if not output_file:
            output_file = self.sim.get("pathOutputAllVariables")
        
        return output_file

    def output_exists(self) -> bool:
        return Path(self.output_file()).is_file()

    def grid_exists(self) -> bool:
        """ check for the existence of a model grid """
        grid_file = self.sim.get('pathGrid')
        return Path(grid_file).is_file()

    def resource_path(self):
        """Get path to solver and resource files """
        return "/work/lib"  # TODO: read this from the sim file

    @classmethod
    def from_simulation_abstraction(self, cfg, sim):
        raise NotImplementedError


class FreeThawDirectory2(FreeThawDirectory):
    MODEL = "freethaw2"

    @staticmethod
    def is_valid_directory(dirpath) -> bool:
        if not len(list(Path(dirpath).glob("*.sim"))) == 1:
            logger.debug("Missing simulation file")
            return False
        
        return True

    def get_simulation_file(self):
        simfile = list(Path(self.dirpath).glob("*.sim"))[0]
        return simfile


class FreeThawAggregator(GroupedNetCDFAggregator):
    """ Aggregate all freethaw output into a single file

    Parameters
    ----------
    SoilTemperatureAggregator : [type]
        [description]

    Returns
    -------
    [type]
        [description]
    """
    NAME = 'freethaw_all'
    SUPPORTS = ['freethaw']
    INDEPENDENT_VARS = ['control_volume', 'time', 'z']  # Not simulation-dependent
    NAMELENGTH = 255

    def __init__(self, modeldirectory):
        super().__init__(modeldirectory)
        self.dir = modeldirectory
        self.dirname = Path(modeldirectory.dirpath).name
        self.output = self.read_output_data()
        
      #  self.validate_output()  # TODO: delete if not doing much
        

    def validate_output(self):
        if not self.output_exists():
            raise FileNotFoundError(f"Could not aggregate. File {self.dir.output_file()} was not found.")
    
    def output_exists(self):
        """
        checks whether the output data file exists or not
        """
        return Path(self.get_data_file()).is_file

    def aggregate(self, target):
        if not Path(target).is_file():
            self.new(target)

        with nc.Dataset(target, 'a') as rootgrp:
            if rootgrp.variables == {}: 
                self.new_freethaw_group(rootgrp)

            i = self.write_simulation_name(rootgrp, self.dirname)
            self.add_output_variables(rootgrp, self.output, i)

    def new_freethaw_group(self, rootgrp):

        self.initialize_group(rootgrp, self.output)
        self.write_independent_variables(rootgrp, self.output)
        
        return rootgrp

  #  @staticmethod
  #  def new(target):
  #      with nc.Dataset(target, 'w') as rootgrp:
  #          self.fill_generic_metadata(rootgrp)

    def get_output_file(self):
        """ Get path to freethaw output file """
        dfile = self.dir.output_file()
        return dfile
    
    def read_output_data(self):
        """
        Read output data files from current geotop simulation directory.
        """
        dfile = self.get_output_file()
        output = nc.Dataset(dfile)
        return output
      
    def initialize_group(self, group, src):
        """
        Add required dimensions and variables to a group from existing file, adding a simulation dimension

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            an open netcdf group or dataset
        src : netCDF4._netCDF4.Dataset
            FreeThaw1d output file

        """
        _ = group.createDimension("nchars", self.NAMELENGTH)
        _ = group.createDimension("simulation", None)  # We don't know how many simulations there will be.
        
        for dim in src.dimensions.values():
            _ = group.createDimension(dim.name, dim.size)
        
        for var in src.variables:
            
            if var in self.INDEPENDENT_VARS:
                new_dims = [d.name for d in src[var].get_dims()]
            else: 
                new_dims = ['simulation',] + [d.name for d in src[var].get_dims()]
    
            dtype = src[var].datatype
            new_var = group.createVariable(var, dtype, new_dims)
            copy_nc_attrs(src[var], new_var)
        
        sim = group.createVariable('simulation', "S1", ('simulation', 'nchars'))
        sim.long_name = "simulation name"

        return group

    def write_independent_variables(self, group, src):
        """ Write independent variables from src into a new group """
        for var in self.INDEPENDENT_VARS:
            group[var][:] = src[var][:]      

    def write_simulation_name(self, group, name: str) -> int:
        """
        Write the simulation directory name to the next available simulation variable
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            An open netcdf group or dataset
        name : str
            Name of simulation (obtained from directory name)

        Returns
        -------
            int : index of simulation
        """
        i = self.next_simulation_index(group)
        # name_arr = nc.stringtochar(np.array(name, dtype='S3'))
        group['simulation'][i, :] = nc.stringtoarr(name, self.NAMELENGTH)
        return(i)

    @staticmethod
    def next_simulation_index(group) -> int:
        """
        Get the next simulation index . This is used to determine where the next array slice
        should be written.

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset

        Returns
        -------
            int : The index of the next empty array slice along the simulation dimension
        """
        return len(group['simulation'])

    def add_output_variables(self, group, src, i):
        """
        Write simulation-specific variables from src into new group "

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            an open netcdf group or dataset to be written
        sim : int
            index along the simulation dimension where array should be written
        """

        for var in set(src.variables).difference(self.INDEPENDENT_VARS):
            data = src[var][:]
            group[var][i, :] = data

        return group
        
def add_simulation_dimension(array):
    """ Add dimension to data array corresponding to simulation index """
    return array[np.newaxis, :]
    

def copy_nc_attrs(src, dst):
    for attr in src.ncattrs():
        dst.setncattr(attr, src.getncattr(attr))
