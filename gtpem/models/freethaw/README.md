## Running FreeThaw1d on Compute Canada HPC resources

You need to use a patched version of the OMS3 container to run it on Niagara.  This is because Niagara uses Singularity instead of Docker, and access to the `/root` folder is impossible without root access on the host machine. OMS keeps some important files in `/root/.oms` and so this makes things incompatible on Niagara.

### Read more

- The [Relevant issue](https://github.com/sidereus3/oms-docker/issues/2) on the official OMS3 docker repository
- Use a [patched OMS3 repository](https://github.com/nicholas512/oms-docker/releases/tag/OMS-Niagara): 
- Use the Docker container at `docker://geocryology/oms:0.3_py3-hpc`

