import logging
import re

from pathlib import Path

logger = logging.getLogger(__name__)

class Simfile:
    """ A reader for OMS *.sim files """
    
    def __init__(self, filepath, home):
        """
        filepath : str
            path to *.sim file
        home : str or Path
            path to the simulation directory
        """
        self.filepath = filepath
        self.home = str(Path(home).resolve())
        self.dict = self.parse(filepath)

    def extract_home(self):
        pass

    def parse(self, filepath):
        logger.debug(f"Reading {filepath}")
        with open(filepath, errors='ignore') as file:
            lines = file.readlines()
        
        params = dict()

        for line in lines:
            definition = re.compile(r"^def\s*(\w*)\s*=\s*(.*)$")
            match = definition.search(line)
            if match:
                key = match[1]
                val = match[2]
                params[key] = val
        
        return params

    def get(self, property, raw=False):
        val = self.dict.get(property)
        if val:
            if not raw:
                val = self.simplify(val)
            return val
            
    def simplify(self, val):
        val = re.sub(r'"(.*)"', r"\1", val)  # strip quotes
        val = re.sub(r'\$home', self.home, val)
        val = re.sub(r'\$oms_prj', self.home, val)
        return val
