from gtpem.models import generic
from gtpem.models.geotop import geotop
from gtpem.models.testmodel import testmodel
from gtpem.models.classic import classic
from gtpem.models.freethaw import freethaw 

library = {'geotop': {'model': geotop.GeotopSimulation,
                      'directory': geotop.GeotopDirectory,
                      'aggregate': {'tar': generic.TarAggregator,
                                    'soil_temperature': geotop.GeotopSoilTemperatureAggregator,
                                    'liquid_water': geotop.GeotopGroupedNCLiquidWaterAggregator,
                                    'water_pressure': geotop.GeotopGroupedNCWaterPressureAggregator,
                                    'thaw_depth': geotop.GeotopDepthOfThawAggregator,
                                    'air_temperature': geotop.GeotopAirTempAggregator,
                                    'snow_depth': geotop.GeotopSnowDepthAggregator,
                                    'swe': geotop.GeotopSnowWaterEquivalentAggregator
                                    },
                      'parameters': {geotop.BasicGeotopParameter.NAME: geotop.BasicGeotopParameter
                                     }
                      },
           'test': {'model': testmodel.TestCmd,
                    'directory': testmodel.TestDirectory,
                    'aggregate': {'tar': generic.TarAggregator,
                                  'soil_temperature': None},
                    'parameters': {testmodel.TestPauseParameter.NAME: testmodel.TestPauseParameter
                                   }
                    },
           'generic': {'aggregate': {'tar': generic.TarAggregator,
                                     'soil_temperature': generic.GroupedNetCDFAggregator,
                                     'snow': geotop.GroupedNetCDFAggregator,
                                     'ice_content': geotop.GroupedNetCDFAggregator,
                                     'liquid_water': geotop.GroupedNetCDFAggregator,
                                     'water_pressure': geotop.GroupedNetCDFAggregator,
                                     'freethaw_all': freethaw.FreeThawAggregator,
                                     'thaw_depth': geotop.GroupedNetCDFAggregator,
                                     'air_temperature': geotop.GroupedNetCDFAggregator,
                                     'snow_depth': geotop.GroupedNetCDFAggregator,
                                     'swe': geotop.GroupedNetCDFAggregator
                                     }
                       },
            
            'classic': {'model': classic.ClassicSimulation,
                        'directory': classic.ClassicDirectory,
                        'aggregate': {'tar': generic.TarAggregator,
                                      'soil_temperature': classic.ClassicSoilTemperatureAggregator,
                                      'liquid_water': classic.ClassicLiquidWaterAggregator},
                         'parameters': {classic.ClassicTomlInit.NAME: classic.ClassicTomlInit}},
            
            'freethaw': {'model': freethaw.FreeThawSimulation, 
                         'directory':freethaw.FreeThawDirectory,
                         'aggregate': {'tar':generic.TarAggregator,
                                       'freethaw_all': freethaw.FreeThawAggregator
                                        }},

            'freethaw2': {'model': freethaw.FreeThawSimulation2, 
                         'directory':freethaw.FreeThawDirectory2,

           }}


def valid_models():
    all_models = list(library.keys())
    all_models.remove('generic')

    return all_models


"""
Other ways to handle this:

1. get list of subclasses for the abstract base classes

from gtpem.models.generic import *
from gtpem.models.geotop import *
from gtpem.models.testmodel import *
from gtpem.models.template import *
[x for x in ModelDirectory.__subclasses__() if x.MODEL == 'geotop']

2. Library singleton object with common interface (so things don't have to change elsewhere?)
class Library:
    __instance = None

    def __init__(self):
        pass

    def get_simulation(self, modelname):
        pass
    
    def get_directory(self, modelname):
        pass

3. parameters and aggregators would be different

-> parameters and aggregators have a SUPPORTS attribute instead of a MODEL attribute, then you could test
[x for x in ParameterSet.__subclasses__() if x.NAME == 'tar' and 'geotop' in x.SUPPORTS]
"""
