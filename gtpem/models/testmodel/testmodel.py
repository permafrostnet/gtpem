import os
from string import Template

from gtpem.models.template import ModelDirectory, Simulation, ParameterSet


class TestCmd(Simulation):
    
    MODEL = "test"

    def __init__(self, cfg, dirpath):
        super().__init__(cfg, dirpath)
        self.directory = TestDirectory(dirpath)

    def command(self):
        binary = self.cfg.get('models', 'test', 'binary')
        return f"cd {self.directory.dirpath} && {binary} > output.txt && date > success.txt"

    def successful_run(self):
        return os.path.isfile(os.path.join(self.directory.dirpath, "success.txt"))


class TestDirectory(ModelDirectory):

    MODEL = "test"

    def __init__(self, dirpath):
        super().__init__(dirpath)
        self.validate()
        self.read_parameters()

    def validate(self):
        return self.is_valid_directory(self.dirpath)

    @staticmethod
    def is_valid_directory(dirpath):
        f = os.path.join(dirpath, "config.txt")
        return os.path.isfile(f)

    def create_directory(self):
        os.mkdir(self.sim_dir)

    @classmethod
    def from_simulation_abstraction(cls, cfg, sim):
        dirpath = ModelDirectory.copy_template_directory(cfg, sim)
        modeldir = cls(dirpath)
        modeldir.write_forcing(cfg, sim)
        modeldir.apply_parameters(sim, cfg)
        modeldir.read_parameters()
        return modeldir

    def write_forcing(self, cfg, sim):
        self.forcing = sim.get_forcing()
        self.site_name = sim.get_site_name()

    def read_parameters(self):
        config_fname = os.path.join(self.dirpath, "config.txt")
        # Only read parameters if the config file exists, otherwise
        # leave them undefined.  A directory is first created in the
        # constructor call inside from_simulation_abstraction(), which
        # calls this function before before apply_parameters() gets
        # called.
        if os.path.isfile(config_fname):
            with open(config_fname, "r") as FILE:
                config = FILE.readlines()
            self.pause = int(config[0].strip().split("=")[1][1:-1])
            self.forcing = config[1].strip().split("=")[1][1:-1]

    def write_parameters(self):
        # Create the model's input config file using a python string Template
        with open(os.path.join(self.dirpath, "config.template"), "r") as FILE:
            cfg_template = Template(FILE.read())
        vars = {'pause': self.pause,
                'msg': f"{self.site_name} : {self.forcing}"}
        with open(os.path.join(self.dirpath, "config.txt"), "w") as FILE:
            FILE.write(cfg_template.substitute(vars))


class TestPauseParameter(ParameterSet):
    """
    Writes a pause value to a model config file. An example of a simple ParameterSet implementation
    ----------
    pause : int
        number of seconds to pause during model execution

    """
    NAME = "test-pause"
    SUPPORTS = ["test"]

    def __init__(self, pause):
        self.pause = pause

    @property
    def filepath(self):
        return self.pause
        
    def apply(self, model_directory, cfg=None):
        """[summary]

        Parameters
        ----------
        model_directory : ModelDirectory
            GTPEM ModelDirectory object where parameters will be written
        """
        model_directory.pause = self.pause
        model_directory.write_parameters()

    def __repr__(self):
        return f"TestPauseParameter({self.pause})"
