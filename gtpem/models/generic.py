import tarfile
from os import path
import netCDF4 as nc
from .._version import __version__
from datetime import datetime

from gtpem.models.template import ModelAggregator


class TarAggregator(ModelAggregator):
    NAME = "tar"
    SUPPORTS = ["geotop", 'classic', 'test']

    @staticmethod
    def new(target):
        f = tarfile.open(name=target, mode='x')
        f.close()
        
    def aggregate(self, target):
        with tarfile.open(name=target, mode='a') as f:
            arcname = path.basename(self.directory.dirpath)
            f.add(self.directory.dirpath, arcname=arcname)
        
    @staticmethod
    def generate_filename(output_path, name):
        return path.join(output_path, name + ".tar")


class GroupedNetCDFAggregator(ModelAggregator):
    SUPPORTS = [None]
    
    def aggregate(self, target):
        raise NotImplementedError("This must be implemented by a child class")
    
    @staticmethod
    def new(target):
        with nc.Dataset(target, 'w') as main:
            GroupedNetCDFAggregator.fill_generic_metadata(main)
    
    @staticmethod
    def fill_generic_metadata(rootgrp):
        rootgrp.comment = f"Created from GTPEM version {__version__}"
        rootgrp.date_created = f"{datetime.now().isoformat()}"
        rootgrp.source = "Simulated data"

    @staticmethod
    def generate_filename(output_path, name):
        return path.join(output_path, name + ".nc")
