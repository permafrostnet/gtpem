import logging
import numpy as np
import netCDF4 as nc
import pandas as pd
import re
import shutil
import subprocess

from datetime import datetime

from os import path, listdir, symlink, makedirs, remove
from pathlib import Path
from tempfile import TemporaryDirectory

from globsim.exporttools import globsim_to_geotop

from gtpem.models.generic import GroupedNetCDFAggregator
from gtpem.models.template import ModelDirectory, Simulation, ParameterSet, ParameterFactory
from gtpem.core import PemConfig
from gtpem.names import GEOTOP

try:
    from .DirectoryReader import DirectoryReader  # todo: better import
except ModuleNotFoundError:
    from gtpem.models.geotop.DirectoryReader import DirectoryReader
logger = logging.getLogger(__name__)


class GeotopSimulation(Simulation):
    """ Simulation is a composition of a directory and model information """
    MODEL = GEOTOP

    def __init__(self, cfg, dirpath):
        self.wrapper = GeotopWrapper(cfg)
        self.directory = GeotopDirectory(dirpath)

    @classmethod
    def from_directory(cls):
        pass
     
    def command(self):
        return self.wrapper.command(self.directory)

    def successful_run(self) -> bool:
        success = Path(self.directory.dirpath, "_SUCCESSFUL_RUN").is_file()
        logger.debug(f"_SUCCESSFUL_RUN flag{' ' if success else ' not '}found in GEOtop directory {self.directory.dirpath}")
        return success

    def failed_run(self) -> bool:
        failed = Path(self.directory.dirpath, "_FAILED_RUN").is_file()
        logger.debug(f"_FAILED_RUN flag{' ' if failed else ' not '}found in GEOtop directory {self.directory.dirpath}")
        return failed


class GeotopWrapper:
    """
    GTPEM wrapper for the GEOtop hydrological model
    """

    MODEL = GEOTOP

    def __init__(self, cfg):
        self.cfg = GeotopConfig.from_config(cfg)

    def run(self, directory):
        binary = self.cfg.get_geotop_binary()
        term = subprocess.call([binary, directory.dirpath])
        return term

    def command(self, directory):
        binary = self.cfg.get_geotop_binary()
        successful_run = Path(directory.dirpath, '_SUCCESSFUL_RUN')
        failed_run = Path(directory.dirpath, '_FAILED_RUN')
        reset_success = f"[ ! -e {successful_run} ] || rm {successful_run}"
        reset_failure = f"[ ! -e {failed_run} ] || rm {failed_run}"
        launch_new = f"{binary} {directory.dirpath}"
        cmd = ";".join([reset_success, reset_failure, launch_new])
        return cmd

    def version(self):
        tmpdir = TemporaryDirectory()
        binary = self.cfg.get_geotop_binary()
        subprocess.call([binary, tmpdir.name])
        file = Path(tmpdir.name, "geotop.log")
        pattern = re.compile("\\d+\\.\\d+\\.\\d+")
        result = None
        
        with open(file) as f:
            while result is None:
                result = pattern.search(f.readline())
        
        return result.group()


class GeotopDirectory(ModelDirectory):
    """[summary]

    Parameters
    ----------
    ModelDirectory : [type]
        [description]

    Returns
    -------
    [type]
        [description]

    Requirements:
    - only 1 simulation period should write output
    """
    MODEL = GEOTOP

    def __init__(self, dirpath):
        super().__init__(dirpath)
        self.validate()

    @staticmethod
    def copy_template_directory(cfg, sim) -> str:
        directory = ModelDirectory.copy_template_directory(cfg, sim)
        
        # Clear success / failure flags
        for flag in ["_SUCCESSFUL_RUN", "_FAILED_RUN",
                     "_SUCCESSFUL_RUN.old", "_FAILED_RUN.old"]:
            flagfile = Path(directory, flag)
            if flagfile.is_file():
                flagfile.unlink()

        return directory

    @classmethod
    def from_simulation_abstraction(cls, cfg, sim):
        "Create new geotop directory from a SimulationAbstraction object"
        cfg = GeotopConfig.from_config(cfg)
        dirpath = GeotopDirectory.copy_template_directory(cfg, sim)

        modeldir = cls(dirpath)
        modeldir.write_forcing(cfg, sim)
        modeldir.apply_parameters(sim, cfg)

        return modeldir

    def write_forcing(self, cfg, sim):
        """ write forcing data to simulation directory """
        
        forcing_file = sim.get_forcing()
        site_name = sim.get_site_name()
        cfg = GeotopConfig.from_config(cfg)
        
        logger.info(f"Copying forcing data for {site_name} from {forcing_file}")

        dst = Path(Path(self.dirpath).parent, 'temp', 'globsim',
                   'meteo', Path(forcing_file).name)
        
        with nc.Dataset(forcing_file, 'r') as ncf:
            i = [j for j, n in enumerate(nc.chartostring(ncf['station_name'][:])) if n == site_name][0]

        new_forcing = Path(dst, f"{i}-{site_name}_Forcing_0001.txt")
        
        if new_forcing.is_file():
            logger.info(f"Skipping globsim conversion for {new_forcing} (already exists)")
            converted = new_forcing 
            
        else: 
            if not Path(dst).parent.is_dir():
                makedirs(dst)
                logger.info(f"created missing directory {dst}")

            if not Path(dst).is_dir():
                makedirs(dst)
                logger.info(f"created missing directory {dst}")
                
            converted = globsim_to_geotop(forcing_file, dst, site_name)[0]

        # rename
        inpts = DirectoryReader(self.dirpath)
        inpts.inpts_read()
        metfile_name = inpts.inpts['MeteoFile'][1:-1]

        if not cfg.get('jobdata', 'skipchecks', fallback=False):
            self.ensure_forcing_date_coverage(inpts, converted)
        else:
            logger.warning("skipping checks")
        
        try:
            target = Path(self.dirpath, metfile_name + "0001.txt")
            if Path(target).is_file():
                remove(target)
            symlink(converted, target)
        except (OSError, PermissionError) as e:
            logger.warning("Could not create a symbolic link to meteo file. Copying file instead.")
            shutil.move(converted, Path(self.dirpath, metfile_name + "0001.txt"))

    @staticmethod
    def ensure_forcing_date_coverage(directory_reader, meteofile):
        model_start = min(directory_reader.inpts['InitDateDDMMYYYYhhmm'])
        model_end = max(directory_reader.inpts['EndDateDDMMYYYYhhmm'])
        meteo = pd.read_csv(meteofile)
        date_header = directory_reader.inpts['HeaderDateDDMMYYYYhhmmMeteo'][1:-1]
        meteo[date_header] = pd.to_datetime(meteo[date_header], format="%d/%m/%Y %H:%M")
        forcing_start = min(meteo[date_header])
        forcing_end = max(meteo[date_header])
        if model_start <= forcing_start:
            logger.error(f"Model start date {model_start} exceeds input meteo data {forcing_start}")
        if model_end >= forcing_end:
            logger.error(f"Model end date {model_end} exceeds input meteo data {forcing_end}")

    def validate(self):
        valid = self.is_valid_directory(self.dirpath)

        if not valid:
            logger.warning(f"A geotop directory was created at {self.dirpath} that did not \
                           meet all tests for a valid directory.")

        return valid

    def write_start_date(self, start_date):
        """Writes start date

        Parameters
        ----------
        end_date : 1d datetime array
            May be length 1
        """
        self.write_single_input('InitDateDDMMYYYYhhmm', start_date)

    def write_end_date(self, end_date):
        """Writes end date

        Parameters
        ----------
        end_date : 1d datetime array
            May be length 1
        """
        self.write_single_input('EndDateDDMMYYYYhhmm', end_date)

    def read_inpts(self):
        reader = DirectoryReader(self.dirpath)
        reader.inpts_read('geotop.inpts')
        return reader.inpts

    def write_single_input(self, key, value):
        reader = DirectoryReader(self.dirpath)
        reader.inpts_read()
        reader.inpts[key] = value
        reader.inpts_write()

    @staticmethod
    def is_valid_directory(dirpath):
        return (GeotopDirectory.__has_inpts(dirpath)
                and GeotopDirectory.__has_forcing(dirpath))

    @staticmethod
    def __has_inpts(dirpath):
        return Path(dirpath, "geotop.inpts").is_file()

    @staticmethod
    def __has_forcing(dirpath):
        reader = DirectoryReader(dirpath)
        reader.inpts_read('geotop.inpts')
        pattern = re.compile(reader.inpts['MeteoFile'][1:-1])
        exists = any(filter(pattern.match, listdir(dirpath)))

        return exists

        
class GeotopConfig(PemConfig):

    """
    Adds geotop-specific calls to PemConfig
    """
    MODEL = GEOTOP

    def __init__(self, tomlfile):
        super().__init__(tomlfile)

    @classmethod
    def from_config(cls, pemconfig):
        geotopconfig = cls(pemconfig._file)
        geotopconfig._config = pemconfig._config
        return geotopconfig

    def get_geotop_binary(self):
        binary = self.get('models', 'geotop', 'binary')
        return binary


class BasicGeotopParameter(ParameterSet):
    """
    Simply writes the contents of inpts files into other inpts files
    Parameters
    ----------
    filepath : str
        Path to a geotop inpts file

    """
    NAME = "basicgeotop"
    SUPPORTS = [GEOTOP]

    def __init__(self, filepath):
        self._filepath = filepath

    @property
    def filepath(self):
        return self._filepath

    def apply(self, model_directory, cfg=None):
        """[summary]

        Parameters
        ----------
        model_directory : ModelDirectory
            GTPEM ModelDirectory object where parameters will be written
        """
        logger.info(f"Applying parameters from {self.filepath} to model directory at {model_directory.dirpath}")
        reader = DirectoryReader(model_directory.dirpath)
        reader.inpts_read()  # Read original parameters
        reader.inpts_read(self.filepath)  # read the target parameter file
        reader.inpts_write()


class BasicGeotopFactory(ParameterFactory):
    """_summary_

    Parameters
    ----------
    csv : str
        csv with columns in the following structure
        |site|P1|P2|...
        |----|--|--|---
        |S1  |M1|M1|...
        |S2  |M1|  |

        where P* is a valid path to a geotop.inpts file
        S* is the name of a site
        M* is the name of a compatible Geotop ParameterSet

    """
    NAME = "selectivegeotop"
    SUPPORTS = [GEOTOP]

    def __init__(self, csv):
        self.table = pd.read_csv(csv)
        self.table['site'] = self.table['site'].astype('str')
        self.check_table(self.table)
        self.csv = Path(csv)

    @staticmethod
    def check_table(df):
        if "site" not in df.columns:
            raise KeyError("'site' must be name of first column")
        for c in df.columns[1:].values:
            if not Path(c).is_file():
                raise FileNotFoundError(f"file {c} not found")
        return True

    def generate(self, site: str) -> "list[ParameterSet]":
        """ Get all parameters for a particular site"""
        sets = []
        if site not in self.table['site'].values:
            logger.debug(f"site {site} not in site list {self.csv.name}")
            return sets
        
        for (p, v) in zip(self.table.columns[1:], self.table.loc[self.table["site"] == site, ].values[0, 1:]):
            # check if its another kind of parameter
            if not pd.isnull(v):
                sets.append(BasicGeotopParameter(p))
        
        return sets


class GeotopSoilTemperatureAggregator(GroupedNetCDFAggregator):
    """[summary]

    Parameters
    ----------
    SoilTemperatureAggregator : [type]
        [description]

    Returns
    -------
    [type]
        [description]
    """
    NAME = 'soil_temperature'
    SUPPORTS = [GEOTOP]
    DATAFILE = 'SoilAveragedTempProfileFileWriteend'
    DEFAULTS = {"SoilPlotDepths": [], "NumSimulationTimes": 1, "DateSoil": 1}
    VARINFO = {'units': 'degree_Celcius',
               'varname': 'Tg',
               'standard_name': 'soil_temperature',
               'long_name': 'Simulated soil temperature'}

    def __init__(self, modeldirectory):
        super().__init__(modeldirectory)
        
        self.dirname = Path(modeldirectory.dirpath).name
        self.inpts_file = Path(self.dirname, "geotop.inpts")
        self.inpts = modeldirectory.read_inpts()
        self.validate_inpts(self.inpts)
        self.read_output_data()

    def validate_inpts(self, inpts):
        if not self.output_exists():
            raise FileNotFoundError(f"Could not aggregate {self.VARINFO['varname']} for"
                                    f"geotop. File {self.get_data_file()} was not found.")

        max_runs = max(np.array(inpts['NumSimulationTimes']) * (np.array(inpts['DtPlotPoint']) > 0))
        n_periods = sum(np.array(inpts['DtPlotPoint']) > 0)
        if (max_runs > 1) or (n_periods != 1):
            raise ValueError("Too many simulations plotted. \
                             DtPlotPoint must be > 0 for only a single simulation")
    
    def output_exists(self):
        """
        checks whether the output data file exists or not
        """
        return path.isfile(self.get_data_file())

    def set_default_inpts(self, inpts):
        
        for key in self.DEFAULTS:
            if key not in inpts.keys():
                inpts[key] = self.DEFAULTS[key]

    def aggregate(self, target):
        if not Path(target).is_file():
            self.new(target)

        with nc.Dataset(target, 'a') as main:
            rootgrp = main[GEOTOP] if GEOTOP in main.groups else self.new_geotop_group(main)
            
            logger.info(f"Writing data from GEOtop simulation {Path(self.directory.dirpath).name}")
            
            for p in self.datahandler.points:
                data = self.datahandler.get_point(p).data_only()

                i = self.write_simulation_name(rootgrp, self.dirname)
                
                # write point-data
                rootgrp['pointid'][i] = p

                if self.info is not None:
                    self.write_info(rootgrp, i, self.info)
                else:
                    logger.warning(f"missing folder manifest information for {self.dirname}. Additional simulation metadata will not be added")

                self.add_output_variables(rootgrp, sim=i, data=data)

    def write_info(self, group, i, info):
        if "sitename" not in group.variables:
            site = group.createVariable("sitename", "S1", ("simulation", "nchars"))
            site._Encoding = 'ascii'
        else:
            site = group["sitename"]

        site[i, :] = info["site"].values[0]

        if "model" not in group.variables:
            model = group.createVariable("model", "S1", ("simulation", "nchars"))
            model._Encoding = 'ascii'
        else:
            model = group["model"]
            
        model[i, :] = info["model"].values[0]

    def new_geotop_group(self, main):
        rootgrp = main.createGroup(GEOTOP)

        self.times = self.datahandler.get_unique_dates()
        self.initialize_group(rootgrp, self.inpts)
        self.create_depth_var(rootgrp, self.inpts)
        self.create_time_var(rootgrp, self.inpts)
        self.write_time_var(rootgrp, self.times)
        self.create_sim_var(rootgrp)
        self.create_point_var(rootgrp)
        
        return rootgrp

    def new(self, target):
        with nc.Dataset(target, 'w') as main:
            _ = self.new_geotop_group(main)

    def get_data_file(self):
        basename = self.inpts[self.DATAFILE][1:-1]
        dfile = Path(self.directory.dirpath, basename + ".txt")
        return dfile
    
    def read_output_data(self):
        """
        Read output data files from current geotop simulation directory.
        """
        dfile = self.get_data_file()
        data = pd.read_csv(dfile) # .iloc[1:, :] 
        self.datahandler = GeotopSoilOutputHandler(data)

  
    def initialize_group(self, group, inpts):
        """
        Add required dimensions to a group

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            an open netcdf group or dataset
        inpts : ParameterIO
            A parameterIO object or similar with geotop.inpts keys as attributes

        """
        n_times = len(self.times)

        _ = group.createDimension("nchars", 255)
        _ = group.createDimension("simulation", None)
        _ = group.createDimension("time", n_times)
        _ = group.createDimension("soil_depth", len(inpts['SoilPlotDepths']))

    @staticmethod
    def create_depth_var(group, inpts):
        """ All depths the same for the group """
        depth = group.createVariable("soil_depth", "f4", ("soil_depth",))
        depth[:] = np.array(inpts['SoilPlotDepths']) / 1000
        depth.units = 'm'
        depth.axis = "Z"
        depth.standard_name = 'depth'
        depth.long_name = 'depth below the ground surface'
        depth.positive = 'down'

    @staticmethod
    def create_time_var(group, inpts):
        """ create time variable """
        timevar = group.createVariable("Date", "f8", ("time",))
        timevar.units = "days since 0001-1-1 0:0:0"
        timevar.calendar = "standard"
        timevar.standard_name = 'time'
        timevar.axis = "T"

    @staticmethod
    def create_sim_var(group):
        """
        create simulation name variable
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset
        """
        simvar = group.createVariable("simulation", "S1", ("simulation", "nchars"))
        simvar._Encoding = 'ascii'

    @staticmethod
    def create_point_var(group):
        """
        create point ID variable
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset
        """
        pidvar = group.createVariable("pointid", "i4", ("simulation"))


    @staticmethod
    def write_time_var(group, times):
        """
        Write values to a group that already contains a time variable called "Date"
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset with a time variable called "Date"
        times : array
            array of datetime objects that can be passed to date2num
        """
        nctimes = nc.date2num(times, units="days since 1-1-1 0:0:0", calendar='standard')
        var = group["Date"]
        var[0:len(np.atleast_1d(times))] = nctimes

    def write_simulation_name(self, group, name):
        """
        Write the geotop simulation directory name to the next available simulation variable
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            An open netcdf group or dataset
        name : str
            Name of simulation (obtained from directory name)

        Returns
        -------
            int : index of simulation
        """
        i = self.next_simulation_index(group)
        # name_arr = nc.stringtochar(np.array(name, dtype='S3'))
        group['simulation'][i, :] = name
        return i

    @staticmethod
    def next_simulation_index(group):
        """
        Get the next simulation index . This is used to determine where the next array slice
        should be written.

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset

        Returns
        -------
            int : The index of the next empty array slice along the simulation dimension
        """
        return len(group['simulation'])

    def add_output_variables(self, group, sim, data):
        """
        Add current geotop output to a netcdf4 group or dataset "

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            an open netcdf group or dataset
        sim : int
            index along the simulation dimension where array should be written
        """
        varname = self.VARINFO['varname']
        if varname not in group.variables:
            var = group.createVariable(varname, "f4", ("simulation", "time", "soil_depth",))
            var.units = self.VARINFO['units']
            var.long_name = self.VARINFO['long_name']
            var.standard_name = self.VARINFO['standard_name']
        else:
            var = group[varname]
        var[sim, :, :] = data

class GeotopDepthOfThawAggregator(GroupedNetCDFAggregator):
    """[summary]

    Parameters
    ----------
    DepthOfThawAggregator : [type]
        [description]

    Returns
    -------
    [type]
        [description]
    """
    NAME = 'thaw_depth'
    SUPPORTS = [GEOTOP]
    DATAFILE = 'PointOutputFileWriteEnd'
    DEFAULTS = {"NumSimulationTimes": 1, "DatePoint": 1}
    COLUMN = 'lowest_thawed_soil_depth[mm]'
    VARINFO = {'units': 'meter',
               'varname': 'AL', 
               'standard_name': 'thaw_depth',
               'long_name': 'Simulated depth of thaw'}
    SCALEFACTOR = 0.001

    def __init__(self, modeldirectory):
        super().__init__(modeldirectory)
        
        self.dirname = Path(modeldirectory.dirpath).name
        self.inpts_file = Path(self.dirname, "geotop.inpts")
        self.inpts = modeldirectory.read_inpts()
        self.validate_inpts(self.inpts)
        self.read_output_data()

    def validate_inpts(self, inpts):
        if not self.output_exists():
            raise FileNotFoundError(f"Could not aggregate {self.VARINFO['varname']} for"
                                    f"geotop. File {self.get_data_file()} was not found.")

        max_runs = max(np.array(inpts['NumSimulationTimes']) * (np.array(inpts['DtPlotPoint']) > 0))
        n_periods = sum(np.array(inpts['DtPlotPoint']) > 0)
        if (max_runs > 1) or (n_periods != 1):
            raise ValueError("Too many simulations plotted. \
                             DtPlotPoint must be > 0 for only a single simulation")
    
    def output_exists(self):
        """
        checks whether the output data file exists or not
        """
        return path.isfile(self.get_data_file())

    def set_default_inpts(self, inpts):
        
        for key in self.DEFAULTS:
            if key not in inpts.keys():
                inpts[key] = self.DEFAULTS[key]

    def aggregate(self, target):
        if not Path(target).is_file():
            self.new(target)

        with nc.Dataset(target, 'a') as main:
            rootgrp = main[GEOTOP] if GEOTOP in main.groups else self.new_geotop_group(main)
            
            logger.info(f"Writing data from GEOtop simulation {Path(self.directory.dirpath).name}")
            
            for p in self.datahandler.points:
                data = self.datahandler.get_point(p).data_only(self.COLUMN) * self.SCALEFACTOR

                i = self.write_simulation_name(rootgrp, self.dirname)
                
                # write point-data
                rootgrp['pointid'][i] = p

                if self.info is not None:
                    self.write_info(rootgrp, i, self.info)
                else:
                    logger.warning("missing folder manifest information. Additional simulation metadata will not be added")

                self.add_output_variables(rootgrp, sim=i, data=data)

    def write_info(self, group, i, info):
        if "sitename" not in group.variables:
            site = group.createVariable("sitename", "S1", ("simulation", "nchars"))
            site._Encoding = 'ascii'
        else:
            site = group["sitename"]

        site[i, :] = info["site"].values[0]

        if "model" not in group.variables:
            model = group.createVariable("model", "S1", ("simulation", "nchars"))
            model._Encoding = 'ascii'
        else:
            model = group["model"]
            
        model[i, :] = info["model"].values[0]

    def new_geotop_group(self, main):
        rootgrp = main.createGroup(GEOTOP)

        self.times = self.datahandler.get_unique_dates()
        self.initialize_group(rootgrp, self.inpts)
        #self.create_depth_var(rootgrp, self.inpts)
        self.create_time_var(rootgrp, self.inpts)
        self.write_time_var(rootgrp, self.times)
        self.create_sim_var(rootgrp)
        self.create_point_var(rootgrp)
        
        return rootgrp

    def new(self, target): 
        with nc.Dataset(target, 'w') as main:
            _ = self.new_geotop_group(main)

    def get_data_file(self):
        basename = self.inpts[self.DATAFILE][1:-1]
        dfile = Path(self.directory.dirpath, basename + ".txt")
        return dfile
    
    def read_output_data(self):
        """
        Read output data files from current geotop simulation directory.
        """
        dfile = self.get_data_file()
        data = pd.read_csv(dfile) # .iloc[1:, :] 
        self.datahandler = GeotopSurfaceOutputHandler(data)
  
    def initialize_group(self, group, inpts):
        """
        Add required dimensions to a group

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            an open netcdf group or dataset
        inpts : ParameterIO
            A parameterIO object or similar with geotop.inpts keys as attributes

        """
        n_times = len(self.times)

        _ = group.createDimension("nchars", 255)
        _ = group.createDimension("simulation", None)
        _ = group.createDimension("time", n_times)


    @staticmethod
    def create_time_var(group, inpts):
        """ create time variable """
        timevar = group.createVariable("Date", "f8", ("time",))
        timevar.units = "days since 1-1-1 0:0:0"
        timevar.calendar = "standard"
        timevar.standard_name = 'time'
        timevar.axis = "T"

    @staticmethod
    def create_sim_var(group):
        """
        create simulation name variable
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset
        """
        simvar = group.createVariable("simulation", "S1", ("simulation", "nchars"))
        simvar._Encoding = 'ascii'

    @staticmethod
    def create_point_var(group):
        """
        create point ID variable
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset
        """
        pidvar = group.createVariable("pointid", "i4", ("simulation"))


    @staticmethod
    def write_time_var(group, times):
        """
        Write values to a group that already contains a time variable called "Date"
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset with a time variable called "Date"
        times : array
            array of datetime objects that can be passed to date2num
        """
        nctimes = nc.date2num(times, units="days since 1-1-1 0:0:0", calendar='standard')
        var = group["Date"]
        var[0:len(np.atleast_1d(times))] = nctimes

    def write_simulation_name(self, group, name):
        """
        Write the geotop simulation directory name to the next available simulation variable
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            An open netcdf group or dataset
        name : str
            Name of simulation (obtained from directory name)

        Returns
        -------
            int : index of simulation
        """
        i = self.next_simulation_index(group)
        # name_arr = nc.stringtochar(np.array(name, dtype='S3'))
        group['simulation'][i, :] = name
        return i

    @staticmethod
    def next_simulation_index(group):
        """
        Get the next simulation index . This is used to determine where the next array slice
        should be written.

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset

        Returns
        -------
            int : The index of the next empty array slice along the simulation dimension
        """
        return len(group['simulation'])

    def add_output_variables(self, group, sim, data):
        """
        Add current geotop output to a netcdf4 group or dataset "

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            an open netcdf group or dataset
        sim : int
            index along the simulation dimension where array should be written
        """
        varname = self.VARINFO['varname']
        
        if varname not in group.variables:
            var = group.createVariable(varname, "f4", ("simulation", "time", ))
            var.units = self.VARINFO['units']
            var.long_name = self.VARINFO['long_name']
            var.standard_name = self.VARINFO['standard_name']
        else:
            var = group[varname]
        
        var[sim, :] = data.values
            

class GeotopAirTempAggregator(GeotopDepthOfThawAggregator):
    NAME = 'air_temperature'
    COLUMN = 'Tair[C]'
    VARINFO = {'units': 'degree_Celcius',
               'varname': 'Ta',
               'standard_name': 'air_temperature',
               'long_name': 'Simulated air temperature'}


class GeotopSnowDepthAggregator(GeotopDepthOfThawAggregator):
    NAME = 'snow_depth'
    COLUMN = 'snow_depth[mm]'
    VARINFO = {'units': 'mm',
               'varname': 'snow_depth_mm',
               'standard_name': 'snow_depth',
               'long_name': 'Simulated snow depth'}
    SCALEFACTOR = 1

class GeotopSnowWaterEquivalentAggregator(GeotopDepthOfThawAggregator):
    NAME = 'swe'
    COLUMN = 'swe'
    VARINFO = {'units': 'kg m-2',
               'varname': 'snow_water_equivalent',
               'standard_name': 'lwe_thickness_of_surface_snow_amount',
               'long_name': 'Simulated snow water equivalent or liquid water content of surface snow in kg m-2'}
    
class GeotopSurfaceOutputHandler:

    def __init__(self, df: "pd.DataFrame"):
        self.metadata = self.analyse_surface_table(df)
        self.data = df.loc[df["Simulation_Period"] == self.metadata['last_sim_period'], :]   # restrict to last

    @staticmethod
    def analyse_surface_table(df:"pd.DataFrame") -> dict:
        ana = dict()
        
        try:
            ana['points'] = df['IDpoint'].unique()
        except NameError:
            logger.warning("Simulation IDpoint column is missing. Cannot distinguish points.")
            ana['points'] = [1]  # default we assume 1 point

        ana['n_points'] = len(ana['points'])
        
        try:
            ana['last_sim_period'] = df['Simulation_Period'].max()
        except NameError:
            logger.warning("Simulation Period column is missing. Cannot determine last simulation period.")
            ana['last_sim_period'] = None  # if Simulation_Period column missing
        
        return ana
    
    @property
    def points(self) -> list:
        return self.metadata['points'] 
    
    def get_point(self, i) -> "GeotopSurfaceOutputHandler":
        if i not in self.points:
            raise ValueError("Point ID out of range")
        return GeotopSurfaceOutputHandler(self.data.loc[self.data['IDpoint'] == i, :])

    def data_only(self, columns:list) -> "pd.DataFrame":
        #pattern = re.compile('\\d+\\.\\d*')
        #column = "lowest_thawed_soil_depth[mm]"
        values = self.data[columns]
        return values

    def get_unique_dates(self, date_index=0):
        """
        Get a list of the unique dates in a geotop output file. Since an output file may have multiple
        points, runs and simulation period, the dates in the original file may be repeated.
        Parameters
        ----------
        raw : Pandas dataframe
            pandas dataframe representing a geotop output file like
        date_index : int
            index for geotop "Date" column. Should be the first one (geotop default)

        Returns
        -------
        array : an array of datetime objects corresponding to the list of times in the output simulation file
        """
        times = self.data.iloc[:, date_index]
        times = [datetime.strptime(i, "%d/%m/%Y %H:%M") for i in times]
        times = np.unique(times)
        return times


class GeotopSoilOutputHandler:

    def __init__(self, df: "pd.DataFrame"):
        self.metadata = self.analyse_soil_table(df)
        self.data = df.loc[df["Simulation_Period"] == self.metadata['last_sim_period'], :]   # restrict to last

    @staticmethod
    def analyse_soil_table(df:"pd.DataFrame") -> dict:
        ana = dict()
        
        try:
            ana['points'] = df['IDpoint'].unique()
        except NameError:
            logger.warning("Simulation IDpoint column is missing. Cannot distinguish points.")
            ana['points'] = [1]  # default we assume 1 point

        ana['n_points'] = len(ana['points'])
        
        try:
            ana['last_sim_period'] = df['Simulation_Period'].max()
        except NameError:
            logger.warning("Simulation Period column is missing. Cannot determine last simulation period.")
            ana['last_sim_period'] = None  # if Simulation_Period column missing
        
        return ana
    
    @property
    def points(self) -> list:
        return self.metadata['points'] 
    
    def get_point(self, i) -> "GeotopSoilOutputHandler":
        if i not in self.points:
            raise ValueError("Point ID out of range")
        return GeotopSoilOutputHandler(self.data.loc[self.data['IDpoint'] == i, :])

    def data_only(self) -> "pd.DataFrame":
        pattern = re.compile('\\d+\\.\\d*')
        values = self.data[[col for col in filter(pattern.match, self.data.columns)]]
        return values

    def get_unique_dates(self, date_index=0):
        """
        Get a list of the unique dates in a geotop output file. Since an output file may have multiple
        points, runs and simulation period, the dates in the original file may be repeated.
        Parameters
        ----------
        raw : Pandas dataframe
            pandas dataframe representing a geotop output file like
        date_index : int
            index for geotop "Date" column. Should be the first one (geotop default)

        Returns
        -------
        array : an array of datetime objects corresponding to the list of times in the output simulation file
        """
        times = self.data.iloc[:, date_index]
        times = [datetime.strptime(i, "%d/%m/%Y %H:%M") for i in times]
        times = np.unique(times)
        return times


class GeotopGroupedNCSnowAggregator(GeotopSoilTemperatureAggregator):
    NAME = 'snow'
    DATAFILE = 'SnowProfileFileWriteEnd'
    VARINFO = {'units': 'm',
               'varname': 'snow_profile',
               'standard_name': 'surface_snow_thickness',
               'long_name': 'Snow profile'}


class GeotopGroupedNCLiquidWaterAggregator(GeotopSoilTemperatureAggregator):
    NAME = 'liquid_water'
    DATAFILE = 'SoilAveragedLiqContentProfileFileWriteEnd'
    VARINFO = {'units': '1',
               'varname': 'liq_water',
               'standard_name': 'volume_fraction_of_water_in_soil',
               'long_name': 'Simulated liquid water content in soil or rock'}


class GeotopGroupedNCWaterPressureAggregator(GeotopSoilTemperatureAggregator):
    NAME = 'water_pressure'
    DATAFILE = 'SoilTotWaterPressProfileFileWriteEnd'
    VARINFO = {'units': '1',
               'varname': 'psi',
               'standard_name': '',
               'long_name': 'Total water pressure'}


class GeotopGroupedNCIceContentAggregator(GeotopSoilTemperatureAggregator):
    NAME = 'ice_content'
    DATAFILE = 'SoilAveragedIceContentProfileFileWriteEnd'
    VARINFO = {'units': '1',
               'varname': 'ice_content',
               'standard_name': '',
               'long_name': 'Averaged ice content'}




