from datetime import datetime
from os import path
from pathlib import Path
import pandas as pd

class DirectoryReader(object):
    """
    Copied from geotop repo for now...
    """

    def __init__(self, directory):
        """Instantiate a new object"""
        self.fmt_date = "%d/%m/%Y %H:%M"
        self.dir = directory
        self.inpts_file = path.join(self.dir, "geotop" + ".inpts")
        self.inpts = dict()

    def inpts_read(self, inpts_file=""):
        """
        Read *.inpts into a list of strings (each line is one entry) and
        parse content into a dictionary.
        """
        if inpts_file == "":
            inpts_file = self.inpts_file
        
        elif Path(inpts_file).is_file():
            pass
        
        else:
            inpts_file = path.join(self.dir, inpts_file)

            # read file
        with open(inpts_file, "r") as myfile:
            inpts_str = myfile.readlines()

        # parse content
        for line in inpts_str:
            d = self.line2dict(line)
            if d is not None:
                self.inpts[list(d.keys())[0]] = list(d.values())[0]

    def get_inpts(self):
        """
        Return inpts dictionary.
        """
        return self.inpts

    @staticmethod
    def __is_only_comment(lin):
        # checks whether line contains nothing but comment
        for c in lin:
            if c != " ":
                if c == "!":
                    return True
                else:
                    return False

    def __string2datetime(self, valu):
        # checks if value is a date string. If true, a datetime object is
        # returned. If false, value is returned unchanged. 01/11/2013 12:00
        if not isinstance(valu, str):
            return valu

        # see if time conversion is possible
        try:
            datelist = [x.strip() for x in valu.split(',')]
            valu = [datetime.strptime(v, self.fmt_date) for v in datelist]
        except ValueError:
            pass
        return valu

    def __string2datetime_list(self, dates):
        # convert list of date strings to datetime
        return [self.__string2datetime(date) for date in dates]

    def line2dict(self, lin):
        """
        Converts one line of a *.inpts file into a dictionary. Comments
        are recognised and ignored, float vectors are preserved, datetime
        converted from string.
        """
        # Check if this is valid
        if self.__is_only_comment(lin):
            return None

        # Remove possible trailing comment form line
        lin = lin.split("!")[0]

        # Discard lines without value assignment
        if len(lin.split("=")) != 2:
            return None

        # Extract name and value, strip of leading/trailling blanks
        name = lin.split("=")[0].strip()
        valu = lin.split("=")[1].strip()

        # Convert to float or list of float if numeric
        try:
            valu = list(map(float, valu.split(",")))
        except ValueError:
            pass

        # Convert to datetime if it is datetime
        valu = self.__string2datetime(valu)

        # Make dictionary and return
        return {name: valu}

    def format_values(self, valu):
        """
        Ensure proper formatting for lines in *.inpts.
        """
        if isinstance(valu, str):
            return valu

        if isinstance(valu, datetime):
            return valu.strftime(self.fmt_date)

        if isinstance(valu, list):
            if isinstance(valu[0], datetime):
                return ', '.join([x.strftime(self.fmt_date) for x in valu])
            else:
                return str(valu)[1:-1]

    def inpts_round(self):
        """
        Round list values in self.inpts to useful lenths.
        """
        for l in self.inpts:
            if type(self.inpts[l]) == list:
                if not isinstance(self.inpts[l][0], datetime):
                    self.inpts[l] = [finite_round(elem, 5) for elem in self.inpts[l]]

    def inpts_write(self):
        """
        Write *.inpts file from self.inpts dictionary.
        """
        self.inpts_round()
        f = open(self.inpts_file, 'w')
        for n in range(len(self.inpts)):
            line = list(self.inpts.keys())[n] + " = " + \
                   self.format_values(list(self.inpts.values())[n]) + "\n"
            f.write(line)
        f.close()

    def ascii_read(self, file_ascii, convert_geotop2=True):
        """
        Read GEOtop time series ASCII file and return them as a pandas data
        frame. The first column is always returned as with data type datetime64.
        All other columns will be in a useful numeric data type.

        The ASCII csv file as formatted by GEOtop needs to have the first column
        name "Date". This is enforced by inserting the line
        HeaderDateSoil = "Date"
        into *.inpts.
        """
        # read file and convert date column
        raw = pd.read_csv(file_ascii)
        raw['Date'] = pd.to_datetime(raw['Date'], format=self.fmt_date)

        # format as geotop 2.0.0 if using geotop 2.0.1
        if convert_geotop2 and any([bool(x.find("DEPTH")) for x in raw.columns]):
            raw = self.__clean_geotop201(raw)

        return raw

    def ascii_write(self, file_ascii, data):
        """
        Write GEOtop time series ASCII files from pandas data frame. The first
        column has to always have data type datetime64. All other columns can
        be in a useful numeric data type.
        """
        data.to_csv(file_ascii, date_format=self.fmt_date, index=False)


def finite_round(x, precision=5):
    """ Round to a useful length, but don't round a finite number to zero"""
    rounded = round(x, precision)
    
    if (rounded == 0) & (x != 0):
        return x
    return rounded
