import xml.etree.ElementTree as ET
import f90nml


class ClassicOutputVariables:

    FORM_SUFFIX = {"pft": "_perpft", "grid": "", "tile": "", "layer": ""}

    def __init__(self, xml_file):
        """Representation of CLASSIC model output XML file

        Parameters
        ----------
        xml_file : string
            path to output variable definition
        """
        tree = ET.parse(xml_file)
        self.variableSet = tree.getroot()

    def form_suffix(self, form):
        return self.FORM_SUFFIX[form]

    def get_active_output_files(self):
        files = []

        for variable in self.variableSet.findall(".//*[@dormant='false']"):
            for variant in variable.findall("./variant"):
                shortname = variable.find("./shortName").text
                form = self.form_suffix(variant.find(r"./outputForm").text) 
                freq = variant.find("./timeFrequency").text
                files.append(f"{shortname}_{freq}{form}.nc")                 
        
        return files

    def get_expected_output_files(self, job_options_file):
        """Return a list of expected files for a successful run

        Parameters
        ----------
        job_options_file : str
            path to job_options_file.txt for a CLASSIC simulation

        Returns
        -------
        list
            A list of files that are expected as output
        """
        expected = self.get_expected_output_dict(job_options_file)

        files = list()

        for group in self.variableSet.findall("./group"):
            if not expected['groups'][group.get('type')]:
                continue
            for variable in group.findall(".//*[@dormant='false']"):
                for variant in variable.findall("./variant"):
                    shortname = variable.find("./shortName").text
                    form = variant.find(r"./outputForm").text
                    form_suffix = self.form_suffix(form)
                    freq = variant.find("./timeFrequency").text
                   
                    if (not expected['frequencies'][freq]) or (not expected['forms'][form]):
                        continue
                    
                    files.append(f"{shortname}_{freq}{form_suffix}.nc")
        
        return files

    def get_expected_output_dict(self, job_options_file):
        opts = f90nml.read(job_options_file)['joboptions']
        
        expected = {'groups': {'class': True,
                               'ctem': opts['ctem_on'],
                               'land_fire': opts['lnduseon'] and opts['dofire'],
                               'land': opts['lnduseon'],
                               'fire': opts['dofire'],
                               'PFTCompetition': opts['PFTCompetition'],
                               'tracer': bool(opts['useTracer']),
                               'methane': opts['doMethane']
                               },
                   
                    'frequencies': {'halfhourly': opts['dohhoutput'],
                                    'daily': opts['dodayoutput'],
                                    'monthly': opts['domonthoutput'],
                                    'annually': opts['doAnnualOutput']
                                    },
                   
                    'forms': {'pft': opts['doperpftoutput'],
                              'tile': opts['dopertileoutput'],
                              'grid': True,
                              'layer': True}
                    }

        return expected

