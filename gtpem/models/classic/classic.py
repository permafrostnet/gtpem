from gtpem.core import PemConfig
import f90nml
import logging
import re
import pandas as pd
import numpy as np
import tomlkit
import shutil 

from os import path, makedirs, replace
from pathlib import Path
import netCDF4 as nc
from gtpem.models.template import ModelDirectory, Simulation, ParameterSet
from .globsim_to_classic import globsim_to_classic_nc, get_timestep, has_leap
from .classic_output_xml import ClassicOutputVariables
from gtpem.models.generic import GroupedNetCDFAggregator
from datetime import datetime
from typing import Optional, Union


logger = logging.getLogger(__name__)

CTEM_FILES = {'CO2File': ['ctem_on'],
              'CH4File': ['doMethane'],
              'OBSWETFFile': ['doMethane'],
              'POPDFile': ['transientPOPD', 'dofire'],
              'LUCFile': []
              }


class ClassicSimulation(Simulation):
    
    """ Simulation is a composition of a directory and model information """
    
    MODEL = "classic"

    def __init__(self, cfg, dirpath):
        self.cfg = cfg
        self.dirpath = dirpath
        self.directory = ClassicDirectory(dirpath)

    @classmethod
    def from_directory(cls):
        pass
    
    def get_bindings(self):
        """Get the singularity bindings that are required for a simulation

        Returns
        -------
        dict
            a dictionary of bindings for running CLASSIC in a singularity container
             the format originalpath:targetpath

        Bind mounts are used so that singularity can access folders from within the container. All files on the 
        host file system must be accessible through the mount points for them to be usable from within the container.
        Overlapping bindings (e.g. a binding for a folder and another for a subdirectory) may be returned and must be filtered
        out at a later step.
        """
        bindings = dict()

        for f in ClassicDirectory.FILES:
            value = self.directory.get_job_option(f)
            original = path.realpath(path.dirname(value))
            if path.isdir(original):
                bindings[original] = f"/gtpem/{f}"  # overwrite duplicates if multiple files in same directory
            else:
                print(f"warning {value} does not exist")
        bindings[path.realpath(self.dirpath)] = "/gtpem/main"
        bindings[path.realpath(path.dirname(self.cfg.get('models', 'classic', 'binary')))] = "/gtpem/binary"

        return bindings
    
    def trim_bindings(self, bindings):
        """remove bindings that are subdirectories of other bindings.

        Parameters
        ----------
        bindings : dict
            dictionary of file paths of the form {src:target} where src is the path to a host directory
            and target is the path to a directory within the singularity container to which to bind src

        Returns
        -------
        dict
            dictionary of file paths of the form {src:target} with overlapping src directories removed (
                the uppermost directory is retained)

        'Overlapping' bind points don't work (i.e. if you create separate bindings for /home/me and /home/me/myfiles).
        This function reduces a set of bindings to a non-overlapping subset.
        """
        trimmed_bindings = dict()
        for key1 in bindings.keys():
            for key2 in bindings.keys():
                if (key2 in key1) and (key1 != key2):
                    break
            trimmed_bindings[key1] = bindings[key1]

        return trimmed_bindings

    def write_bindings(self, bindings):
        """Create new job_options file with container-appropriate paths

        Parameters
        ----------
        bindings : dict
            [description]
        """
        for f in ClassicDirectory.FILES:
            for directory in bindings.keys():
                if re.search(directory, path.realpath(path.dirname(self.directory.get_job_option(f)))):
                    new_value = re.sub(directory, bindings[directory], path.realpath(self.directory.get_job_option(f)))
                    self.directory.set_job_option(f, new_value, write=False)
        new_job_options_file = path.join(self.dirpath, "gtpem_job_options.txt")
        f90nml.patch(self.directory.job_options_file, self.directory.job_options, new_job_options_file)
        self.directory.read_job_options()  # reset

    def str_bindings(self, bindings):
        """Convert dictionary of bindings to singularity flags

        Parameters
        ----------
        bindings : dict
            dictionary of file paths of the form {src:target} where src is the path to a host directory
            and target is the path to a directory within the singularity container to which to bind src

        Returns
        -------
        str
            A string of singularity command-line parameters to bind paths to the container
            (e.g. "-B src1:dst1 -B src2:dst2")
        """
        template = "-B {}:{}"
        text_bindings = [template.format(key, bindings[key]) for key in bindings]
        return " ".join(text_bindings)

    def command(self):
        """Create command to run the simulation

        Returns
        -------
        str
            a terminal command that launches the simulation
        """
        image = self.cfg.get('models', 'classic', 'container')
        binary = path.join("/gtpem/binary", path.basename(self.cfg.get('models', 'classic', 'binary')))
        new_job_options_file = path.join('/gtpem/main', "gtpem_job_options.txt")
        bindings = self.trim_bindings(self.get_bindings())
        str_bindings = self.str_bindings(bindings)
        
        self.write_bindings(bindings)
        
        rm_success = f"[ ! -e {self.success_flag} ] || rm {self.success_flag}"
        rm_fail = f"[ ! -e {self.fail_flag} ] || rm {self.fail_flag}"
        cd = f"cd {path.dirname(image)}"
        singularity = f"singularity exec {str_bindings} {image} {binary} {new_job_options_file} 0/0"
        
        cmd = ";".join([cd, rm_success, rm_fail,
                        "if " + singularity,
                        f'then echo "$(date)" > {self.success_flag}',
                        f'else echo "$(date)" > {self.fail_flag}',
                        'fi'])

        return cmd

    @property
    def success_flag(self):
        return path.join(self.dirpath, "_SUCCESSFUL_RUN")

    @property
    def fail_flag(self):
        return path.join(self.dirpath, "_FAILED_RUN")  

    def successful_run(self):
        """Whether a simulation completed successfully or not

        Returns
        -------
        bool
            Whether a simulation completed successfully
        """
        output_directory = self.directory.get_job_option('output_directory')
        output_xml = self.directory.get_job_option('xmlFile')
        xml_reader = ClassicOutputVariables(output_xml)
        expected_files = xml_reader.get_expected_output_files(self.directory.job_options_file)
        file_found = [Path(output_directory, f).is_file() for f in expected_files]
        success = all(file_found)
        logger.debug(f"Directory {output_directory} has {sum(file_found)} out of {len(file_found)} output files.")
        
        return success

    def failed_run(self):
        return Path(self.fail_flag).is_file()

    def version(self):
        return "Unknown CLASSIC version"


class ClassicDirectory(ModelDirectory):
    """Directory object for the CLASSIC model
    https://cccma.gitlab.io/classic_pages/model/

    Parameters
    ----------
    dirpath : str
        path to directory
   
    CLASSIC directory must meet the following requirements:
        1. contain valid job_options_file.txt
    """
    MODEL = "classic"

    METFILES = {"metFileFss": 'metVar_sw.nc', "metFileFdl": 'metVar_lw.nc',
                "metFilePre": 'metVar_pr.nc', "metFileTa": 'metVar_ta.nc',
                "metFileQa": 'metVar_qa.nc', "metFileUv": 'metVar_wi.nc',
                "metFilePres": 'metVar_ap.nc'}

    FILES = ['CO2File', 'CH4File', 'OBSWETFFile', 'POPDFile', 'LGHTFile', 'LUCFile',
             'output_directory', 'xmlFile', 'init_file', 'rs_file_to_overwrite', 'runparams_file',
             *METFILES]

    def __init__(self, dirpath):
        super().__init__(dirpath)
        self.validate()
        self.read_job_options()
        self.runparams = f90nml.read(self.get_job_option("runparams_file"))

    @classmethod
    def from_simulation_abstraction(cls, cfg, sim):
        "Create new classic directory from a SimulationAbstraction object"
        if not ClassicDirectory.check_forcing(sim.get_forcing()):
            logger.error("Meteo data missing required variable for CLASSIC (relative humidity)."
                         "You may be using an older GlobSim file."
                         f"\n {path.basename(sim.get_forcing())}\n")
            return

        dirpath = ModelDirectory.copy_template_directory(cfg, sim)
        
        modeldir = cls(dirpath)
        modeldir.write_forcing(cfg, sim)
        modeldir.copy_init()
        modeldir.apply_parameters(sim, cfg)

        output_directory = Path(path.realpath(modeldir.dirpath), "output")
        
        if not Path(output_directory).is_dir():
            makedirs(output_directory)

        modeldir.set_output_directory(str(output_directory))

        return modeldir

    def write_forcing(self, cfg, sim):
        """ Convert globsim data into CLASSIC netcdf files, and set classic parameters appropriately """
        forcing_file = sim.get_forcing()
        site_name = sim.get_site_name()

        logger.info(f"Copying forcing data for {site_name} from {forcing_file}")

        dst = path.join(path.dirname(self.dirpath), 'temp', 'classic',
                        'meteo', site_name, Path(forcing_file).stem)
        if not path.isdir(dst):
            makedirs(dst)
        metloader = cfg.get('models', 'classic', 'metloader')
        container = cfg.get('models', 'classic', 'container')
        metfile, converted = globsim_to_classic_nc(forcing_file, dst, site_name, container, metloader)  # TODO: get the names of these files and set them in the job options

        # Template directory might have different leap year configuration
        leaps = has_leap(metfile)
        if  leaps != self.get_job_option('leap'):  
            self.set_job_option('leap', leaps)
        DELT = self.runparams['classicparams']['DELT']
        forcing_timestep = get_timestep(metfile)
        
        if forcing_timestep != DELT:
            # TODO: investigate whether this patching strategy is more appropriate
            # TODO: if the runparams file is already in the directory, do we replace it? (patch might not work if its the same path)
            patch = {'classicparams': {'DELT': forcing_timestep}}
            # self.runparams['classicparams']['DELT'] = forcing_timestep
            new_runparams = Path(self.dirpath, "runparams.txt")

            f90nml.patch(self.get_job_option("runparams_file"), patch, new_runparams)
            self.set_job_option("runparams_file", str(new_runparams))
            logger.info(f"Changing CLASSIC timestep (DELT) from {DELT} to {forcing_timestep} and creating new run parameters file")

        # set time bounds
        with nc.Dataset(converted[0]) as rootgrp:
            time = rootgrp['time'][:]  # classic nc files are 'day as YYYYMMDD.FFFF' which isn't a well-supported format
            t0 = str(time[0])
            tn = str(time[-1])
            start = datetime(year=int(t0[:4]),
                             month=int(t0[4:6]),
                             day=int(t0[6:8]))
            end = datetime(year=int(tn[:4]),
                           month=int(tn[4:6]),
                           day=int(tn[6:8]))

        self.set_simulation_range(start, end)

        # write paths to job_options_file
        for name in self.METFILES:
            self.set_job_option(name, path.join(path.realpath(dst), self.METFILES[name]), write=False)

        self.write_job_options()

    def copy_init(self):
        """ Ensure the init.nc file is located in the model directory so it can be modified """
        initfile = Path(self.get_job_option('init_file')).resolve()
        
        if not str(Path(self.dirpath).resolve()) in str(initfile):
            initfile_copy = Path(self.dirpath, initfile.name)
            shutil.copy(initfile, initfile_copy)

        self.set_job_option("init_file", str(initfile_copy), write=True)
    
    def set_simulation_range(self, start, end):
        """Set simulation duration and output according to start and end dates """
        met_start_year = start.year
        met_end_year = end.year
        met_start_doy = start.timetuple().tm_yday
        met_end_doy = end.timetuple().tm_yday
        
        self.set_job_option("readMetStartYear", met_start_year)
        self.set_job_option("readMetEndYear", met_end_year)

        self.set_job_option("JHHSTD", 1)
        self.set_job_option("JHHENDD", 365)
        self.set_job_option("JHHSTY", met_start_year)
        self.set_job_option("JHHENDY", met_end_year)

        self.set_job_option("JDSTD", 1)
        self.set_job_option("JDENDD", 365)
        self.set_job_option("JDSTY", met_start_year)
        self.set_job_option("JDENDY", met_end_year)

        self.set_job_option("JMOSTY", met_start_year)

    @staticmethod
    def check_forcing(forcing_file):
        with nc.Dataset(forcing_file) as rootgrp:
            if 'SH_sur' in rootgrp.variables:
                return True
        return False

    def ensure_forcing_date_coverage(self, job_options):
        model_start = self.get_job_option("")
        for f in ClassicDirectory.METFILES[1]:  # just check one of them 
            t_range = nc.Dataset(self.get_job_option(f))['time'][:]
            whole_days = pd.to_datetime(np.array(t_range, dtype='int').astype('str'), format="%Y%m%d")
            fractional_days = pd.to_timedelta(t_range - np.array(t_range, dtype='int'), unit='days')
            date_range = whole_days + fractional_days
            forcing_start = min(date_range)
            forcing_end = max(date_range)

    @staticmethod
    def is_valid_directory(dirpath):
        return (ClassicDirectory.__has_job_options(dirpath, verbose=False)
                and ClassicDirectory.__init_exists(dirpath, verbose=False))

    @staticmethod
    def __has_job_options(dirpath, verbose=True):
        job_options_file = path.join(dirpath, "job_options_file.txt")
        exists = path.isfile(job_options_file)

        if not exists and verbose:
            logger.warning(f"'job_options_file' {job_options_file} does not exist")
        
        return exists

    @staticmethod
    def __init_exists(dirpath, verbose=True):
        job_options_file = path.join(dirpath, "job_options_file.txt")
        job_options = f90nml.read(job_options_file)
        init_file = job_options['joboptions']['init_file']
        
        exists = path.isfile(init_file)
        
        if not exists and verbose:
            logger.warning(f"'init_file' {init_file} does not exist")
        
        return exists

    @property
    def job_options_file(self) -> str:
        """ Path to job_options_file.txt """
        return path.join(self.dirpath, "job_options_file.txt")
        
    def read_job_options(self):
        self.job_options = f90nml.read(self.job_options_file)

    def write_job_options(self):
        job_options_file = Path(self.dirpath, "job_options_file.txt")
        new_jof = Path(self.dirpath, "new_job_options_file.txt")
        f90nml.patch(job_options_file, self.job_options, new_jof)
        replace(new_jof, job_options_file)

    def set_output_directory(self, output_directory):
        if not self.get_job_option('output_directory') == path.realpath(self.dirpath):
            self.set_job_option('output_directory', output_directory)
            logger.info(f'Changed "output_directory" to {output_directory}')
        
    def get_job_option(self, keyword):
        return self.job_options['joboptions'][keyword]

    def set_job_option(self, keyword, value, write=True):
        self.job_options['joboptions'][keyword] = value
        if write:
            self.write_job_options()


def patch_job_options(job_options_namelist, job_options_file):
    dirpath = Path(job_options_file).parent
    if Path(job_options_file).is_file():
        new_jof = Path(dirpath, "new_job_options_file.txt")
        f90nml.patch(job_options_file, job_options_namelist, new_jof)
        replace(new_jof, job_options_file)

# https://gitlab.com/cccma/classic/-/blob/master/src/classStateVars.f90
# THLQACC_MO THICACC_MO  


class ClassicSoilTemperatureAggregator(GroupedNetCDFAggregator):
    """[summary]

    Parameters
    ----------
    SoilTemperatureAggregator : [type]
        [description]

    Returns
    -------
    [type]
        [description]
    """
    NAME = 'soil_temperature'
    SUPPORTS = ['classic']
    FREQUENCY = 'monthly'
    FREQ_OPTIONS = ['annually', 'monthly', 'daily', 'halfhourly']
    VAR_NAME = 'tsl'  # this needs to match what's defined in the classic output xml
    x = "THLQACC_MO"

    def __init__(self, modeldirectory):
        super().__init__(modeldirectory)
        self.dirname = path.basename(modeldirectory.dirpath)
        self.outdir = modeldirectory.job_options['joboptions']['output_directory']

    def aggregate(self, target):
        if not path.isfile(target):
            self.new(target)

        with nc.Dataset(target, 'a') as main:
            rootgrp = main['classic'] if 'classic' in main.groups else self.new_classic_group(main)

            i = self.write_simulation_name(rootgrp, self.dirname)
            self.add_output_variables(rootgrp, sim=i)

    def sanity_checks(self):
        pass
    
    def get_file(self, frequency='monthly'):
        if frequency not in self.FREQ_OPTIONS:
            raise ValueError(f"{frequency} must be one of: {self.FREQ_OPTIONS}")
        
        outfile = path.join(self.outdir, f"{self.VAR_NAME}_{frequency}.nc")
        return outfile

    def new_classic_group(self, main):
        data = self.get_file(self.FREQUENCY)
        with nc.Dataset(data) as template:
            rootgrp = main.createGroup('classic')

            for d in template.dimensions:
                dim = template.dimensions[d]
                _ = rootgrp.createDimension(dim.name, dim.size)
            
            _ = rootgrp.createDimension("nchars", 255)
            _ = rootgrp.createDimension("simulation", None)

            for v in template.variables:
                var = template.variables[v]
                if var.name == 'time':
                    dims = var.dimensions
                else:
                    dims = (*var.dimensions, "simulation")
                                
                newvar = rootgrp.createVariable(var.name, var.dtype, dims)

                for attr in var.ncattrs():
                    if attr[0] != "_":
                        newvar.setncattr(attr, var.getncattr(attr))

            _ = rootgrp.createVariable('simulation', "S1", ("simulation", "nchars"))
            
            rootgrp['time'][:] = template['time'][:]
        
        return rootgrp

    def new(self, target):
        with nc.Dataset(target, 'w') as main:
            _ = self.new_classic_group(main)

    def write_simulation_name(self, group, name):
        """
        Write the geotop simulation directory name to the next available simulation variable
        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            An open netcdf group or dataset
        name : str
            Name of simulation (obtained from directory name)

        Returns
        -------
            int : index of simulation
        """
        i = self.next_simulation_index(group)
        # name_arr = nc.stringtochar(np.array(name, dtype='S3'))
        group['simulation'][i, :] = name
        return(i)

    @staticmethod
    def next_simulation_index(group):
        """
        Get the next simulation index . This is used to determine where the next array slice
        should be written.

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            netcdf group or dataset

        Returns
        -------
            int : The index of the next empty array slice along the simulation dimension
        """
        return len(group['simulation'])

    def add_output_variables(self, group, sim):
        """
        Add current geotop output to a netcdf4 group or dataset "

        Parameters
        ----------
        group : netCDF4._netCDF4.Dataset
            an open netcdf group or dataset
        sim : int
            index along the simulation dimension where array should be written
        """
        
        data = self.get_file(self.FREQUENCY)

        with nc.Dataset(data) as original:

            for v in original.variables:
                var = original.variables[v]
                if v == 'time':
                    continue
                else:
                    group[v][..., sim] = original[v][:]


class ClassicSnowAggregator(ClassicSoilTemperatureAggregator):
    NAME = 'snow'
    VAR_NAME = 'wsnw'


class ClassicLiquidWaterAggregator(ClassicSoilTemperatureAggregator):
    NAME = 'liquid_water'
    VAR_NAME = 'mrsll'
    CODE_NAME = "THLQACC"

# mrsll =   = Volumetric liquid water content of soil layers
# mrsfl thicacc  In each soil layer, the mass of water in ice phase
#
# mrsol mrsol in each soil layer, the mass of water in all phases

# actlyrmax actlyr_max Maximum active layer depth
# actlyrmin actlyr_min Minimum Active Layer Depth


class ClassicGlobsim(ParameterSet):
    NAME = "classic-globsim"
    def __init__(self, globsim_file, cfg=None):
        self.globsim_file = globsim_file

    @property
    def filepath(self):
        return self.globsim_file

    def apply(self, modeldirectory, sim, cfg):
        """ Convert globsim data into CLASSIC netcdf files, """
        forcing_file = sim.get_forcing()
        site_name = sim.get_site_name()

        logger.info(f"Copying forcing data for {site_name} from {forcing_file}")

        dst = path.join(path.dirname(self.dirpath), 'temp', 'classic',
                        'meteo', site_name, Path(forcing_file).stem)
        if not path.isdir(dst):
            makedirs(dst)
        metloader = cfg.get('models', 'classic', 'metloader')
        container = cfg.get('models', 'classic', 'container')
        converted = globsim_to_classic_nc(forcing_file, dst, site_name, container, metloader)

        # write paths to job_options_file
        for name in self.METFILES:
            self.set_job_option(name, path.join(path.realpath(dst), self.METFILES[name]), write=False)

        self.write_job_options()
    
    def set_simulation_range(self, start, end):
        """Set simulation duration and output according to start and end dates """
        met_start_year = start.year
        met_end_year = end.year
        met_start_doy = start.timetuple().tm_yday
        met_end_doy = end.timetuple().tm_yday
        
        self.set_job_option("readMetStartYear", met_start_year)
        self.set_job_option("readMetEndYear", met_end_year)

        self.set_job_option("JHHSTD", met_start_doy)
        self.set_job_option("JHHENDD", met_end_doy)
        self.set_job_option("JHHSTY", met_start_year)
        self.set_job_option("JHHENDY", met_end_year)
        self.set_job_option("JDSTD", met_start_doy)
        self.set_job_option("JDENDD", met_end_doy)
        self.set_job_option("JDSTY", met_start_year)
        self.set_job_option("JDENDY", met_end_year)
        self.set_job_option("JMOSTY", met_start_year)

    @staticmethod
    def check_forcing(forcing_file):
        with nc.Dataset(forcing_file) as rootgrp:
            if 'SH_sur' in rootgrp.variables:
                return True
        return False

    def ensure_forcing_date_coverage(self, job_options):
        model_start = self.get_job_option("")
        for f in ClassicDirectory.METFILES[1]:  # just check one of them 
            t_range = nc.Dataset(self.get_job_option(f))['time'][:]
            whole_days = pd.to_datetime(np.array(t_range, dtype='int').astype('str'), format="%Y%m%d")
            fractional_days = pd.to_timedelta(t_range - np.array(t_range, dtype='int'), unit='days')
            date_range = whole_days + fractional_days
            forcing_start = min(date_range)
            forcing_end = max(date_range)


class ClassicTOMLParameter(ParameterSet):
    """
    Write classic parameters into a CLASSIC netcdf file from TOML-formatted text files.
    Child classes implement writing for either rsfile or initfile
    

    Parameters
    ----------
    filepath : str
        Path to a geotop inpts file

    """
    NAME = "classictoml-base"
    SUPPORTS = [None]
    NCFILEKEY = None

    def __init__(self, filepath: str):
        self._filepath = filepath
        self.inputs = self.read_input_file(filepath)

    @property
    def filepath(self):
        return self._filepath

    def read_input_file(self, filepath: str) -> dict:
        with open(filepath, encoding='utf8') as stream:
            config = tomlkit.parse(stream.read())
        return config

    def get_input_parameter(self, var_name: str) -> np.array:
        """ Get value of parameter from input file"""
        return np.atleast_1d(np.array(self.inputs.get(var_name)))

    def write_parameter(self, var_name: str, parameter_value: np.array, rootgrp: nc.Dataset):
        """ Write the parameter value. Each destination variable must have no more
            than one dimension with length greater than 1.
            Lat, lon, and tile dimensions must have length 1"""
        var = rootgrp[var_name]
        if sum([L != 1 for L in var.shape]) > 1:
            logger.error(f"Could not unambiguously write parameter {var_name}." +
                         f"Destination variable has too many non-single-length dimensions {[f'{x}: {y}' for x,y in zip(var.dimensions, var.shape)]}")
            return 
        var[:] = parameter_value

    def get_variable_names(self, inputs) -> list:
        """ return a list corresponding to the netcdf variables that will be written """
        return list(inputs.keys())

    def get_parameter_file(self, model_directory: ModelDirectory) -> str:
        """ Return path to netcdf file containing input parameters """
        try:
            return model_directory.get_job_option(self.NCFILEKEY)
        except KeyError as e:
            logger.error(f"No matching entry in job options for {self.NCFILEKEY}")

    def apply(self, model_directory, cfg:PemConfig=None):
        """ Apply parameters from file to target directory """
        
        ncfile = self.get_parameter_file(model_directory)
        
        with nc.Dataset(ncfile, 'a') as rootgrp:
            self.check_nc_file(rootgrp)
            
            for var_name in self.get_variable_names(self.inputs):
                parameter_value = self.get_input_parameter(var_name)
                self.write_parameter(var_name, parameter_value, rootgrp)
    
    def check_nc_file(self, rootgrp):
        """ Make sure assumptions are valid """
        if (rootgrp.dimensions['lat'].size > 1) or (rootgrp.dimensions['lat'].size > 1):
            msg = "This parameter-setting class does not handle multiple latitudes or longitudes"

        elif rootgrp.dimensions['tile'].size > 1:
            msg = "This parameter-setting class does not handle multiple tiles"
        
        else:
            return True
        
        logger.error(msg)
        raise RuntimeError(msg)


class ClassicTomlRS(ClassicTOMLParameter):

    NAME = "classictomlrs"
    SUPPORTS = ['classic']
    NCFILEKEY = 'rsfile.nc'


class ClassicTomlInit(ClassicTOMLParameter):

    NAME = "classictomlinit"
    SUPPORTS = ['classic']
    NCFILEKEY = 'init_file'
    
    def __init__(self, filepath):
        super().__init__(filepath)


def coff(job_options_file: str,
         id: Optional[int]=None,
         short_name: Optional[str]=None,
         name_in_code: Optional[str]=None, 
         freq_pref: "list[str]"=['half-hourly', 'daily', 'monthly']
         ) -> str:
    """ Classic Output File Finder (coff) """

    jo = f90nml.read(job_options_file)
    od = jo['joboptions']['output_directory']

    if id is not None:
        xml = jo['joboptions']['xmlFile']
        with open(xml) as f:
            raise NotImplementedError
    elif short_name is not None:
        prefix = short_name
    elif name_in_code is not None:
        pass
    else:
        raise ValueError("One of id, short_name, name_in_code must be provided")
    
    for frequency in freq_pref:
        output_file = Path(od, f"{prefix}_{frequency}.nc")

        if output_file.is_file():
            return str(output_file)

    raise FileNotFoundError