import netCDF4 as nc
import subprocess
import numpy as np
import re
from os import path
from pathlib import Path
from pandas import read_fwf, to_datetime

try:
    from globsim.exporttools import globsim_to_classic_met
except ModuleNotFoundError:
    print("install globsim")


def globsim_to_classic_nc(globsim_file, output_directory, site, container, metASCIILoader, silent=True):
    """Convert globsim output to a classic netCDF output file

    Parameters
    ----------
    globsim_file : str
        pathlike; the location of a globsim output file
    output_directory : str
        pathlike; where the output netcCDF files should be created
    site : int or str
        index of site (zero-indexed) or site name if only one site is desired.
    metASCIILoader : str
        path to script
    """
    metfile = globsim_to_classic_met(globsim_file, output_directory, site)[0]
    
    with nc.Dataset(globsim_file) as f:
        names = nc.chartostring(f['station_name'][:])
        i = site if isinstance(site, int) else np.where(names == site)
        lat = float(f['latitude'][i])
        lon = float(f['longitude'][i])
        
    filenames = metASCIILoaderpy(metfile, container, metASCIILoader, lon, lat, silent=silent)
    return metfile, filenames


def metASCIILoaderpy(metfile, container, metASCIILoader, lon, lat, out=None, silent=True):
    """Run the metASCIILoader
   
    Parameters
    ----------
    metfile : str
        pathlike pointing to classic ASCII meteorological file
    container : str
        path to singularity container for CLASSIC
    metASCIILoader : str
        Full path to metASCIILoader script. Usually in the CLASSIC code directory under `tools`. (e.g.
        CLASSIC/tools/metASCIILoader/bin/metASCIILoader)
    lon : float
        longitude of site
    lat : float
        latitude of site
    out : str, optional
        where the output files should be created, by default creates files in the 
        same directory as metfile.

    Returns
    -------
    str : path to newly created files
    """
    singularity_cmd = 'singularity'

    bindpath = "{}:/files".format(Path(metfile).parent)
    execfile = str(Path("/files", path.basename(metfile)))

    bindmet = "{}:/met".format(Path(metASCIILoader).parent)
    execmet = str(Path("/met", path.basename(metASCIILoader)))

    out = out if out else str(Path(metfile).parent)
    
    leap = "true" if has_leap(metfile) else "false"

    commands = [singularity_cmd, "exec",
                          "-B", bindpath,
                          "-B", bindmet,
                          "--pwd", "/files", #  MetASCIILoader creates files in current directory
                          container, execmet, execfile, str(lon), str(lat), leap]
    if not silent:
        print(f"\nCalling:\n{' '.join(commands)}\n")

    res = subprocess.run(commands,
                          cwd=out)
    filenames = list(Path(metfile).parent.glob("met*"))
    return filenames

def has_leap(filepath):
    """ Determine whether a classic ASCII met file has leap years """
    leap = False
    pattern = re.compile(r"\d{2} \d{2}  366\s{2}\d{4}")
    
    with open(filepath) as met:
        for line in met:
            if pattern.search(line):
                leap = True
                
    
    return leap

def get_timestep(filepath, nrows=500):
    """ Determine timestep (in seconds) of globsim CLASSIC METfile output.
    Returns -1 if timestep is unequal
    """
    table = read_classic_MET(filepath, nrows=nrows, dtype=str)
    dates = to_datetime(table.agg("".join, axis=1), format="%H%M%j%Y")
    
    DELT = np.array(np.diff(dates), dtype=int) / 1.0e9
    
    if not len(set(DELT)) == 1:
        return -1

    return DELT[0]

def read_classic_MET(filepath, **kwargs):
    colspecs = [(1,3), (4,6), (8,11), (13,17)]
    names = ["HH", "MM", "JJJ", "YYYY"]
    table = read_fwf(filepath, colspecs=colspecs, names=names, **kwargs)
    return table

def get_date_range(filepath):
    """ Get the date range for the forcing files """
    table = read_classic_MET(filepath, dtype=str)
    dates = to_datetime(table.agg("".join, axis=1), format="%H%M%j%Y")
    start = min(dates)
    end = max(dates)
    return start, end

if __name__ == "__main__":
    met="/gpfs/fs1/home/s/stgruber/nbr512/temp/2_Site_3_ERA.MET"
    container="/project/s/stgruber/_shared/CLASSIC/CLASSIC_container.simg"
    loader ="/project/s/stgruber/_shared/CLASSIC/tools/metASCIILoader/bin/metASCIILoader"
    lon = -142
    lat = 62
    metASCIILoaderpy(met, container, loader, lon, lat)
