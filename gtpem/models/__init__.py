import sys
import inspect
import logging
from .template import Simulation, ModelDirectory, ParameterSet, ModelAggregator, ParameterFactory

from typing import Type, Optional

from .geotop.geotop import *
from .classic.classic import *
from .testmodel.testmodel import *
from .freethaw.freethaw import *
from .generic import *


logger = logging.getLogger(__name__)


def lookup_directory(model_name: str) -> "Optional[Type[ModelDirectory]]":
    """Get a ModelDirectory object based on the model name

    Parameters
    ----------
    model_name : str
        name of model as described in the MODEL class attribute

    Returns
    -------
    ModelDirectory
        A ModelDirectory class corresponding to the specified model
    """
    class_list = inspect.getmembers(sys.modules['gtpem.models'], inspect.isclass)
    lookup = {CLASS.MODEL: CLASS for (real_name, CLASS) in class_list if issubclass(CLASS, ModelDirectory)}
    try:
        return lookup[model_name]
    except KeyError as e:
        return None


def lookup_simulation(model_name) -> "Optional[Type[Simulation]]":
    """Get a Simulation object based on the model name

    Parameters
    ----------
    model_name : str
        name of model as described in the MODEL class attribute

    Returns
    -------
    Simulation
        A Simulation class corresponding to the specified model
    """
    class_list = inspect.getmembers(sys.modules['gtpem.models'], inspect.isclass)
    lookup = {CLASS.MODEL: CLASS for (real_name, CLASS) in class_list if issubclass(CLASS, Simulation)}
    try:
        return lookup[model_name]
    except KeyError as e:
        return None


def lookup_parameter_set(ps_name, model) -> "Optional[Type[ParameterSet]]":
    """Get a ParameterSet object based on the model name

    Parameters
    ----------
    parameterset_name : str
        The name of the desired ParameterSet class according to its definition

    Returns
    -------
    ParameterSet
        A ParameterSet class with the specified name
    """
    class_list = inspect.getmembers(sys.modules['gtpem.models'], inspect.isclass)
    lookup = {CLASS.NAME: CLASS for (real_name, CLASS) in class_list if issubclass(CLASS, ParameterSet) and model in CLASS.SUPPORTS}
    try:
        return lookup[ps_name]
    except KeyError as e:
        return None


def list_generic(cls):
    class_list = inspect.getmembers(sys.modules['gtpem.models'], inspect.isclass)
    X = {CLASS.NAME: CLASS for (real_name, CLASS) in class_list if issubclass(CLASS, cls)}
    return X


def list_parameter_sets():
    return list_generic(ParameterSet)


def list_parameter_factories():
    return list_generic(ParameterFactory)


def lookup_parameter_factory(pf_name, model) -> "Optional[Type[ParameterFactory]]":
    """Get a ParameterFactory object based on the model name

    Parameters
    ----------
    pf_name : str
        The name of the desired ParameterFactory class according to its definition

    Returns
    -------
    ParameterFactory
        A ParameterFactory class with the specified name
    """
    class_list = inspect.getmembers(sys.modules['gtpem.models'], inspect.isclass)
    lookup = {CLASS.NAME: CLASS for (real_name, CLASS) in class_list if issubclass(CLASS, ParameterFactory) and model in CLASS.SUPPORTS}
    try:
        return lookup[pf_name]
    except KeyError as e:
        return None


def lookup_aggregator(agg_name, model) -> "Optional[Type[ModelAggregator]]":
    """Get a ModelAggregator object based on the model name

    Parameters
    ----------
    name : str
        The NAME class attribute of the desired ModelAggregator
    model : str
        The NAME class attribute of the model

    Returns
    -------
    ModelAggregator
        A ModelAggregator class with the specified name which supports the desired model
    """
    class_list = inspect.getmembers(sys.modules['gtpem.models'], inspect.isclass)
    lookup = {CLASS.NAME: CLASS for (real_name, CLASS) in class_list if issubclass(CLASS, ModelAggregator) and model in CLASS.SUPPORTS}
    try:
        Aggregator = lookup[agg_name]
    except KeyError as e:
        logger.error(f"Aggregator with name '{agg_name}' not registered for {model} model. Check library.py")
        Aggregator = None

    return Aggregator
