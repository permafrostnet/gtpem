from gtpem.core import PemConfig
from gtpem.models.classic import classic



if __name__ == "__main__":

    import sys
    import subprocess

    c = PemConfig.empty()
    c._config['models']['classic'] = {}
    c._config['models']['classic']['container'] = sys.argv[2]
    c._config['models']['classic']['binary'] = sys.argv[3]

    command =  classic.ClassicSimulation(c, sys.argv[1]).command()
    print(command)
    #subprocess.Popen(command)
    