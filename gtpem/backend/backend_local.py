from string import Template
import logging
import os
import subprocess
from gtpem.backend import Backend

logger = logging.getLogger(__name__)


class LocalBackend(Backend):
    def __init__(self, cfg, dryrun):
        super().__init__(cfg, dryrun)
        
    def start(self, simulations):
        """Run jobs directly on local host

        Might consider running multiple models at the same time, but
        for now this backend runs them sequentially

        """
        if not(self.dryrun):
            errmsg = "All simulations completed successfully!"
            for sim in simulations:
                cmd = sim.command()
                logger.info(f"Running {cmd}")
                try:
                    subprocess.call(cmd, shell=True, cwd=self.job_dir)
                except subprocess.CalledProcessError as e:
                    # I wonder if we should capture error information here
                    # and return it?
                    # https://docs.python.org/3/library/subprocess.html#subprocess.CalledProcessError
                    errmsg = "WARNING: Some simulations returned error codes"
            return errmsg
        else:
            return "No simulations ran because this is a dry-drun"


