import sys
import math
import logging
import os
import re

import numpy as np

from gtpem.backend import SlurmBackend

logger = logging.getLogger(__name__)

class NiagaraBackend(SlurmBackend):
    """Creates SLURM scripts to run all simulations in one or more cluster
    jobs, with constraints specific to the ComputeCanada Niagara
    cluster.

    """
    
    SCRIPT_TEMPLATE = """#!/bin/bash
#SBATCH --nodes=${num_compute_nodes}
#SBATCH --ntasks-per-node=40
#SBATCH --time=0-${duration}
#SBATCH --partition=${account}

module load CCEnv
module load nixpkgs gcc openmpi
module load glost

echo "Starting ${job_name} at: `date`"

srun glost_launch ${glost_fname}

echo "Finished ${job_name} with exit code $$? at: `date`"
"""

    def __init__(self, cfg, dryrun):
        super()._initialize_generic(cfg, dryrun)
        self.num_compute_nodes = self.cfg.get('backend', self.classname, 'nodecount', datatype='Integer')
        self.num_jobs = self.cfg.get('backend', self.classname, 'jobcount', datatype='Integer')
        self.duration = self.cfg.get('backend', self.classname, 'duration', datatype='String')
        self.slurm_account = self.cfg.get('backend', self.classname, 'account', datatype='String')
        self.script_template = NiagaraBackend.SCRIPT_TEMPLATE

    def _validate(self, simulations):
        if self.slurm_account == "debug":
            if self.num_compute_nodes > 1:
               logger.error(f"ERROR: Invalid 'nodecount' ({self.num_compute_nodes}). Maximum node count for 'debug' is 1.")
               sys.exit(1)
            
            duration = re.match(r"(\d\d):(\d\d):?(\d?\d?)", self.duration)
            
            if (int(duration.group(1)) > 1) or ((int(duration.group(1)) == 1) and (int(duration.group(2)) != 0)):
                logger.error(f"ERROR: Invalid 'duration' ({self.duration}). Maximum duration for 'debug' is 01:00:00.")
                sys.exit(1)

        if self.num_jobs < 1:
            logger.error("ERROR: Invalid 'jobcount' - must create at least one cluster job")
            sys.exit(1)
        if self.num_jobs > len(simulations):
            logger.error("ERROR: Invalid 'jobcount' - cannot have more cluster jobs than simulations")
            sys.exit(1)
        if self.num_compute_nodes < 1:
            logger.error("ERROR: Invalid 'nodecount' - must use at least one Niagara node")
            sys.exit(1)
        if math.floor(len(simulations)/self.num_jobs) < 40*self.num_compute_nodes:
            if self.slurm_account == 'debug':
                logger.warning("Each Niagara job should have at least 40 simulations per compute node. Job submissions on Niagara on accounts other than 'debug' will result in an error.")
            else:
                logger.error("ERROR: Each Niagara job should have at least 40 simulations per compute node")
                sys.exit(1)

    def _prep_jobs(self, simulations):
        job_info = []
        sim_chunk = np.array_split(simulations, self.num_jobs)
        for i in range(len(sim_chunk)):
            job_name = self.job_name + "-" + str(i)
            job_info.append({
                'simulations': sim_chunk[i],
                'job_name': job_name,
                'duration': self.duration,
                'num_compute_nodes': self.num_compute_nodes,
                'account': self.slurm_account,
                'glost_fname': os.path.join(self.job_dir, f'glost_{job_name}.sh'),
                'slurm_fname': os.path.join(self.job_dir, f'submit_{job_name}.sh'),
            })
        return job_info
