import os
import sys
import subprocess
from string import Template
import math

import numpy as np

from gtpem.backend import Backend


class SlurmBackend(Backend):
    """Creates SLURM scripts to run all simulations in one or more cluster jobs

    This implementation assumes that the simulation folders are
    created on the login node, and are accessible on all the cluster's
    compute nodes.

    Also assumes that there is one CPU core (i.e. SLURM's ntasks) per
    simulation in a SLURM job.

    """

    SCRIPT_TEMPLATE = """#!/bin/bash
#SBATCH --ntasks=${num_tasks}
#SBATCH --time=0-${duration}
#SBATCH --partition=${account}

echo "Starting ${job_name} at: `date`"

srun glost_launch ${glost_fname}

echo "Finished ${job_name} with exit code $$? at: `date`"
"""

    JOB_INSTRUCTIONS = """To monitor status of your incomplete jobs, run this command:

   squeue --me

"""

    def __init__(self, cfg, dryrun):
        super()._initialize_generic(cfg, dryrun)
        self.num_cores = self.cfg.get('backend', self.classname, 'corecount')
        self.num_jobs = self.cfg.get('backend', self.classname, 'jobcount', datatype='Integer')
        self.duration = self.cfg.get('backend', self.classname, 'duration', datatype='String')
        self.slurm_account = self.cfg.get('backend', self.classname, 'account', datatype='String')
        self.script_template = SlurmBackend.SCRIPT_TEMPLATE

    def _validate(self, simulations):
        if self.num_jobs < 1:
            print("ERROR: Invalid 'jobcount' - must create at least one cluster job")
            sys.exit(1)
        if self.num_jobs > len(simulations):
            print("ERROR: Invalid 'jobcount' - cannot have more cluster jobs than simulations")
            sys.exit(1)
        if math.floor(len(simulations)/self.num_jobs) < self.num_cores:
            print("ERROR: Jobs have more cores than simulations")
            sys.exit(1)

    def _prep_jobs(self, simulations):
        job_info = []
        sim_chunk = np.array_split(simulations, self.num_jobs)
        for i in range(len(sim_chunk)):
            job_name = self.job_name + "-" + str(i)
            if self.num_cores > 0:
                num_tasks = self.num_cores
            else:
                num_tasks = len(sim_chunk[i])
            job_info.append({
                'simulations': sim_chunk[i],
                'job_name': job_name,
                'duration': self.duration,
                'account': self.slurm_account,
                'num_tasks': num_tasks,
                'glost_fname': os.path.join(self.job_dir, f'glost_{job_name}.sh'),
                'slurm_fname': os.path.join(self.job_dir, f'submit_{job_name}.sh'),
            })
        return job_info
        
    def _write_glost(self, job_info):
        """ Write glost task list from a list of simulations. """
        for j in job_info:
            with open(j['glost_fname'], 'w') as GLOST:
                for sim in j['simulations']:
                    # create list of commands to run each model simulation
                    GLOST.write(sim.command() + '\n')

    def _write_slurm(self, job_info):
        """ Write SLURM submission script. """
        # Create a SLURM submission script to run the model commands
        # on the cluster
        slurm_template = Template(self.script_template)
        for j in job_info:
            with open(j['slurm_fname'], 'w') as F:
                F.write(slurm_template.substitute(j))

    def _submit(self, job_info):
        """ submit slurm job. """
        for j in job_info:
            subprocess.call(['sbatch', j['slurm_fname']], cwd=self.job_dir)

    def start(self, simulations):
        self._validate(simulations)
        job_info = self._prep_jobs(simulations)
        self._write_glost(job_info)
        self._write_slurm(job_info)
        if not(self.dryrun):
            self._submit(job_info)
            return SlurmBackend.JOB_INSTRUCTIONS
        else:
            return "Dry run - not running any jobs"
