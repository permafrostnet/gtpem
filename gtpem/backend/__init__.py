import sys
import inspect

from .backend import Backend
from .backend_local import LocalBackend
from .backend_slurm import SlurmBackend
from .backend_niagara import NiagaraBackend

def lookup_backend(backend_name):
    class_lookup = dict(inspect.getmembers(sys.modules['gtpem.backend'], inspect.isclass))
    return class_lookup[backend_name]
