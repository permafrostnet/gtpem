import os

from abc import ABC, abstractmethod


class Backend(ABC):
    def __init__(self, cfg, dryrun):
        self._initialize_generic(cfg, dryrun)

    def _initialize_generic(self, cfg, dryrun):
        self.dryrun = dryrun
        self.cfg = cfg
        self.classname = self.__class__.__name__
        self.job_name = self.cfg.get_job_name()
        self.work_dir = self.cfg.get_working_directory()
        self.job_dir = os.path.join(self.work_dir, self.job_name)
    
    @abstractmethod
    def start(self, simulations):
        """Start simulations on compute resources

        We might asynchronously submit job(s) to a cluster scheduling
        system for the user to monitor themselves, or synchronously
        run the simulations and wait for them to complete before
        returning from this function.  It depends on the specific
        backend.

        Parameters
        ----------
        simulations: list of Simulation objects

        Returns
        -------
        str : string
            Message to display to user - status message and/or
            instructions to directly monitor job(s)

        """
        pass
