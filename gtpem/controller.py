import json
import logging
import os
import re
import uuid
import hashlib
import pandas as pd

from pathlib import Path
from itertools import product
from typing import Optional

from gtpem.core import PemConfig
from gtpem.models.template import Simulation
from gtpem.models import lookup_directory, lookup_parameter_set, lookup_simulation, lookup_parameter_factory, list_parameter_factories
from gtpem.models.library import valid_models
from gtpem.models.template import Simulation
from gtpem.names import model_name_abbreviation

logger = logging.getLogger(__name__)

# Directory patterns to ignore when checking directory types
ignore_directories = [re.compile("^temp$"),
                      re.compile("^temp$")]


class JobList:
    """
    Class that contains all the simulations necessary for a job,
    can be built from a configuration file

    Parameters
    ----------
    cfg : gtpem control file object

    """

    def __init__(self, cfg: PemConfig, abstractions):
        self.cfg = cfg
        self.abstractions = abstractions

    @classmethod
    def from_naive_combination(cls, cfg: PemConfig):
        abstractions = naive_abstraction_list(cfg)
        self = cls(cfg, abstractions)
        
        return self

    @classmethod
    def with_factories(cls, cfg: PemConfig):
        models = cfg.get_models()
        sites = cfg.get_sites()
        abstractions = build_abstraction_list(cfg=cfg, models=models, meteo_sites=sites)
        self = cls(cfg, abstractions)

        return self

    def build_abstraction_list(self):
        """
        Create a collection of simulations from a configuration file
        """
        models = self.cfg.get_models()
        sites = self.cfg.get_sites()
        reanalyses = self.cfg.get_reanalyses()

        self.abstractions = []

        for m in models:
            for r in sites:
                for s in sites[r]:
                    for parameter_set in self.cfg.naive_combination(m):
                        if parameter_set == []:
                            continue
                        else:
                            abstraction = SimulationAbstraction(model=m, site=s, forcing=r, parameters=parameter_set)
                            self.abstractions.append(abstraction)

        return self.abstractions

    def write_abstractions(self):
        dst = self.cfg.get_working_directory()
        abstraction_list = []
        for s in self.abstractions:
            abs_path = s.write(dst)
            abstraction_list.append(abs_path)

        return abstraction_list

    def table(self):
        parameters = set([t for a in self.abstractions for (p, t) in a.parameters])
        df = pd.DataFrame({
             "id": [a.param["id"] for a in self.abstractions],
             "model": [a.param["model"] for a in self.abstractions],
             "site": [a.param["site"] for a in self.abstractions],
             "forcing": [a.param["forcing"] for a in self.abstractions],
             "parameters": [a.parameters for a in self.abstractions],
        })

        for p in parameters:
            try:
                header = Path(p).name
            except Exception:
                header = p
            
            df[header] = [header in str(a.parameters) for a in self.abstractions]
        
        return df
    

def naive_abstraction_list(cfg) -> "list[SimulationAbstraction]":
    """
    Create a collection of simulations from a configuration file
    """
    models = cfg.get_models()
    sites = cfg.get_sites()
    reanalyses = cfg.get_reanalyses()

    abstractions = []

    for m in models:
        for r in sites:
            for s in sites[r]:
                for parameter_set in cfg.naive_combination(m):
                    abstraction = SimulationAbstraction(model=m, site=s, forcing=r, parameters=parameter_set)
                    abstractions.append(abstraction)

    return abstractions


class SimulationAbstraction:
    """
    Abstracts the description of a single simulation.

    Contains minimal information about
    the locale or paths necessary to construct the simulation.

    A class that abstracts the information necessary to build a single
    directory for a simulation.

    """

    def __init__(self, model: str, site: str, forcing: str, parameters: "list[tuple[str, str]]" = [], id: "Optional[str]" = None):
        self.param = dict()
        self.param['id'] = id if id else uuid.uuid4().hex
        self.param['model'] = model
        self.param['site'] = site
        self.param['forcing'] = forcing
        self._parameters = parameters

    def __str__(self):
        return f"Instructions for a {self.get_model()} simulation at {self.get_site_name()}"

    @property
    def parameters(self) -> "list[tuple[str, str]]":
        return self._parameters

    @parameters.setter
    def parameters(self, value):
        try:
            value[0]
        except TypeError as e:
            raise Exception("Parameter sets must be a iterable collection of tuples") from e
        
        if not len(value[0]) == 2:
            raise ValueError("Parameter sets must be provided as tuples of length 2 ('ParameterSet name', 'target')")
        
        self._parameters = value

    def get_id(self):
        return self.param['id']

    def get_site_name(self):
        return self.param['site']

    def get_model(self):
        "Return name of model"
        return self.param['model']

    def get_forcing(self) -> str:
        "Full path to forcing file"
        return self.param['forcing']

    def get_parameters(self):
        res = [lookup_parameter_set(parameter, self.get_model())(target)
               for parameter, target in self._parameters]
        return res
    
    @property
    def directory_name(self):
        """ A readable and (hopefully unique) directory name """
        model_name = model_name_abbreviation(self.get_model())
        forcing = re.sub('scaled_([a-zA-Z0-9]*)_.*', r'\1', Path(self.get_forcing()).stem)
        name = "_".join([model_name,
                        re.sub(r"[^\w]*", "", self.get_site_name()),
                        forcing])
        uniq = hashlib.sha1("".join([self.get_id(),
                                     str(self.parameters)
                                     ]).encode("UTF-8")
                            ).hexdigest()
        
        return f"{name}_{uniq[:7]}"

    def write(self, dst):
        """
        dumps the contents of the simulation abstraction to a file that
        can be loaded later on.
          - submitted to  run / built by a model
        """
        out_file = Path(dst, self.get_id() + ".json")
        out_dict = self.param.copy()
        out_dict['parameters'] = self.parameters

        with open(out_file, 'w') as f:
            json.dump(out_dict, f)

        return out_file

    @classmethod
    def from_file(cls, filepath):
        with open(filepath) as file:
            raw = json.load(file)

        model = raw['model']
        site = raw['site']
        forcing = raw['forcing']
        id = raw['id']

        sim = cls(model=model, site=site, forcing=forcing, id=id)
        return sim

    def to_lines(self):
        """ Writes parameters to CSV lines """
        lines = (",".join(["id", "site", "directory", "model", "forcing", "parameters"]) + "\n",
                 ",".join([self.get_id(),
                           self.get_site_name(),
                           self.directory_name,
                           self.get_model(),
                           Path(self.get_forcing()).stem,
                           ";".join([f"{p}:{t}" for p,t in self.parameters])]) + "\n")
        
        return lines


def run_model(config_file, simulation_file):
    cfg = PemConfig(config_file)

    # find out what kind of model to use &  build the directory
    abstraction = SimulationAbstraction.from_file(simulation_file)
    mod_name = abstraction.get_model()
    Directory = lookup_directory(mod_name)
    if not Directory:
        raise KeyError(f"No simulation directory for {mod_name}")
    
    directory = Directory.from_simulation_abstraction(cfg, abstraction)

    # run the model
    Sim = lookup_simulation(mod_name)
    
    if not Sim:
        raise KeyError(f"No Simulation class for {mod_name}")
    
    simulation = Sim(cfg, directory.dirpath)
    simulation.run(directory)

    # aggregate

    os.remove(simulation_file)


def simulation_from_abstraction(cfg, abstraction) -> Simulation:
    modeltype = abstraction.get_model()
    Directory = lookup_directory(modeltype)
    directory = Directory.from_simulation_abstraction(cfg, abstraction)
    
    if not directory:  # TODO: improve handling (added for classic directories w/ JRA
        logger.error(f"Could not create directory for simulation abstraction with properties:\n{abstraction.param}\n")
        return
    Sim = lookup_simulation(modeltype)
    
    if Sim is None:
        logger.error(f"could not find simulation for {modeltype}")
        raise ValueError(f"could not find simulation for {modeltype}")

    simulation = Sim(cfg, directory.dirpath)
    add_directory_to_manifest(cfg, abstraction)
    
    return simulation


def add_directory_to_manifest(cfg: PemConfig, simulation: SimulationAbstraction):
    """ Add a line to file linking the directory name with the simulation properties """
    dirfile = Path(cfg.get_working_directory(), cfg.get_job_name(), "folder_manifest.csv")
    
    lines = simulation.to_lines()
    already_exists = dirfile.is_file()
    
    with open(dirfile, 'a') as f:
        
        if not already_exists:
            f.writelines(lines[0])  # header line
       
        f.writelines(lines[1])


def detect_directory_type(dirpath):
    if any(map(lambda p: p.search(Path(dirpath).name), ignore_directories)):
        logger.debug(f"Directory {dirpath} ignored")
        return

    for modeltype in valid_models():
        logger.debug(f"Checking if {dirpath} can be run with {modeltype}")
        
        try:
            Directory = lookup_directory(modeltype)
            if Directory.is_valid_directory(dirpath):
                return modeltype
        
        except KeyError as e:
            logger.debug(f"Model {e} is not configured. Did you forget to include a MODEL class attribute? See 'Contributing' documentation.")
        
    logger.error(f"Directory {dirpath} is not valid for any configured models")
    return


def simulations_from_directories(cfg, rootdir) -> "list[Simulation]":
    """ Return a list of simulation objects from a directory of valid model directories """
    simulations = list()
    
    for f in os.listdir(rootdir):
        sim_dir = Path(rootdir, f)
                
        if Path(sim_dir).is_dir():
            modeltype = detect_directory_type(sim_dir)
            
            if modeltype is not None:
                Sim = lookup_simulation(modeltype)
                
                if Sim is None:
                    logger.error(f"could not find simulation for {modeltype}")
                    continue 

                sim = Sim(cfg, sim_dir)
                simulations.append(sim)

    return simulations


def build_abstraction_list(cfg, models: "list[str]", meteo_sites: "dict[str, list[str]]") -> "list[SimulationAbstraction]":
    """
    Create a collection of simulations from a configuration file
    meteo : dict {(path to reanalysis): (name of )
    """
    
    abstractions = []

    for m in models:
        for r in meteo_sites:  # reanalysis
            for s in meteo_sites[r]:  # site
                # make parameter sets
                all_parameters = cfg.get_model_parameters(m)
                
                # Expand parameters
                factories = list_parameter_factories()
                for parameter_option in all_parameters:  # parameter_option : [[parameter, target], [parameter, target]]
                    for i, o in enumerate(parameter_option):  # o: [parameter, target]
                        if o[0] in factories.keys():
                            
                            Factory = lookup_parameter_factory(o[0], m)
                            if not Factory:
                                raise RuntimeError()
                            factory = Factory(o[1])
                           
                            parameter_option[i: i + 1] = factory.generate_text(s)
            
                # remove empty parameter options
                all_parameters = [p for p in all_parameters if len(p) > 0]
                
                combinations = [p for p in product(*all_parameters)]

                for parameter_combination in combinations:
                    if len(parameter_combination) == 0:
                        continue
                    abstraction = SimulationAbstraction(model=m, site=s, forcing=r, parameters=parameter_combination)
                    abstractions.append(abstraction)

    return abstractions
