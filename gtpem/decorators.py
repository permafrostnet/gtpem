from typing import Optional


def deprecated(recommendation: "Optional[str]" = None, logger=None):
    def decorator(func):
        def wrapper(*args, **kwargs):

            message = f"Function '{func.__name__}' is deprecated. {recommendation}"
            
            if logger:
                logger.warning(message)
            else:
                print(message)

            result = func(*args, **kwargs)

            return result
        return wrapper
    return decorator
