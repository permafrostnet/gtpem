import os
import logging

from gtpem.core import PemConfig
from gtpem.controller import simulation_from_abstraction, simulations_from_directories, JobList
from gtpem.backend import lookup_backend
from gtpem.models.library import library
from gtpem.models import lookup_aggregator


logger = logging.getLogger(__name__)


def build(args):

    cfg = PemConfig(args.config)

    if args.job_dir is not None:
        cfg.config['jobdata']['title'] = os.path.basename(args.job_dir)
        cfg.config['jobdata']['workingdirectory'] = os.path.dirname(args.job_dir)

    wd = os.path.join(cfg.get_working_directory(), cfg.get_job_name())

    jobs = JobList.with_factories(cfg)
    simulations = [simulation_from_abstraction(cfg, a) for a in jobs.abstractions]


def run(args):

    cfg = PemConfig(args.config)

    if args.job_dir is not None:
        cfg._config['jobdata']['title'] = os.path.basename(args.job_dir)
        cfg._config['jobdata']['workingdirectory'] = os.path.dirname(args.job_dir)

    wd = os.path.join(cfg.get_working_directory(), cfg.get_job_name())
    
    if args.build_dirs:
        # Make simulations automatically from control file
        jobs = JobList.with_factories(cfg)
        simulations = [simulation_from_abstraction(cfg, a) for a in jobs.abstractions]
        simulations = [s for s in simulations if s is not None]
    else:
        # Make Simulations from existing directories
        simulations = simulations_from_directories(cfg, wd)

    if args.restart:  # Remove _FAILED_RUN and _SUCCESSFUL_RUN flags
        for sim in simulations:
            sim.reset_status()

    else:  # Remove failed or successfully completed simulations from list
        simulations = list(filter(lambda s: not s.successful_run(), simulations))
        simulations = list(filter(lambda s: not s.failed_run(), simulations))
    
    # run
    Backend = lookup_backend(cfg.get('jobdata', 'backend', datatype='String'))
    backend = Backend(cfg, args.dry_run)
    print(backend.start(simulations))


def status(args):

    cfg = PemConfig(args.config)

    if args.job_dir is not None:
        cfg._config['jobdata']['title'] = os.path.basename(args.job_dir)
        cfg._config['jobdata']['workingdirectory'] = os.path.dirname(args.job_dir)

    wd = os.path.join(cfg.get_working_directory(), cfg.get_job_name())
    simulations = simulations_from_directories(cfg, wd)

    if args.details:
        for sim in simulations:
            if sim.successful_run():
                print(f"{sim} SUCCESS")
            elif sim.failed_run():
                print(f"{sim} FAIL")
            else:
                print(f"{sim} INCOMPLETE")
        print()

    success = sum([sim.successful_run() for sim in simulations])
    fail = sum([sim.failed_run() for sim in simulations])
    
    total = len(simulations)
    
    print(f"{success} completed successfully and {fail} failed out of {total}")


def aggregate(args):
    
    cfg = PemConfig(args.config)
    wd = os.path.join(cfg.get_working_directory(), cfg.get_job_name())
    for method in cfg.get_aggregators():
        # make new aggregation file
        # TODO: improve creation of target file (should a generic aggregator be required?)
        
        # Get class object
        generic_aggregator = library['generic']['aggregate'][method]
        
        # Create output file, pass path to file
        aggregated_file = generic_aggregator.generate_filename(wd, f"result_{method}")
        
        # Initialize new output, then fill. 
        generic_aggregator.new(aggregated_file)
        
        for sim in simulations_from_directories(cfg, wd):
            if not sim.successful_run():
                logger.warning(f"Simulation in directory {sim.directory.dirpath} was not successful. Not included in aggregation.")
                pass
            
            ModelAggregator = lookup_aggregator(method, model=sim.MODEL)
            if ModelAggregator:
                aggregator = ModelAggregator(sim.directory)
                aggregator.aggregate(aggregated_file)