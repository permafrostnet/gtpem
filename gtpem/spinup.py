import xarray as xr
import f90nml
import shutil 
import numpy as np

from abc import ABC, abstractmethod
from datetime import datetime
from os import replace
from pathlib import Path
from typing import Type, Any, Optional

from gtpem.core import PemConfig
from gtpem.names import CLASSIC
from gtpem.models.classic.classic import ClassicDirectory, ClassicSimulation, coff


def stepwise_spinup(path: str,
                    model_type: str,
                    spin_times: "list[int]",
                    spin_start: "list[datetime]",
                    spin_end: "list[datetime]",
                    column_depth: "list[int]",
                    init_in_new_periods: int,
                    save_state: "Optional[list[bool]]"=None,
                    save_results: "Optional[list[bool]]"=None,
                    **kwargs) -> None:
    """ Launch a geotop-style stepwise spin-up with selected model """
    if save_state is None:
        save_state = [False for time in spin_times]
    if save_results is None:
        save_results = [False for time in spin_times]
    
    # Sanity checks
    assert(len(spin_times) == len(spin_start) == len(spin_end) == len(column_depth) == len(save_state) == len(save_results))

    # Initialize classes
    schedule = _expand_schedule(spin_times, 
                                spin_start=spin_start,
                                spin_end=spin_end,
                                column_depth=column_depth,
                                save_state=save_state,
                                save_results=save_results)

    ModelSpinner = get_spinner(model_type)
    spinner = ModelSpinner(path, init_in_new_periods)

    try:
        # Do any preparation necessary
        spinner.set_up()

        # First spin (no previous results to interpolate)
        first_spin = schedule.pop(0)
        spinner.update_simulation_times(t_start=first_spin['spin_start'], t_end=first_spin['spin_end'])
        spinner.update_column_depth(first_spin['column_depth'])
        spinner.run_model(**kwargs)

        if first_spin.get("save_state"):
            spinner.save_current_state()
        if first_spin.get("save_results"):
            spinner.save_results()

        # Subsequent spins
        for run in schedule:
            spinner.update_simulation_times(t_start=run['spin_start'], t_end=run['spin_end'])
            spinner.update_initial_conditions(column_depth=run['column_depth'])
            spinner.run_model(**kwargs)
            
            if run.get("save_state"):
                spinner.save_current_state()
            if run.get("save_results"):
                spinner.save_results()

    except Exception as e:
        raise
    
    finally:
        # Clean up temporary files
        spinner.tear_down()


def _expand_schedule(spin_times: "list[int]", **kwargs: list) -> "list[dict[str, Any]]":
    schedule = []
    for period, iterations in enumerate(spin_times):
        for i in range(iterations):
            iteration = { key: value[period] for key, value in kwargs.items()}
            schedule.append(iteration)
    
    return schedule


def get_spinner(model_type: str) -> "Type[Spinner]":
    if model_type == CLASSIC:
        return ClassicSpinner
    return ClassicSpinner


class Spinner(ABC):
    
    def __init__(self, path, init_in_new_periods: int = 1):
        """ A class for (GEOtop-style) step-wise model spinup 
        
        Parameters
        ----------
        path : str
            Path to model directory or 
        init_in_new_periods : int
            How initial conditions should be calculated at the start of each model run.
            1 : all variables are instantaneous end-of-run values above spin-up depth and equal 
            to the run-averaged value of the lowest node below the spin-up depth (constant). 

            2 : all variables are run-averaged values above spin-up depth and equal 
            to the run-averaged value of the lowest node below the spin-up depth. 

            3: all variables are instantaneous end-of-run values above spin-up depth and extrapolated 
            below spin-up depth using the run-averaged lowest node value and assuming steady-state conditions.
        """
        self.path = path
        self.init_in_new_periods = init_in_new_periods

    def set_up(self):
        """ Do anything that needs to be done ahead of time.
        
        This should be run before any values are set, and may include
        the creation of temporary files, or the clean-up of temporary files
        from previous spin-ups (in case tear_down was not successful).
        """
        return
    
    def tear_down(self):
        """ Clean up any files that were created. 
        
        Restores model directory to initial state, but saved model state and results may persist.
        """
        return

    @abstractmethod
    def update_column_depth(self, column_depth: int):
        """ Does not require previous model run. Updates model settings or IC to reflect chosen column_depth 
        Needed for the first spin before there are model outputs
        """
        raise NotImplementedError

    @abstractmethod
    def update_initial_conditions(self, column_depth, mode=1):
        """ Requires previous model run. Adjust max depth if necessary (otherwise left as-is)
        then update initial conditions according to the mode
        """
        raise NotImplementedError

    @abstractmethod
    def update_simulation_times(self, t_start: "Optional[datetime]", t_end: "Optional[datetime]"):
        """ Reconfigure the model to run for the period [t_start, t_end) """
        raise NotImplementedError

    @abstractmethod
    def run_model(self, **kwargs):
        """ Run the model in its current configuration. 
        
        Optional keyword arguments can be passed to indicate model executables or other necessary files
        """
        raise NotImplementedError

    def save_current_state(self):
        """ Backup current model state
        This prevents model state from being overwritten in subsequent runs and
        is useful for asessing spin-up
        """
        raise NotImplementedError

    def save_results(self):
        """ Backup current set of model results
        
        This prevents model results from being overwritten in subsequent runs and
        is useful for asessing spin-up
        """
        raise NotImplementedError


class ClassicSpinner(Spinner):
    
    def __init__(self, path, init_in_new_periods=1):
        """ Stepwise spin-up class for CLASSIC
        
        Renames 
        """
        super().__init__(path, init_in_new_periods=init_in_new_periods)
        self.directory = ClassicDirectory(path)

    def save_results(self):
        output_directory = Path(coff(self.directory.job_options_file, short_name="tsl")).parent  # TODO: get this directly from job_options
        new_output = next_path(str(Path(output_directory).with_name("CLASSIC_results_{}.tar")))
        
        shutil.make_archive(Path(new_output).with_suffix(""), 'tar', output_directory)

    def save_current_state(self):
        current_state = self.rsfile
        saved = next_path(str(Path(self.rsfile).with_name("modelstate.{}.nc")))
        shutil.copy(current_state, saved)

    def set_up(self):
        # Delete existing
        if Path(self.backup_initfile).is_file():
            Path(self.backup_initfile).unlink()
            print(f" (DELETEME) deleted {self.backup_initfile}")

        for p in Path(self.rsfile).parent.glob("modelstate*.nc"):
            print(f"DELETED old model state {p}")
            p.unlink()
        output_directory = Path(coff(self.directory.job_options_file, short_name="tsl")).parent
        for p in Path(output_directory).parent.glob("CLASSIC_results_*.tar"):
            print(f"DELETED old model results {p}")
            p.unlink()

        print("TODO: delete saved output and model state")

        # make copy of init file
        init = xr.open_dataset(self.initfile)
        init.to_netcdf(self.backup_initfile)
        init.to_netcdf(self.rsfile)
        print(f" (DELETEME) created {self.backup_initfile}")
        print(f" (DELETEME) created {self.rsfile}")

    def tear_down(self):
        # Replace back-up init file with regular init file
        replace(self.backup_initfile, self.initfile)
        
    def run_model(self, **kwargs):
        import subprocess

        c = PemConfig.empty()
        c._config['models']['classic'] = {}
        c._config['models']['classic']['container'] = kwargs["container"]
        c._config['models']['classic']['binary'] = kwargs["binary"]

        shutil.copy(self.initfile, self.rsfile)  # make sure RSfile has same dimensions
        command =  ClassicSimulation(c, self.directory.dirpath).command()
        print(command)
        p = subprocess.Popen(command, shell=True)
        (output, err) = p.communicate()  # Blocks further action
        

    @property
    def initfile(self) -> str:
        """ Path to init file """
        return str(Path(self.directory.get_job_option('init_file')).resolve())

    @property
    def rsfile(self) -> str:
        return str(Path(self.directory.get_job_option('rs_file_to_overwrite')).resolve())

    @property
    def backup_initfile(self) -> str:
        """ Return the path of the backup initfile"""
        backup_init_file = Path(self.initfile).with_suffix(".backup.nc")
        return str(backup_init_file)

    def update_column_depth(self, column_depth: int):
        # TODO: get current column depth. If equal or less than current. just subset current.
        if not Path(self.backup_initfile).is_file():
            raise FileNotFoundError(f"Backup file '{self.backup_initfile}' does not exist. Run set_up() to create it.")
        else:
            self._update_column_depth(self.initfile, backup_initfile=self.backup_initfile, column_depth=column_depth)
        
        print(f"Updated simulation column depth to {column_depth} layers")

    @staticmethod
    def _update_column_depth(initfile: str, backup_initfile:str, column_depth: int):
        original = xr.open_dataset(backup_initfile)
        subsetted = original.isel(layer=slice(0, column_depth, 1))
        # TODO: read initfile and replace values in subsetted where possible.
        subsetted.to_netcdf(initfile)
        
    def update_initial_conditions(self, column_depth, mode: int=1):
        if mode == 1:
            self._update_instantaneous(self.backup_initfile, self.rsfile, self.initfile, column_depth)
            self._extrapolate_ra(1)
        elif mode == 2:
            raise NotImplementedError
        elif mode == 3:
            self._update_instantaneous(self.backup_initfile, self.rsfile, self.initfile, column_depth)
            self._extrapolate_ra(2)
        else:
            raise ValueError

    @staticmethod
    def _update_instantaneous(original_ic: str, rsfile: str, initfile:str, column_depth: int):
        """ Updates initial conditions with instantaneous end-of-run values 
        if column depth is greater, fill from og initial conditions
        """
        # Open instantaneous values from end of last run
        instantaneous = xr.open_dataset(rsfile)
        current_layers = int(max(instantaneous.coords['layer']))  # this is 1-indexed value

        if current_layers == column_depth:  # no new layers needed
            new = instantaneous.copy()

        else:
            try:
                ds = xr.open_dataset(original_ic)  # Open orignal values
            except FileNotFoundError:
                raise FileNotFoundError(f"Backup file '{original_ic}' does not exist. Run setup to create it.")

            below_spin_up = ds.isel(layer=slice(current_layers, column_depth, 1))  #  WARNING: treating values like indices (off by 1)

            new = xr.concat([instantaneous, below_spin_up], 
                             dim='layer', 
                             compat="override", 
                             coords="minimal", 
                             data_vars="minimal")

        # Save init file
        new.to_netcdf(initfile)

    def _extrapolate_ra(self, mode=1):
        """
        Extrapolate run-averaged data below lowest spun-up depth
        """
        initfile = self.initfile  # below spin up values have already been copied from OG ICs
        jof = self.directory.job_options_file
        
        with xr.open_dataset(initfile) as i:
                initial_conditions = i.load()  # fully load data so we can write to file
        
        if mode == 1:
            # Temperature
            with xr.open_dataset(coff(jof, short_name="tsl")) as output:
                spun_up_depth = int(max(output.layer))

                lowest_node_run_avg = output["tsl"].sel(layer=spun_up_depth).mean().values
                lowest_node_run_avg -= 273.15  # Convert units (K -> C)
                print(f"lowest node T={lowest_node_run_avg} C")
                
                data = initial_conditions["TBAR"]
                new = data.where(data.layer <= spun_up_depth, lowest_node_run_avg)
                initial_conditions.update({"TBAR": new})

            # Water
            delz_lowest_node = initial_conditions["DELZ"].sel(layer=spun_up_depth).values  # [m]
            init_total_moisture = initial_conditions["THIC"][:] + initial_conditions["THLQ"][:]  # [m3 m-3]

            with xr.open_dataset(coff(jof, short_name="mrsll")) as mrsll:
                with xr.open_dataset(coff(jof, short_name="mrsfl")) as mrsfl:
                    mrsll_lowest_node_run_avg = mrsll["mrsll"].sel(layer=spun_up_depth).mean().values  # [kg m-2]
                    rho_water = 1e3  # [kg m-3]
                    mrsll_lowest_node_run_avg = mrsll_lowest_node_run_avg / (rho_water * delz_lowest_node)  # [m3 m-3]

                    mrsfl_lowest_node_run_avg = mrsfl["mrsfl"].sel(layer=spun_up_depth).mean().values  # [kg m-2]
                    rho_ice = 0.917e3  # [kg m-3]  from CLASSIC codes
                    mrsfl_lowest_node_run_avg = mrsfl_lowest_node_run_avg / (rho_ice * delz_lowest_node)  # [m3 m-3]

                    ra_lowest_node_total_moisture = mrsfl_lowest_node_run_avg + mrsll_lowest_node_run_avg  # [m3 m-3]
                    assert(ra_lowest_node_total_moisture) < 1

                    liq_fraction = mrsll_lowest_node_run_avg / ra_lowest_node_total_moisture
                    ice_fraction = mrsfl_lowest_node_run_avg / ra_lowest_node_total_moisture

                    thlq = initial_conditions["THLQ"]  # [m3 m-3]
                    new_thlq = thlq.where(thlq.layer <= spun_up_depth, liq_fraction * init_total_moisture)
                    initial_conditions.update({"THLQ": new_thlq})

                    thic = initial_conditions["THIC"]  # [m3 m-3]
                    new_thic = thic.where(thic.layer <= spun_up_depth, ice_fraction * init_total_moisture)
                    initial_conditions.update({"THIC": new_thic})
        
        elif mode == 3:
            # Temperature
            with xr.open_dataset(coff(jof, short_name="tsl")) as output:
                spun_up_depth = int(max(output.layer))

                lowest_node_t_avg = output["tsl"].sel(layer=spun_up_depth).mean().values
                lowest_node_t_avg -= 273.15  # Convert units (K -> C)
                 
                dfe = initial_conditions.where(initial_conditions.layer >= spun_up_depth, drop=True)  # data for interpolation
                extrapolated = steady_state_extrapolation(T=dfe['TBAR'].values.ravel(),
                                                          Q=0.0,  # TODO: check that this is the case
                                                          delta_z=dfe['DELZ'].values.ravel(),
                                                          lambda_t = lambda_CLASSIC,
                                                          THLQ=dfe['THLQ'].values.ravel(),
                                                          THIC=dfe['THIC'].values.ravel(),
                                                          CLAY=dfe['CLAY'].values.ravel(),
                                                          SAND=dfe['SAND'].values.ravel(),
                                                          ORGM=dfe['ORGM'].values.ravel())
                tbar = initial_conditions["TBAR"]
                new_tbar = tbar.where(tbar.layer <= spun_up_depth, extrapolated)
                initial_conditions.update({"TBAR": new_tbar})

            # Water
            delz_lowest_node = initial_conditions["DELZ"].sel(layer=spun_up_depth).values  # [m]
            init_total_moisture = initial_conditions["THIC"][:] + initial_conditions["THLQ"][:]  # [m3 m-3]

            with xr.open_dataset(coff(jof, short_name="mrsll")) as mrsll:
                with xr.open_dataset(coff(jof, short_name="mrsfl")) as mrsfl:
                    mrsll_lowest_node_run_avg = mrsll["mrsll"].sel(layer=spun_up_depth).mean().values  # [kg m-2]
                    rho_water = 1e3  # [kg m-3]
                    mrsll_lowest_node_run_avg = mrsll_lowest_node_run_avg / (rho_water * delz_lowest_node)  # [m3 m-3]

                    mrsfl_lowest_node_run_avg = mrsfl["mrsfl"].sel(layer=spun_up_depth).mean().values  # [kg m-2]
                    rho_ice = 0.917e3  # [kg m-3]  from CLASSIC codes
                    mrsfl_lowest_node_run_avg = mrsfl_lowest_node_run_avg / (rho_ice * delz_lowest_node)  # [m3 m-3]

                    ra_lowest_node_total_moisture = mrsfl_lowest_node_run_avg + mrsll_lowest_node_run_avg  # [m3 m-3]
                    assert(ra_lowest_node_total_moisture) < 1

                    liq_fraction = mrsll_lowest_node_run_avg / ra_lowest_node_total_moisture
                    ice_fraction = mrsfl_lowest_node_run_avg / ra_lowest_node_total_moisture

                    thlq = initial_conditions["THLQ"]  # [m3 m-3]
                    new_thlq = thlq.where(thlq.layer <= spun_up_depth, liq_fraction * init_total_moisture)
                    initial_conditions.update({"THLQ": new_thlq})

                    thic = initial_conditions["THIC"]  # [m3 m-3]
                    new_thic = thic.where(thic.layer <= spun_up_depth, ice_fraction * init_total_moisture)
                    initial_conditions.update({"THIC": new_thic})
        else:
            raise ValueError(f"bad mode ({mode})")

        # Save
        initial_conditions.to_netcdf(initfile)

    @staticmethod
    def _get_simulated_values(initfile: str,
                              init_var: str,
                              simulated:str,
                              simulated_var: str,
                              scaling_fn=None):
        """
        modify an initfile by extrapolating constant run-averaged values below a certain depth

        Parameters
        ----------
        initfile : str
            path to initialization file
        init_var : str
            name of variable in initialization file to write to
        simulated : str
            path to result
        simulated_var : str
            name of variable in simulated file
        scaling_fn : callable
            function to call on avaredaged data
        """
        # open files
        output = xr.open_dataset(simulated)
        spun_up_depth = max(output.layer)
        with xr.open_dataset(initfile) as i:
            initial_conditions = i.load()  # fully load data so we can write to file
        
        # if nearest
        lowest_node_run_avg = output[simulated_var].isel(layer=spun_up_depth).mean().values

    @staticmethod
    def _extrapolate_ra_constant(initfile: str,
                                 init_var: str,
                                 simulated:str,
                                 simulated_var: str,
                                 scaling_fn=None):
        """
        modify an initfile by extrapolating constant run-averaged values below a certain depth

        Parameters
        ----------
        initfile : str
            path to initialization file
        init_var : str
            name of variable in initialization file to write to
        simulated : str
            path to result
        simulated_var : str
            name of variable in simulated file
        scaling_fn : callable
            function to call on avaredaged data
        """
        #simulated= r"/home/s/stgruber/nbr512/CA-Qfo/output/tsl_daily.nc"
        #initfile = r"/home/s/stgruber/nbr512/CA-Qfo/CA-Qfo_init.nc"
        #max_depth = 5
        #init_var = "TBAR"
        #simulated_var = "tsl"
        if scaling_fn is None:
            scaling_fn = lambda x: x
        # open files
        output = xr.open_dataset(simulated)
        spun_up_depth = max(output.layer)
        with xr.open_dataset(initfile) as i:
            initial_conditions = i.load()  # fully load data so we can write to file
        
        # check assumptions
        assert(output.lon.shape == output.lat.shape == (1,))
        assert(initial_conditions.lon.shape == initial_conditions.lat.shape == (1,))
        
        # if nearest
        lowest_node_run_avg = scaling_fn(output[simulated_var].sel(layer=spun_up_depth).mean().values)
        data = initial_conditions[init_var]
        new = data.where(data.layer <= spun_up_depth, lowest_node_run_avg)
        initial_conditions.update({init_var: new})
        initial_conditions.to_netcdf(initfile)

    @staticmethod
    def _extrapolate_inst_constant(initfile: str,
                                   init_var: str,
                                   spun_up_depth: int):
        """
        modify an initfile by extrapolating instantaneous values below a certain depth

        Parameters
        ----------
        initfile : str
            path to initialization file
        init_var : str
            name of variable in initialization file to write to
        max depth : int 
            index (1-indexed) of lowest depth that was spun up
        """
        #simulated= r"/home/s/stgruber/nbr512/CA-Qfo/output/tsl_daily.nc"
        #initfile = r"/home/s/stgruber/nbr512/CA-Qfo/CA-Qfo_init.nc"
        #max_depth = 5
        #init_var = "TBAR"
        #simulated_var = "tsl"

        # open files
        with xr.open_dataset(initfile) as i:
            initial_conditions = i.load()  # fully load data so we can write to file
        
        # check assumptions
        assert(initial_conditions.lon.shape == initial_conditions.lat.shape == (1,))
        
        # if nearest
        lowest_inst_value = initial_conditions[init_var].isel(layer=spun_up_depth).mean().values
        data = initial_conditions[init_var]
        updated_data = data.where(data.layer <= spun_up_depth, lowest_inst_value)
        initial_conditions.update({init_var: updated_data})
        initial_conditions.to_netcdf(initfile)

    def update_simulation_times(self,
                                t_start: "Optional[datetime]"=None,
                                t_end: "Optional[datetime]"=None ):
        job_options_file = self.directory.job_options_file
        self._update_simulation_times(job_options_file=job_options_file,
                                      t_start=t_start, t_end=t_end)

    @staticmethod
    def _update_simulation_times(job_options_file: str, 
                      t_start: "Optional[datetime]" = None,
                      t_end: "Optional[datetime]" = None):
        """ Update simulation dates in job options file"""
        if (not t_start and not t_end):
            raise ValueError("At least one of 'start', 'end' must be provided")
        
        job_options = f90nml.read(job_options_file)
        
        if t_start is not None:
            job_options['joboptions']["readMetStartYear"] = t_start.year
            print(f"Set start time {t_start.year}")
            
            job_options['joboptions']["JHHSTY"] = t_start.year
            job_options['joboptions']["JDSTY"] = t_start.year
            job_options['joboptions']["JMOSTY"] = t_start.year
            print(f"Set output start: {t_start.year}")
        
        if t_end is not None:
            job_options['joboptions']["readMetEndYear"] = t_end.year
            print(f"Set end time {t_end.year}")
            
            job_options['joboptions']["JHHENDY"] = t_end.year
            job_options['joboptions']["JDENDY"] = t_end.year
            print(f"Set output end: {t_end.year}")

        new_jof = Path(job_options_file).with_name("new_job_options_file.txt")
        f90nml.patch(job_options_file, job_options, new_jof)
        replace(new_jof, job_options_file)


def next_path(path_pattern: str) -> str:
    """
    Finds the next free path in an sequentially named list of files

    e.g. path_pattern = 'file-%s.txt':

    file-1.txt
    file-2.txt
    file-3.txt

    Runs in log(n) time where n is the number of existing files in sequence
    """
    i = 1

    # First do an exponential search
    while Path(path_pattern.format(i)).is_file() or Path(path_pattern.format(i)).is_dir():
        i *= 2

    # Result lies somewhere in the interval (i/2..i]
    # We call this interval (a..b] and narrow it down until a + 1 = b
    a, b = (i // 2, i)
    while a + 1 < b:
        c = (a + b) // 2 # interval midpoint
        a, b = (c, b) if Path(path_pattern.format(c)).is_file() or Path(path_pattern.format(c)).is_dir() else (a, c)

    return path_pattern.format(b) 


def steady_state_extrapolation(T, Q: float, delta_z, lambda_t, **kwargs):
    """
    T : array
        initial temperatures guess
    Q : float
        geothermal flux [W m-2]
    delta_z : array
        width of each soil layer [m]. Same length as T
    lambda_t : 
        function with first argument T, and kwargs matching those provided to this function 
        calculate thermal conductivity for each layer
        
    Returns
    -------
    array
        Steady-state temperature values 
    """
    maxiter = 10

    while maxiter:
        #2 calculate theta(n)
        conductivity = lambda_t(T, **kwargs)

        #3 calculate lambda(n,T) for each layer
        T_new = np.ones_like(T)
        T_new[0] = T[0]

        #4 starting from initial T(n), update T(n) with Q, deltaz and theta(n)
        for n in np.arange(1, len(T_new)):
            T_top_of_next = T_new[n - 1] + (Q * 0.5 * delta_z[n - 1] / conductivity[n - 1])
            T_new[n] = T_top_of_next + (Q * 0.5 * delta_z[n] / conductivity[n])

        # 5 If within tolerance, end; otherwise, use new T(n) as guess and repeat 
        tolerance = 0.1
        errfn = lambda a,b : np.less(np.sqrt(np.mean(np.square(np.subtract(a, b)))), tolerance)
        
        print(np.sqrt(np.mean(np.square(np.subtract(T, T_new)))))

        if errfn(T, T_new):  
            T = T_new
            break
        else:
            T = T_new
            maxiter -= 1

    return T


def delta_T(Q, delta_z, conductivity) -> float:
    del_t = Q * delta_z / conductivity
    return del_t


def lambda_CLASSIC(T, THLQ, THIC, SAND, CLAY, ORGM):
    """ Interpreted from CLASSIC documentation"""
    if np.any(SAND == -1) or np.any(SAND == -2):
        raise ValueError("I haven't figured that part out yet (Sand -1 or -2 )")
        
    # Constants
    TCW = 0.57 # water [W m-1 K-1]
    TCICE = 2.24 # ice [W m-1 K-1]
    TCSAND = 2.5 # sand particles  [W m-1 K-1]
    TCCLAY = 2.5 # fine mineral particles  [W m-1 K-1]
    TCOM = 0.25 # organic matter  [W m-1 K-1]
    RHOSOL = 2.65E3 # soil particle density [kg m-3] (set in run parameters)
    RHOOM = 1.30E3 # org matter particle density [kg m-3]

    # From soilProperties.f90
    THPOR = ( - 0.126 * SAND + 48.9) / 100.0  
    VSAND = SAND / (RHOSOL * 100.0)
    VORG = ORGM / (RHOOM * 100.0)
    VFINE = (100.0 - SAND - ORGM) / (RHOSOL * 100.0)
    VTOT = VSAND + VFINE + VORG
    THSAND = (1.0 - THPOR) * VSAND / VTOT
    THORG = (1.0 - THPOR) * VORG / VTOT
    THFINE = 1.0 - THPOR - THSAND - THORG
    #HCPS(I,M,J) = (HCPSND * THSAND + HCPCLY * THFINE + 
    #            HCPOM * THORG) / (1.0 - THPOR)
    TCS = (TCSAND * THSAND + TCOM * THORG + TCCLAY * THFINE) / (1.0 - THPOR)

    # From energyBudgetPrep.f90
    SATRAT = THLQ + THIC / THPOR  

    r"""
    For each layer, if THLIQ is found to exceed THLMIN + THEVAP,
    THFREZ is compared to the available water. If THFREZ \f$\leq\f$
    THLIQ - THLMIN - THEVAP, all of the available energy sink
    is used to freeze part of the liquid water content in
    the permeable part of the soil layer, the amount of
    energy involved is subtracted from HTC and added to HMFG,
    \f$C_g\f$ is recalculated and the layer temperature is set to
    0 C. Otherwise, all of the liquid water content of the
    layer above THLMIN + THEVAP is converted to frozen water,

    If the soil layer temperature is above freezing, the liquid moisture
    content would be set to the field capacity and the frozen moisture 
    content to zero; if the layer temperature is below zero, the liquid
    moisture content would be set to the minimum value and the frozen
    moisture content to the field capacity minus the minimum value 
    (*THLMIN*; see @ref soilProperties.f90)
    """
    GRKSAT = 7.0556E-6 * (np.exp(0.0352 * SAND - 2.035))
    BI = 0.159 * CLAY + 2.91
    PSISAT = 0.01 * np.exp( -0.0302 * SAND + 4.33)
    THFC = THPOR * np.power((1.157E-9 / GRKSAT), (1.0 / (2.0 * BI + 3.0)))

    THLMIN = 0.04  
    THLIQG = np.where(T>0, THLQ + THIC, THLMIN)
    THICEG = np.where(T>0, 0, THLQ + THIC - THLMIN)
    THICEG = np.where(THICEG > 0, THICEG, 0)  # make sure its not zero?

    THLSAT = THLIQG / (THLIQG + THICEG)
    THISAT = THICEG / (THLIQG + THICEG)

    TCDRY = 0.75 * np.exp( - 2.76 * THPOR)
    TCSATU = TCW * THPOR + TCS * (1.0 - THPOR)
    TCSATF = TCICE * THPOR + TCS * (1.0 - THPOR)
    TCKAPU = (4.0 * SAND + 1.9 * 100 - SAND) / 100.0  #converted back from ISAND
    TCKAPF = (1.2 * SAND + 0.85 * 100 - SAND) / 100.0
    TCRATU = TCKAPU * SATRAT / (1.0 + (TCKAPU - 1.0) * SATRAT)
    TCRATF = TCKAPF * SATRAT / (1.0 + (TCKAPF - 1.0) * SATRAT)
    TCSOLU = (TCSATU - TCDRY) * TCRATU + TCDRY
    TCSOLF = (TCSATF - TCDRY) * TCRATF + TCDRY
    TCSOIL = TCSOLU * THLSAT + TCSOLF * THISAT

    TCSOIL = np.where(SAND==-3, TCSAND, TCSOIL)  # bedrock
    TCSOIL = np.where(SAND==-4, TCSAND, TCICE)  # ice sheet

    return TCSOIL


#data = nc.Dataset(r"/home/s/stgruber/nbr512/RU-Che/RU-Che_init.nc")
if __name__ == "__main__":
    import netCDF4 as nc
    data = nc.Dataset(r"/home/s/stgruber/nbr512/RU-Che/rsfile.nc")
    #k = lambda_CLASSIC(data['TBAR'][:].ravel(), )

    k = steady_state_extrapolation(T=data['TBAR'][:].ravel(),
                             Q=0.1,
                             delta_z=data['DELZ'][:].ravel(),
                             lambda_t = lambda_CLASSIC,
                             THLQ=data['THLQ'][:].ravel(),
                             THIC=data['THIC'][:].ravel(),
                             CLAY=data['CLAY'][:].ravel(),
                             SAND=data['SAND'][:].ravel(),
                             ORGM=data['ORGM'][:].ravel())