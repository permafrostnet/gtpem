import argparse
import tomlkit

from datetime import datetime
from pathlib import Path

from gtpem.spinup import get_spinner, stepwise_spinup
from gtpem.names import CLASSIC


def spinup(args):
    print(args)
    print('Stepwise spinup!')
    
    with open(args.file) as toml:
        config = tomlkit.loads(toml.read())

    keyword_args = {key: value for key,value in [item.split('=') for item in args.K]}

    stepwise_spinup(path=args.path,
                    model_type=args.model_type,
                    spin_times=config.get("NumSimulationTimes"),
                    spin_start=config.get("InitDate"),
                    spin_end=config.get("EndDate"),
                    column_depth=config.get("SpinupLayerBottom"),
                    init_in_new_periods=config.get("InitInNewPeriods"),
                    save_results=config.get("SaveModelResults"),
                    save_state=config.get("SaveModelState"),
                    **keyword_args)


def main():
    parser = argparse.ArgumentParser(
        description="Do stepwise spin up of a model.",
        formatter_class=argparse.RawTextHelpFormatter, 
        add_help=True,
        epilog="If you want to run stepwise spinup using a single line, you must create a TOML-file with\
GEOtop-like parameters. Use the following template:\n\n \
InitInNewPeriods = 1  # Integer, Either 1 or 2\n \
NumSimulationTimes = [ 5, 20 ]  # Number of times to run model in each simulation period\n \
SpinupLayerBottom = [ 10, 15 ]  # Lowest layer to spin up in each simulation period \n \
InitDate = [ 1980-01-01T00:00, 1980-01-01T00:00 ]  # Start date for each simulation period\n \
EndDate = [ 1985-05-27T07:32, 1995-12-31T00:00 ]  # End date for each simulation period\n \
SaveModelState = [ true, true ]  # Whether or not to save model state at end of each run\n \
SaveModelResults = [false, true] # Whether or not to back-up model results at end of each run")

    
    parser.set_defaults(func = lambda x: 0)
    subparsers = parser.add_subparsers()

    ssparser = subparsers.add_parser(name="spinup", help="Do a stepwise spin-up")
    ssparser.add_argument('-f', '--file', type=Path, help='path to TOML config file. Run "sspin --help" for more information')
    ssparser.set_defaults(func=spinup)

    setup_parser = subparsers.add_parser(name="setup", help="Prepare simulation directory for a spin-up")
    setup_parser.set_defaults(func=setup)

    update_parser = subparsers.add_parser(name="update", help="Update either the depth bounds or the date")
    update_parser.set_defaults(func=update)
    update_parser.add_argument("-s", "--start", default=None, type=datetime.fromisoformat, help="Beginning of simulation period in ISO date format YYYY-MM-DDTHH:MM:SS")
    update_parser.add_argument("-e", "--end", default=None, type=datetime.fromisoformat, help="End of simulation period in ISO date format YYYY-MM-DDTHH:MM:SS")
    update_parser.add_argument("-d", "--column-depth", default=None, type=int, help="Lowest layer to spin up in next run. 0 spins up all layers")


    run_parser = subparsers.add_parser(name="run", help="Run the model with the current configuration", 
                                       formatter_class=argparse.RawTextHelpFormatter, 
                                       epilog="Required model keywords:\n\
CLASSIC: container:path to singularity container, binary:path to CLASSIC binary file")
    run_parser.set_defaults(func=run)
    run_parser.add_argument("-n", help="number of times to run model")
    for p in [run_parser, ssparser]:
        p.add_argument("-K", action="append", type=str, help="Model specific keywords in the form -K 'a=b' -K b=c used to run the model.\
             See 'sspin run --help' for more information")
    

    save_parser =  subparsers.add_parser(name="save", help="Save current model state or results")
    save_parser.set_defaults(func=save)
    save_parser.add_argument("-S", "--state", action="store_true", help="Make a copy of the current model state (end of last simulation)")
    save_parser.add_argument("-R", "--results", action="store_true", help="Make a copy of the previous results (last simulation)")
     
    init_parser = subparsers.add_parser(name="init", help="Reinitialize model initial conditions from previous run.")
    init_parser.set_defaults(func=reinitialize)
    init_parser.add_argument("-i", "--init-new", type=int, default=1, choices=[1,3], help="How to re-calculate initial conditions. Layers are calculated according to the value. (1) instantaneous values above, interpolated values (using the averages) below. (2) run averaged values above, interpolated values (using the averages) below")
    init_parser.add_argument("-d", "--column-depth", default=None, type=int, help="Lowest layer to spin up in next run. 0 spins up all layers. First layer is 1 (i.e. NOT zero-indexed)")


    finish_parser = subparsers.add_parser(name="finish", help="Prepare simulation directory for a spin-up")
    finish_parser.set_defaults(func=finish)

    for p in [ssparser, update_parser, run_parser, setup_parser, init_parser, save_parser, finish_parser]:
        p.add_argument('-m', "--model", dest='model_type', choices=[CLASSIC], help="Which model to run")
        p.add_argument('path', type=str,  help="Path to model directory or file")

    args, extra_args = parser.parse_known_args()
    print(args)
    args.func(args)


def update(args):
    print("Run Update")

    ModelSpinner = get_spinner(args.model_type)
    spinner = ModelSpinner(args.path)

    if args.start is not None or args.end is not None:
        spinner.update_simulation_times(args.start, args.end)
    
    if args.column_depth is not None:
        try:
            spinner.update_column_depth(args.column_depth)
        except FileNotFoundError:
            print("Model has not been set up for spinning.  Run spinup.py setup")


def reinitialize(args):
    print("Run Init")
    
    ModelSpinner = get_spinner(args.model_type)
    spinner = ModelSpinner(args.path)

    if args.init_new:
        spinner.update_initial_conditions(args.column_depth, args.init_new)


def run(args):
    print(args)
    ModelSpinner = get_spinner(args.model_type)
    spinner = ModelSpinner(args.path)

    keyword_args = {key: value for key,value in [item.split('=') for item in args.K]}
    spinner.run_model(**keyword_args)


def setup(args):
    print("Run setup")

    ModelSpinner = get_spinner(args.model_type)
    spinner = ModelSpinner(args.path)
    spinner.set_up()


def save(args):
    print("Run save")

    ModelSpinner = get_spinner(args.model_type)
    spinner = ModelSpinner(args.path)
    if args.results:
        spinner.save_results()
    if args.state:
        spinner.save_current_state()


def finish(args):
    print("Run tear down")

    ModelSpinner = get_spinner(args.model_type)
    spinner = ModelSpinner(args.path)
    spinner.tear_down()


if __name__ == "__main__":
    main()

