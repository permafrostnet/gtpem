import logging
from pathlib import Path


log_levels = {
    0: logging.CRITICAL,
    1: logging.ERROR,
    2: logging.WARN,
    3: logging.INFO,
    4: logging.DEBUG,
}


def configure_logging(logger, args):
    verbosity = min(args.verbosity, 4)
    #logging.basicConfig(format='%(asctime)s  %(asctime)s ')
    logger.setLevel(log_levels[verbosity])

    console_formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)s %(message)s', datefmt="%H:%M:%S")

    ch = logging.StreamHandler()
    ch.setLevel(log_levels[verbosity])
    ch.setFormatter(console_formatter)
    logger.addHandler(ch)

    if vars(args).get("logfile"):
        file_formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)s %(message)s', datefmt="%Y%m%d%TH%M%S")
        logfile = Path(args.logfile)
        if not logfile.is_file():
            if not logfile.parent.is_dir():
                logfile.parent.mkdir(parents=True)
        fh = logging.FileHandler(logfile)
        fh.setLevel(log_levels[verbosity])
        fh.setFormatter(file_formatter)
        logger.addHandler(fh)

