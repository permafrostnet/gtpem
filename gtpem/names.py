GEOTOP = 'geotop'
CLASSIC = 'classic'
FREETHAW = 'freethaw'


def model_name_abbreviation(name: str) -> str:
    """ returns an abbreviated name """
    abbr = {GEOTOP: "gt",
            CLASSIC: "cls",
            FREETHAW: "ft"}

    try:
        return abbr[name]
    except KeyError:
        return name

