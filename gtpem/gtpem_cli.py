#!/usr/bin/env python3

import sys
import argparse

from gtpem._version import __version__ as version
from gtpem.log_config import configure_logging
from gtpem import commands

import logging

logger = logging.getLogger()


def main():
    
    mainparser = argparse.ArgumentParser(description="Grid Toolkit for Permafrost Ensemble Modelling (GTPEM)")
    subparsers = mainparser.add_subparsers()
    
    #  Shared arguments
    mainparser.add_argument("--version", action='version', version=f"GTPEM version {version}")

    # Invisibly inherited parent parsers
    use_jobdir = argparse.ArgumentParser(add_help=False)
    use_jobdir.add_argument("-J", '--job-dir', type=str, dest='job_dir', default=None,
                            help="Overrides the working directory in the configuration file and runs "
                            "the job on the provided directory instead. Does not modify the configuration file")

    sharedparser = argparse.ArgumentParser(add_help=False)
    sharedparser.add_argument("config", help="path to configuration toml file.")
    sharedparser.add_argument("-v", "--verbose", dest="verbosity", action="count", default=2,
                              help="Verbosity (between 1-4 occurrences with more leading to more "
                              "verbose logging). ERROR=1, WARN=2, INFO=3, "
                              "DEBUG=4")
    sharedparser.add_argument('-L', "--log", type=str, dest="logfile", default=None)

    build = subparsers.add_parser("build", parents=[use_jobdir, sharedparser],
                                  description="Automatically generate gtpem model directories using a control file")
    run = subparsers.add_parser("run", parents=[use_jobdir, sharedparser],
                                description="Launch gtpem jobs")
    aggregate = subparsers.add_parser("aggregate", parents=[sharedparser],
                                      description="Aggregate the successfully-run simulations based on a control file")
    status = subparsers.add_parser("status", parents=[use_jobdir, sharedparser],
                                   description="Get information on the status of jobs")

    # parser-specific
    run.add_argument("-b", "--build-directories", action='store_true',
                     dest='build_dirs',
                     help="Whether or not to build directories automatically from a control file")
    
    run.add_argument("-d", "--dry-run", action='store_true', default=False,
                     dest='dry_run',
                     help="If selected, will not submit job, and will only create directories and submission scripts")
    
    run.add_argument("-R", "--restart", action='store_true', default=False,
                     dest='restart',
                     help="Re-submit directories that have failed or tawere successful. Otherwise, failed and successfully completed"
                          "simulations will not be run.")

    status.add_argument("-d", "--details", action='store_true', default=False,
                        dest='details',
                        help="If selected, will print individual status for each simulation")

    build.set_defaults(func=commands.build)
    run.set_defaults(func=commands.run)
    aggregate.set_defaults(func=commands.aggregate)
    status.set_defaults(func=commands.status)

    if len(sys.argv) == 1:
        mainparser.print_help(sys.stderr)
        sys.exit(1)

    else:
        args = mainparser.parse_args()
        configure_logging(logger, args)
        logger.info(f"GTPEM version {version}")
        args.func(args)


if __name__ == "__main__":
    main()
